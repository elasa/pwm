_pwm_8bits:
;RGB_PWM_Android.c,14 :: 		unsigned int pwm_8bits(unsigned int ratio, unsigned short byte)
; byte start address is: 4 (R1)
; ratio start address is: 0 (R0)
;RGB_PWM_Android.c,17 :: 		temp = ((unsigned long) ratio * (255 - byte)) / 255;
; byte end address is: 4 (R1)
; ratio end address is: 0 (R0)
; ratio start address is: 0 (R0)
; byte start address is: 4 (R1)
UXTH	R3, R0
; ratio end address is: 0 (R0)
RSB	R2, R1, #255
SXTH	R2, R2
; byte end address is: 4 (R1)
MULS	R3, R2, R3
MOVS	R2, #255
UDIV	R2, R3, R2
;RGB_PWM_Android.c,18 :: 		return (unsigned int)temp;
UXTH	R0, R2
;RGB_PWM_Android.c,19 :: 		}
L_end_pwm_8bits:
BX	LR
; end of _pwm_8bits
_U3Rx_Int:
;RGB_PWM_Android.c,22 :: 		void U3Rx_Int() iv IVT_INT_USART3 ics ICS_AUTO
;RGB_PWM_Android.c,24 :: 		USART3_SRbits.RXNE = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(USART3_SRbits+0)
MOVT	R0, #hi_addr(USART3_SRbits+0)
STR	R1, [R0, #0]
;RGB_PWM_Android.c,25 :: 		*pRx_Str = USART3_DR;
MOVW	R0, #lo_addr(USART3_DR+0)
MOVT	R0, #hi_addr(USART3_DR+0)
LDR	R2, [R0, #0]
MOVW	R1, #lo_addr(_pRx_Str+0)
MOVT	R1, #hi_addr(_pRx_Str+0)
LDR	R0, [R1, #0]
STRB	R2, [R0, #0]
;RGB_PWM_Android.c,26 :: 		if(*pRx_Str == '.')                // End character
MOV	R0, R1
LDR	R0, [R0, #0]
LDRB	R0, [R0, #0]
CMP	R0, #46
IT	NE
BNE	L_U3Rx_Int0
;RGB_PWM_Android.c,28 :: 		Rx_Flag = 1;                      // Set Flag
MOVS	R1, #1
MOVW	R0, #lo_addr(_Rx_Flag+0)
MOVT	R0, #hi_addr(_Rx_Flag+0)
STRB	R1, [R0, #0]
;RGB_PWM_Android.c,29 :: 		USART3_CR1bits.RXNEIE = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(USART3_CR1bits+0)
MOVT	R0, #hi_addr(USART3_CR1bits+0)
STR	R1, [R0, #0]
;RGB_PWM_Android.c,30 :: 		}
L_U3Rx_Int0:
;RGB_PWM_Android.c,31 :: 		pRx_Str++;
MOVW	R1, #lo_addr(_pRx_Str+0)
MOVT	R1, #hi_addr(_pRx_Str+0)
LDR	R0, [R1, #0]
ADDS	R0, R0, #1
STR	R0, [R1, #0]
;RGB_PWM_Android.c,32 :: 		}
L_end_U3Rx_Int:
BX	LR
; end of _U3Rx_Int
_main:
;RGB_PWM_Android.c,34 :: 		void main()
SUB	SP, SP, #8
;RGB_PWM_Android.c,39 :: 		UART3_Init_Advanced(9600, _UART_8_BIT_DATA, _UART_NOPARITY, _UART_ONE_STOPBIT, &_GPIO_MODULE_USART3_PD89);
MOVW	R0, #lo_addr(__GPIO_MODULE_USART3_PD89+0)
MOVT	R0, #hi_addr(__GPIO_MODULE_USART3_PD89+0)
PUSH	(R0)
MOVW	R3, #0
MOVW	R2, #0
MOVW	R1, #0
MOVW	R0, #9600
BL	_UART3_Init_Advanced+0
ADD	SP, SP, #4
;RGB_PWM_Android.c,41 :: 		pwm_ratio = PWM_TIM4_Init(1000);
MOVW	R0, #1000
BL	_PWM_TIM4_Init+0
STRH	R0, [SP, #4]
;RGB_PWM_Android.c,43 :: 		PWM_TIM4_Set_Duty(pwm_8bits(pwm_ratio,0) , _PWM_NON_INVERTED, _PWM_CHANNEL1);
MOVS	R1, #0
BL	_pwm_8bits+0
MOVS	R2, #0
MOVS	R1, #0
BL	_PWM_TIM4_Set_Duty+0
;RGB_PWM_Android.c,44 :: 		PWM_TIM4_Set_Duty(pwm_8bits(pwm_ratio,0) , _PWM_NON_INVERTED, _PWM_CHANNEL2);
MOVS	R1, #0
LDRH	R0, [SP, #4]
BL	_pwm_8bits+0
MOVS	R2, #1
MOVS	R1, #0
BL	_PWM_TIM4_Set_Duty+0
;RGB_PWM_Android.c,45 :: 		PWM_TIM4_Set_Duty(pwm_8bits(pwm_ratio,0) , _PWM_NON_INVERTED, _PWM_CHANNEL3);
MOVS	R1, #0
LDRH	R0, [SP, #4]
BL	_pwm_8bits+0
MOVS	R2, #2
MOVS	R1, #0
BL	_PWM_TIM4_Set_Duty+0
;RGB_PWM_Android.c,47 :: 		PWM_TIM4_Start(_PWM_CHANNEL1, &_GPIO_MODULE_TIM4_CH1_PD12);
MOVW	R1, #lo_addr(__GPIO_MODULE_TIM4_CH1_PD12+0)
MOVT	R1, #hi_addr(__GPIO_MODULE_TIM4_CH1_PD12+0)
MOVS	R0, #0
BL	_PWM_TIM4_Start+0
;RGB_PWM_Android.c,48 :: 		PWM_TIM4_Start(_PWM_CHANNEL2, &_GPIO_MODULE_TIM4_CH2_PD13);
MOVW	R1, #lo_addr(__GPIO_MODULE_TIM4_CH2_PD13+0)
MOVT	R1, #hi_addr(__GPIO_MODULE_TIM4_CH2_PD13+0)
MOVS	R0, #1
BL	_PWM_TIM4_Start+0
;RGB_PWM_Android.c,49 :: 		PWM_TIM4_Start(_PWM_CHANNEL3, &_GPIO_MODULE_TIM4_CH3_PD14);
MOVW	R1, #lo_addr(__GPIO_MODULE_TIM4_CH3_PD14+0)
MOVT	R1, #hi_addr(__GPIO_MODULE_TIM4_CH3_PD14+0)
MOVS	R0, #2
BL	_PWM_TIM4_Start+0
;RGB_PWM_Android.c,51 :: 		memset(UART_Rx_Str,0,32);
MOVS	R2, #32
SXTH	R2, R2
MOVS	R1, #0
MOVW	R0, #lo_addr(_UART_Rx_Str+0)
MOVT	R0, #hi_addr(_UART_Rx_Str+0)
BL	_memset+0
;RGB_PWM_Android.c,52 :: 		pRx_Str = &UART_Rx_Str;
MOVW	R1, #lo_addr(_UART_Rx_Str+0)
MOVT	R1, #hi_addr(_UART_Rx_Str+0)
MOVW	R0, #lo_addr(_pRx_Str+0)
MOVT	R0, #hi_addr(_pRx_Str+0)
STR	R1, [R0, #0]
;RGB_PWM_Android.c,54 :: 		USART3_SRbits.RXNE = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(USART3_SRbits+0)
MOVT	R0, #hi_addr(USART3_SRbits+0)
STR	R1, [R0, #0]
;RGB_PWM_Android.c,55 :: 		USART3_CR1bits.RXNEIE = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(USART3_CR1bits+0)
MOVT	R0, #hi_addr(USART3_CR1bits+0)
STR	R1, [R0, #0]
;RGB_PWM_Android.c,56 :: 		NVIC_IntEnable(IVT_INT_USART3);
MOVW	R0, #55
BL	_NVIC_IntEnable+0
;RGB_PWM_Android.c,57 :: 		Enableinterrupts();
BL	_EnableInterrupts+0
;RGB_PWM_Android.c,59 :: 		while(1)
L_main1:
;RGB_PWM_Android.c,61 :: 		if(Rx_Flag == 1)
MOVW	R0, #lo_addr(_Rx_Flag+0)
MOVT	R0, #hi_addr(_Rx_Flag+0)
LDRB	R0, [R0, #0]
CMP	R0, #1
IT	NE
BNE	L_main3
;RGB_PWM_Android.c,63 :: 		Rval = ((UART_Rx_Str[5] - 0x30) * 100) + ((UART_Rx_Str[6] - 0x30) * 10) + (UART_Rx_Str[7] - 0x30);
MOVW	R0, #lo_addr(_UART_Rx_Str+5)
MOVT	R0, #hi_addr(_UART_Rx_Str+5)
LDRB	R0, [R0, #0]
SUBW	R1, R0, #48
SXTH	R1, R1
MOVS	R0, #100
SXTH	R0, R0
MUL	R2, R1, R0
SXTH	R2, R2
MOVW	R0, #lo_addr(_UART_Rx_Str+6)
MOVT	R0, #hi_addr(_UART_Rx_Str+6)
LDRB	R0, [R0, #0]
SUBW	R1, R0, #48
SXTH	R1, R1
MOVS	R0, #10
SXTH	R0, R0
MULS	R0, R1, R0
SXTH	R0, R0
ADDS	R1, R2, R0
SXTH	R1, R1
MOVW	R0, #lo_addr(_UART_Rx_Str+7)
MOVT	R0, #hi_addr(_UART_Rx_Str+7)
LDRB	R0, [R0, #0]
SUBS	R0, #48
SXTH	R0, R0
ADDS	R0, R1, R0
STRB	R0, [SP, #0]
;RGB_PWM_Android.c,64 :: 		Gval = ((UART_Rx_Str[9] - 0x30) * 100) + ((UART_Rx_Str[10] - 0x30) * 10) + (UART_Rx_Str[11] - 0x30);
MOVW	R0, #lo_addr(_UART_Rx_Str+9)
MOVT	R0, #hi_addr(_UART_Rx_Str+9)
LDRB	R0, [R0, #0]
SUBW	R1, R0, #48
SXTH	R1, R1
MOVS	R0, #100
SXTH	R0, R0
MUL	R2, R1, R0
SXTH	R2, R2
MOVW	R0, #lo_addr(_UART_Rx_Str+10)
MOVT	R0, #hi_addr(_UART_Rx_Str+10)
LDRB	R0, [R0, #0]
SUBW	R1, R0, #48
SXTH	R1, R1
MOVS	R0, #10
SXTH	R0, R0
MULS	R0, R1, R0
SXTH	R0, R0
ADDS	R1, R2, R0
SXTH	R1, R1
MOVW	R0, #lo_addr(_UART_Rx_Str+11)
MOVT	R0, #hi_addr(_UART_Rx_Str+11)
LDRB	R0, [R0, #0]
SUBS	R0, #48
SXTH	R0, R0
ADDS	R0, R1, R0
STRB	R0, [SP, #1]
;RGB_PWM_Android.c,65 :: 		Bval = ((UART_Rx_Str[13] - 0x30) * 100) + ((UART_Rx_Str[14] - 0x30) * 10) + (UART_Rx_Str[15] - 0x30);
MOVW	R0, #lo_addr(_UART_Rx_Str+13)
MOVT	R0, #hi_addr(_UART_Rx_Str+13)
LDRB	R0, [R0, #0]
SUBW	R1, R0, #48
SXTH	R1, R1
MOVS	R0, #100
SXTH	R0, R0
MUL	R2, R1, R0
SXTH	R2, R2
MOVW	R0, #lo_addr(_UART_Rx_Str+14)
MOVT	R0, #hi_addr(_UART_Rx_Str+14)
LDRB	R0, [R0, #0]
SUBW	R1, R0, #48
SXTH	R1, R1
MOVS	R0, #10
SXTH	R0, R0
MULS	R0, R1, R0
SXTH	R0, R0
ADDS	R1, R2, R0
SXTH	R1, R1
MOVW	R0, #lo_addr(_UART_Rx_Str+15)
MOVT	R0, #hi_addr(_UART_Rx_Str+15)
LDRB	R0, [R0, #0]
SUBS	R0, #48
SXTH	R0, R0
ADDS	R0, R1, R0
STRB	R0, [SP, #2]
;RGB_PWM_Android.c,67 :: 		PWM_TIM4_Set_Duty(pwm_8bits(pwm_ratio,Rval), _PWM_NON_INVERTED, _PWM_CHANNEL1);
LDRB	R1, [SP, #0]
LDRH	R0, [SP, #4]
BL	_pwm_8bits+0
MOVS	R2, #0
MOVS	R1, #0
BL	_PWM_TIM4_Set_Duty+0
;RGB_PWM_Android.c,68 :: 		PWM_TIM4_Set_Duty(pwm_8bits(pwm_ratio,Gval), _PWM_NON_INVERTED, _PWM_CHANNEL2);
LDRB	R1, [SP, #1]
LDRH	R0, [SP, #4]
BL	_pwm_8bits+0
MOVS	R2, #1
MOVS	R1, #0
BL	_PWM_TIM4_Set_Duty+0
;RGB_PWM_Android.c,69 :: 		PWM_TIM4_Set_Duty(pwm_8bits(pwm_ratio,Bval), _PWM_NON_INVERTED, _PWM_CHANNEL3);
LDRB	R1, [SP, #2]
LDRH	R0, [SP, #4]
BL	_pwm_8bits+0
MOVS	R2, #2
MOVS	R1, #0
BL	_PWM_TIM4_Set_Duty+0
;RGB_PWM_Android.c,71 :: 		memset(UART_Rx_Str,0,32);
MOVS	R2, #32
SXTH	R2, R2
MOVS	R1, #0
MOVW	R0, #lo_addr(_UART_Rx_Str+0)
MOVT	R0, #hi_addr(_UART_Rx_Str+0)
BL	_memset+0
;RGB_PWM_Android.c,72 :: 		pRx_Str = &UART_Rx_Str;
MOVW	R1, #lo_addr(_UART_Rx_Str+0)
MOVT	R1, #hi_addr(_UART_Rx_Str+0)
MOVW	R0, #lo_addr(_pRx_Str+0)
MOVT	R0, #hi_addr(_pRx_Str+0)
STR	R1, [R0, #0]
;RGB_PWM_Android.c,73 :: 		Rx_Flag = 0;                     // Reset Flag
MOVS	R1, #0
MOVW	R0, #lo_addr(_Rx_Flag+0)
MOVT	R0, #hi_addr(_Rx_Flag+0)
STRB	R1, [R0, #0]
;RGB_PWM_Android.c,74 :: 		USART3_SRbits.RXNE = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(USART3_SRbits+0)
MOVT	R0, #hi_addr(USART3_SRbits+0)
STR	R1, [R0, #0]
;RGB_PWM_Android.c,75 :: 		USART3_CR1bits.RXNEIE = 1;       // Enable Interrupt
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(USART3_CR1bits+0)
MOVT	R0, #hi_addr(USART3_CR1bits+0)
STR	R1, [R0, #0]
;RGB_PWM_Android.c,76 :: 		}
L_main3:
;RGB_PWM_Android.c,77 :: 		}
IT	AL
BAL	L_main1
;RGB_PWM_Android.c,79 :: 		}
L_end_main:
L__main_end_loop:
B	L__main_end_loop
; end of _main
