#line 1 "E:/Electro/MikroC_ARM/EasyMX_STM32/STM32F107VCT6/RGB_PWM_Android/RGB_PWM_Android.c"



sbit Red at GPIOD_ODR.B12;
sbit Green at GPIOD_ODR.B13;
sbit Blue at GPIOD_ODR.B14;


volatile char *pTx_Str,*pRx_Str;
volatile unsigned short Rx_Flag=0;
volatile char UART_Rx_Str[32];


unsigned int pwm_8bits(unsigned int ratio, unsigned short byte)
{
 unsigned long temp;
 temp = ((unsigned long) ratio * (255 - byte)) / 255;
 return (unsigned int)temp;
}


void U3Rx_Int() iv IVT_INT_USART3 ics ICS_AUTO
{
 USART3_SRbits.RXNE = 0;
 *pRx_Str = USART3_DR;
 if(*pRx_Str == '.')
 {
 Rx_Flag = 1;
 USART3_CR1bits.RXNEIE = 0;
 }
 pRx_Str++;
}

void main()
{
 unsigned short Rval,Gval,Bval;
 unsigned int pwm_ratio;

 UART3_Init_Advanced(9600, _UART_8_BIT_DATA, _UART_NOPARITY, _UART_ONE_STOPBIT, &_GPIO_MODULE_USART3_PD89);

 pwm_ratio = PWM_TIM4_Init(1000);

 PWM_TIM4_Set_Duty(pwm_8bits(pwm_ratio,0) , _PWM_NON_INVERTED, _PWM_CHANNEL1);
 PWM_TIM4_Set_Duty(pwm_8bits(pwm_ratio,0) , _PWM_NON_INVERTED, _PWM_CHANNEL2);
 PWM_TIM4_Set_Duty(pwm_8bits(pwm_ratio,0) , _PWM_NON_INVERTED, _PWM_CHANNEL3);

 PWM_TIM4_Start(_PWM_CHANNEL1, &_GPIO_MODULE_TIM4_CH1_PD12);
 PWM_TIM4_Start(_PWM_CHANNEL2, &_GPIO_MODULE_TIM4_CH2_PD13);
 PWM_TIM4_Start(_PWM_CHANNEL3, &_GPIO_MODULE_TIM4_CH3_PD14);

 memset(UART_Rx_Str,0,32);
 pRx_Str = &UART_Rx_Str;

 USART3_SRbits.RXNE = 0;
 USART3_CR1bits.RXNEIE = 1;
 NVIC_IntEnable(IVT_INT_USART3);
 Enableinterrupts();

 while(1)
 {
 if(Rx_Flag == 1)
 {
 Rval = ((UART_Rx_Str[5] - 0x30) * 100) + ((UART_Rx_Str[6] - 0x30) * 10) + (UART_Rx_Str[7] - 0x30);
 Gval = ((UART_Rx_Str[9] - 0x30) * 100) + ((UART_Rx_Str[10] - 0x30) * 10) + (UART_Rx_Str[11] - 0x30);
 Bval = ((UART_Rx_Str[13] - 0x30) * 100) + ((UART_Rx_Str[14] - 0x30) * 10) + (UART_Rx_Str[15] - 0x30);

 PWM_TIM4_Set_Duty(pwm_8bits(pwm_ratio,Rval), _PWM_NON_INVERTED, _PWM_CHANNEL1);
 PWM_TIM4_Set_Duty(pwm_8bits(pwm_ratio,Gval), _PWM_NON_INVERTED, _PWM_CHANNEL2);
 PWM_TIM4_Set_Duty(pwm_8bits(pwm_ratio,Bval), _PWM_NON_INVERTED, _PWM_CHANNEL3);

 memset(UART_Rx_Str,0,32);
 pRx_Str = &UART_Rx_Str;
 Rx_Flag = 0;
 USART3_SRbits.RXNE = 0;
 USART3_CR1bits.RXNEIE = 1;
 }
 }

}
