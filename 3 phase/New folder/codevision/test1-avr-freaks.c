/*****************************************************
This program was produced by the
CodeWizardAVR V2.05.3 Standard
Automatic Program Generator
� Copyright 1998-2011 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : 
Version : 
Date    : 01/27/2016
Author  : Hamed
Company : Elasa Co
Comments: 


Chip type               : ATmega128
Program type            : Application
AVR Core Clock frequency: 8.000000 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 1024
*****************************************************/

#include <mega128.h>

/* Includes ------------------------------------------------------------------*/
//#include "stm32f4_discovery.h"
#include <math.h>
//#include "stm32f4xx.h"
/*----------------------------------------------------------------------------*/


/*Initialisation Structures---------------------------------------------------*/
//GPIO_InitTypeDef  GPIO_InitStructure;
//TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
//TIM_OCInitTypeDef  TIM_OCInitStructure;
/*----------------------------------------------------------------------------*/


/* Global's-------------------------------------------------------------------*/
#define PI 3.14159265358979323846
#define sqrt3  1.73205080756887729353
#define sqrt2 1.414213562
#define PIoverThree 1.047197551196598
#define T 0.00005
#define MAXMODINDEX 2100

volatile int n=0;
volatile int i=0;
volatile float  FREF =50;

float   MAXn;
float  ARRAYa[1000] ;
float  ARRAYb[1000] ;
float  ARRAYc [1000];
//MAXn = 1/(FREF*T);

/*----------------------------------------------------------------------------*/

/* Function prototypes -----------------------------------------------*/
void TIM_Config(void);
void PWM_Config(void);
//void Delay(__IO uint32_t nCount);
void GPIO_Config(void);
void SVPWM(int);
/* --------- ---------------------------------------------------------*/

/* Tim 4 PWM Compare Register Pointer's-------------------------------*/
int *mia =(int*)0x40000834; //CCR1 Pointer
int *mib =(int*)0x40000838; //CCR2 Pointer
int *mic =(int*)0x4000083c; //CCR3 Pointer
int *INTERRUPTCOUNT =(int*)0x40000840; //CCR4 Pointer

/*--------------------------------------------------------------------*/
flash unsigned char sinf[256]={
0x80,0x83,0x86,0x89,0x8c,0x8f,0x92,0x95,0x98,0x9c,0x9f,0xa2,0xa5,0xa8,0xab,0xae,
0xb0,0xb3,0xb6,0xb9,0xbc,0xbf,0xc1,0xc4,0xc7,0xc9,0xcc,0xce,0xd1,0xd3,0xd5,0xd8,
0xda,0xdc,0xde,0xe0,0xe2,0xe4,0xe6,0xe8,0xea,0xec,0xed,0xef,0xf0,0xf2,0xf3,0xf5,
0xf6,0xf7,0xf8,0xf9,0xfa,0xfb,0xfc,0xfc,0xfd,0xfe,0xfe,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xff,0xfe,0xfe,0xfd,0xfc,0xfc,0xfb,0xfa,0xf9,0xf8,0xf7,
0xf6,0xf5,0xf3,0xf2,0xf0,0xef,0xed,0xec,0xea,0xe8,0xe6,0xe4,0xe2,0xe0,0xde,0xdc,
0xda,0xd8,0xd5,0xd3,0xd1,0xce,0xcc,0xc9,0xc7,0xc4,0xc1,0xbf,0xbc,0xb9,0xb6,0xb3,
0xb0,0xae,0xab,0xa8,0xa5,0xa2,0x9f,0x9c,0x98,0x95,0x92,0x8f,0x8c,0x89,0x86,0x83,
0x80,0x7c,0x79,0x76,0x73,0x70,0x6d,0x6a,0x67,0x63,0x60,0x5d,0x5a,0x57,0x54,0x51,
0x4f,0x4c,0x49,0x46,0x43,0x40,0x3e,0x3b,0x38,0x36,0x33,0x31,0x2e,0x2c,0x2a,0x27,
0x25,0x23,0x21,0x1f,0x1d,0x1b,0x19,0x17,0x15,0x13,0x12,0x10,0x0f,0x0d,0x0c,0x0a,
0x09,0x08,0x07,0x06,0x05,0x04,0x03,0x03,0x02,0x01,0x01,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x02,0x03,0x03,0x04,0x05,0x06,0x07,0x08,
0x09,0x0a,0x0c,0x0d,0x0f,0x10,0x12,0x13,0x15,0x17,0x19,0x1b,0x1d,0x1f,0x21,0x23,
0x25,0x27,0x2a,0x2c,0x2e,0x31,0x33,0x36,0x38,0x3b,0x3e,0x40,0x43,0x46,0x49,0x4c,
0x4f,0x51,0x54,0x57,0x5a,0x5d,0x60,0x63,0x67,0x6a,0x6d,0x70,0x73,0x76,0x79,0x7c};


/*Interrupt Handler on Tim 4 Capture Compare 4 Register-----------------------*/
interrupt [TIM2_COMP] void timer2_compare_isr(void)
{
 int ZZZ,YYY; 
  //if (TIM_GetITStatus(TIM4, TIM_IT_CC4) != RESET)
  if (1)
  {
      //GPIO_ToggleBits(GPIOB, GPIO_Pin_11);
      SVPWM(n);
      n=n+1;
      MAXn=1.0/(FREF*T);
       ZZZ = MAXn;
   if(n>=MAXn){
         n=0;
          YYY=*mia;

     }

      //TIM_ClearITPendingBit(TIM4, TIM_IT_CC4);
  }
}
/*--------------------------------------------------------------------*/


void SVPWM(int count){

float Vr = sqrt3/2;
float THETA= 2*PI*FREF*count*T;
float THETADEGREES =THETA*57.29577951;

int   SECTOR = THETA/(PI/3);
//GPIO_SetBits(GPIOB, GPIO_Pin_11);
float Ua = 1*(FREF/50)*sinf((PI/3)-(THETA-SECTOR*(PI/3)));
float Ub = 1*(FREF/50)*sinf(THETA-SECTOR*(PI/3));
float Uc = 1.0-Ua-Ub;

switch(SECTOR)
    {
    case 0:       *mia = MAXMODINDEX*(Ua + Ub + 0.5*Uc);
                  *mib = MAXMODINDEX*(Ub + 0.5*Uc);
                  *mic = MAXMODINDEX*(0.5*Uc);
        break;

    case 1:
                  *mia = MAXMODINDEX*(Ua + 0.5*Uc);
                  *mib = MAXMODINDEX*(Ua + Ub + 0.5*Uc);
                  *mic = MAXMODINDEX*(0.5*Uc);
        break;

    case 2:       *mia = MAXMODINDEX*(0.5*Uc);
                  *mib = MAXMODINDEX*(Ua + Ub + 0.5*Uc);
                  *mic = MAXMODINDEX*(Ub + 0.5*Uc);

        break;

    case 3:       *mia = MAXMODINDEX*(0.5*Uc);
                  *mib = MAXMODINDEX*(Ua + 0.5*Uc);
                  *mic = MAXMODINDEX*(Ua + Ub + 0.5*Uc);

        break;

    case 4:       *mia = MAXMODINDEX*(Ub + 0.5*Uc);
                  *mib = MAXMODINDEX*(0.5*Uc);
                  *mic = MAXMODINDEX*(Ua + Ub + 0.5*Uc);

        break;

    case 5:       *mia = MAXMODINDEX*(Ua + Ub + 0.5*Uc);
                  *mib = MAXMODINDEX*(0.5*Uc);
                  *mic = MAXMODINDEX*(Ua + 0.5*Uc);

        break;


    default: break;
        }

//GPIO_ResetBits(GPIOB, GPIO_Pin_11);
}



void  main(void)
{
int MIA=*mia;
int MIB=*mib;
int MIC=*mic;
int pulse_width;
//float  ARRAYa [1000]={};
//float  ARRAYb [1000]={};
//float  ARRAYc [1000]={};
   *INTERRUPTCOUNT=2100;

  //uint16_t pulse_width = 0;
  pulse_width = 0;

  GPIO_Config();
  TIM_Config();
  PWM_Config();
 // GPIO_ResetBits(GPIOB, GPIO_Pin_11| GPIO_Pin_13| GPIO_Pin_15);


   
  while (1)
  {
      /*Debug Arrays----------------------------------------------------------------*/
      #if 1
      ARRAYa[n]= *mia;
      ARRAYb[n]= *mib;
      ARRAYc[n]= *mic;
      #endif
      /*--------------------------------------------------------------------*/

  }
}


