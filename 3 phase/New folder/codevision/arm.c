/* Includes ------------------------------------------------------------------*/
#include "stm32f4_discovery.h"
#include <math.h>
#include "stm32f4xx.h"
/*----------------------------------------------------------------------------*/


/*Initialisation Structures---------------------------------------------------*/
GPIO_InitTypeDef  GPIO_InitStructure;
TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
TIM_OCInitTypeDef  TIM_OCInitStructure;
/*----------------------------------------------------------------------------*/


/* Global's-------------------------------------------------------------------*/
#define PI 3.14159265358979323846
#define sqrt3  1.73205080756887729353
#define sqrt2 1.414213562
#define PIoverThree 1.047197551196598
#define T 0.00005
#define MAXMODINDEX 2100

volatile int n=0;
volatile int i=0;
volatile float  FREF =50;

float   MAXn;

//MAXn = 1/(FREF*T);

/*----------------------------------------------------------------------------*/

/* Function prototypes -----------------------------------------------*/
void TIM_Config(void);
void PWM_Config(void);
void Delay(__IO uint32_t nCount);
void GPIO_Config(void);
void SVPWM(int);
/* --------- ---------------------------------------------------------*/

/* Tim 4 PWM Compare Register Pointer's-------------------------------*/
int *mia =(int*)0x40000834; //CCR1 Pointer
int *mib =(int*)0x40000838; //CCR2 Pointer
int *mic =(int*)0x4000083c; //CCR3 Pointer
int *INTERRUPTCOUNT =(int*)0x40000840; //CCR4 Pointer

/*--------------------------------------------------------------------*/


/*Interrupt Handler on Tim 4 Capture Compare 4 Register-----------------------*/
void TIM4_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM4, TIM_IT_CC4) != RESET)
  {
	  //GPIO_ToggleBits(GPIOB, GPIO_Pin_11);
      SVPWM(n);
      n=n+1;
      MAXn=1.0/(FREF*T);
      int ZZZ = MAXn;
   if(n>=MAXn){
    	 n=0;
    	 int YYY=*mia;

     }

      TIM_ClearITPendingBit(TIM4, TIM_IT_CC4);
  }
}
/*--------------------------------------------------------------------*/

int main(void)
{
  uint16_t pulse_width = 0;

  GPIO_Config();
  TIM_Config();
  PWM_Config();
  GPIO_ResetBits(GPIOB, GPIO_Pin_11| GPIO_Pin_13| GPIO_Pin_15);

int MIA=*mia;
int MIB=*mib;
int MIC=*mic;
   *INTERRUPTCOUNT=2100;
   float  ARRAYa [1000]={};
   float  ARRAYb [1000]={};
   float  ARRAYc [1000]={};
  while (1)
  {
	  /*Debug Arrays----------------------------------------------------------------*/
	  #if 1
	  ARRAYa[n]= *mia;
	  ARRAYb[n]= *mib;
	  ARRAYc[n]= *mic;
	  #endif
	  /*--------------------------------------------------------------------*/

  }
}

void SVPWM(count){

float Vr = sqrt3/2;
float THETA= 2*PI*FREF*count*T;
float THETADEGREES =THETA*57.29577951;

int   SECTOR = THETA/(PI/3);
GPIO_SetBits(GPIOB, GPIO_Pin_11);
float Ua = 1*(FREF/50)*sinf((PI/3)-(THETA-SECTOR*(PI/3)));
float Ub = 1*(FREF/50)*sinf(THETA-SECTOR*(PI/3));
float Uc = 1.0-Ua-Ub;

switch(SECTOR)
	{
	case 0:       *mia = MAXMODINDEX*(Ua + Ub + 0.5*Uc);
	              *mib = MAXMODINDEX*(Ub + 0.5*Uc);
	              *mic = MAXMODINDEX*(0.5*Uc);
		break;

	case 1:
		          *mia = MAXMODINDEX*(Ua + 0.5*Uc);
			      *mib = MAXMODINDEX*(Ua + Ub + 0.5*Uc);
			      *mic = MAXMODINDEX*(0.5*Uc);
		break;

	case 2:       *mia = MAXMODINDEX*(0.5*Uc);
                  *mib = MAXMODINDEX*(Ua + Ub + 0.5*Uc);
                  *mic = MAXMODINDEX*(Ub + 0.5*Uc);

		break;

	case 3:       *mia = MAXMODINDEX*(0.5*Uc);
                  *mib = MAXMODINDEX*(Ua + 0.5*Uc);
                  *mic = MAXMODINDEX*(Ua + Ub + 0.5*Uc);

		break;

	case 4:       *mia = MAXMODINDEX*(Ub + 0.5*Uc);
                  *mib = MAXMODINDEX*(0.5*Uc);
                  *mic = MAXMODINDEX*(Ua + Ub + 0.5*Uc);

		break;

	case 5:       *mia = MAXMODINDEX*(Ua + Ub + 0.5*Uc);
                  *mib = MAXMODINDEX*(0.5*Uc);
                  *mic = MAXMODINDEX*(Ua + 0.5*Uc);

		break;


	default: break;
        }

GPIO_ResetBits(GPIOB, GPIO_Pin_11);
}



void GPIO_Config(void)

{    /* GPIOD clock enable */
	  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	/* GPIOD For PWM Configuration:  TIM4 CH1 (PD12), TIM4 CH2 (PD13), TIM4 CH3 (PD14), TIM4 CH4 (PD15) */
	  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12| GPIO_Pin_13| GPIO_Pin_14| GPIO_Pin_15;
	  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	  GPIO_Init(GPIOD, &GPIO_InitStructure);

	  /* Connect TIM4 pins to AF2 */
	  GPIO_PinAFConfig(GPIOD, GPIO_PinSource12, GPIO_AF_TIM3);
	  GPIO_PinAFConfig(GPIOD, GPIO_PinSource13, GPIO_AF_TIM3);
	  GPIO_PinAFConfig(GPIOD, GPIO_PinSource14, GPIO_AF_TIM3);
	  GPIO_PinAFConfig(GPIOD, GPIO_PinSource15, GPIO_AF_TIM3);


  /* GPIOB Periph clock enable */
      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

  /* Configure  PB11, PB13 and PB15 in output pushpull mode */
      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
      GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_13| GPIO_Pin_15;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
      GPIO_Init(GPIOB, &GPIO_InitStructure);

}

void TIM_Config(void)
{
	  NVIC_InitTypeDef NVIC_InitStructure;

	  NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
	  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	  NVIC_Init(&NVIC_InitStructure);

	 /* TIM4 clock enable */
	  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

   /* Time base configuration */
	  TIM_TimeBaseStructure.TIM_Prescaler = 0;
	  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_CenterAligned3;
	  TIM_TimeBaseStructure.TIM_Period = 2100;
	  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	  TIM_TimeBaseStructure.TIM_RepetitionCounter=0;
	  TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);

   /* TIM4 IT enable */
	  TIM_ITConfig(TIM4, TIM_IT_CC4, ENABLE);
   /* TIM4 enable counter */
	  TIM_Cmd(TIM4, ENABLE);





}

void PWM_Config(void)
{


  /* PWM1 Mode configuration: */
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = 0;

  /* PWM2 Mode TIM4 CH1 = PD 12 */
  TIM_OC1Init(TIM4, &TIM_OCInitStructure);
  TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);

  /* PWM2 Mode TIM4 CH2 = PD 13 */
  TIM_OC2Init(TIM4, &TIM_OCInitStructure);
  TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Enable);

  /* PWM2 Mode TIM4 CH3 = PD 14 */

  TIM_OC3Init(TIM4, &TIM_OCInitStructure);
  TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);

  /* PWM2 Mode TIM4 CH4 = PD 15 */

  TIM_OC4Init(TIM4, &TIM_OCInitStructure);
  TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);


  TIM_ARRPreloadConfig(TIM4, ENABLE);


  /* TIM4 enable counter */
  TIM_Cmd(TIM4, ENABLE);
}

void Delay(__IO uint32_t nCount)
{
  while(nCount--)
  {
  }
}