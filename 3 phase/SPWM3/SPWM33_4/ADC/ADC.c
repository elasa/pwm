//------------------------------------------------------------------------------
// Copyright:      Pascal Stang ( 30.09.2002 )
// Author:         Pascal Stang - Copyright (C) 2002
// Remarks:        Modified by Sh. Nourbakhsh Rad  at date: 20.10.2008
// known Problems: none
// Version:        2.0.0
// Description:    Analog-to-Digital converter functions for Atmel AVR series
//------------------------------------------------------------------------------

#include "ADC.h"


//! Software flag used to indicate when
/// the a2d conversion is complete.
volatile unsigned char 								a2dCompleteFlag;

// functions

// initialize a2d converter
void a2dInit(void)
{
	sbi(ADCSR, ADEN);																						// enable ADC (turn on ADC power)
	cbi(ADCSR, ADFR);																						// default to single sample convert mode
	a2dSetPrescaler(ADC_PRESCALE);															// set default prescaler
	a2dSetReference(ADC_REFERENCE);															// set default reference
	cbi(ADMUX, ADLAR);																					// set to right-adjusted result

	sbi(ADCSR, ADIE);																						// enable ADC interrupts

	a2dCompleteFlag = False;																		// clear conversion complete flag
	sei();																											// turn on interrupts (if not already on)
}

// turn off a2d converter
void a2dOff(void)
{
	cbi(ADCSR, ADIE);																						// disable ADC interrupts
	cbi(ADCSR, ADEN);																						// disable ADC (turn off ADC power)
}

// configure A2D converter clock division (prescaling)
void a2dSetPrescaler(unsigned char prescale)
{
	outb(ADCSR, ((inb(ADCSR) & ~ADC_PRESCALE_MASK) | prescale));
}

// configure A2D converter voltage reference
void a2dSetReference(unsigned char ref)
{
	outb(ADMUX, ((inb(ADMUX) & ~ADC_REFERENCE_MASK) | (ref<<6)));
}

// sets the a2d input channel
void a2dSetChannel(unsigned char ch)
{
	outb(ADMUX, (inb(ADMUX) & ~ADC_MUX_MASK) | (ch & ADC_MUX_MASK));			// set channel
}

// start a conversion on the current a2d input channel
void a2dStartConvert(void)
{
	sbi(ADCSR, ADIF);																						// clear hardware "conversion complete" flag 
	sbi(ADCSR, ADSC);																						// start conversion
}

// return True if conversion is complete
unsigned char a2dIsComplete(void)
{
	return bit_is_set(ADCSR, ADSC);
}

// Perform a 10-bit conversion
// starts conversion, waits until conversion is done, and returns result
unsigned short a2dConvert10bit(unsigned char ch)
{
	#if (FastRead ==1)
		sbi(ADCSR, ADIF);																										// clear hardware "conversion complete" flag 
		sbi(ADCSR, ADSC);																										// start conversion
		while(bit_is_set(ADCSR, ADSC));																			// wait until conversion complete
		
	#else
		a2dCompleteFlag = False;																						// clear conversion complete flag
		outb(ADMUX, (inb(ADMUX) & ~ADC_MUX_MASK) | (ch & ADC_MUX_MASK));		// set channel
		
		sbi(ADCSR, ADIF);																										// clear hardware "conversion complete" flag 
		sbi(ADCSR, ADSC);																										// start conversion
		while(!a2dCompleteFlag);																						// wait until conversion complete

	#endif
	
	return (inb(ADCW));																										// read ADC (full 10 bits);
}

// Perform a 8-bit conversion.
// starts conversion, waits until conversion is done, and returns result
unsigned char a2dConvert8bit(unsigned char ch)
{
	// do 10-bit conversion and return highest 8 bits
	return a2dConvert10bit(ch)>>2;																				// return ADC MSB byte
}

//! Interrupt handler for ADC complete interrupt.
ISR_ADC_COMPLETE()
{
	// set the a2d conversion flag to indicate "complete"
	a2dCompleteFlag = True;
}
