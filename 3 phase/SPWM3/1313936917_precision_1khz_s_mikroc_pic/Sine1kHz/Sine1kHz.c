/******************************************************************************
	Sine1kHz.c     Precision 1kHz sinewave generator
  9th Jun 2011 - Open Source - www.RomanBlack.com/onesec/High_Acc_Timing.htm

  Method to generate sinewave; PWM module with TMR2 at 100 ticks (20uS)
  giving 100 possible PWM resolution steps. However sine table goes from
  14% to 86% PWM or a total height range of 73 steps. Sine table has 50
  entries over one full 1kHz cycle (20uS per entry). Because the PIC and
  the sine math table produce a sinewave with good basic shape it can be
  easily filtered by a passive RC filter to make a sinewave with xtal locked
  freq accuracy and very low sine distortion.

  Note! The sine table has been modified to include compensation for the
  2nd harmonic error caused by standard left-aligned PWM. This sine table
  will now produce an analog sine after filtering which has no harmonic
  distortion (apart from that caused by samplerate). Please see my web page
  (above) for details of the sine and the filter.

  PIC 16F628 - 20MHz xtal (trimmer cap adjusted to exactly 20MHz)
  PIC pins;
   RB0 in = switch, cal mode = low, using internal RB0 pullup resistor
   RB1 out = RED LED, on = HI, calibrate mode
   RB2 out = GREEN LED, on = HI, sine mode
   RB3 out = PWM out (uses external RC filter to make sinewave out)

_BODEN_OFF _PWRTE_ON _WDT_OFF _LVP_OFF _MCLRE_OFF _HS_OSC
******************************************************************************/
// global vars
unsigned char pwmstep;      // the PWM step 0-49
unsigned char pwmval;       // the PWM value to be used for this step
unsigned char debounce;     // used for debouncing switch input

// this is the 50 entry "harmonic compensated" sine table (updated 20th Jun 2011)
const unsigned char sine[50] = {52,57,62,66,70,74,77,80,82,84,85,86,86,
                                   86,85,83,81,78,75,72,69,65,61,56,52,
                                48,44,39,35,31,28,25,22,19,17,15,14,14,
                                   14,15,16,18,20,23,26,30,34,38,43,48};


//=============================================================================
//  MAIN
//=============================================================================
void main()
{
	//--------------------------------------------------------
	// setup the PIC 16F628 pins
	CMCON =  0b00000111;   // comparators OFF, all pins digital
	PORTA =  0b00000000;   //
	TRISA  = 0b00000000;   // PORTA not used
	PORTB =  0b00000000;   // 
	TRISB  = 0b00000001;   // RB0 switch, RB1,2 LEDs, RB3 is PWM output 

	//--------------------------------------------------------
	// TMR0 not used, but sets PORTB pullup resistors
	OPTION_REG = 0b00001000;	// TMR0 prescale 1:1, PORTB pullups ON

	// TMR2 used for CCP1 PWM output
	T2CON = 0b00000100;		  // TMR2 ON, postscale 1:1, prescale 1:1
  PR2 = (100-1);          // PR2 set to total PWM period of 100 ticks (20uS)

  // small delay to let PSU stabilise
  Delay_mS(300);
  
  //-------------------------------------------------------
  // setup any variables before main loop
  pwmstep = 0;
  debounce = 0;
   	
  //-------------------------------------------------------
  // SINE MODE! use PWM to make the sine out pin PORTB.F3
  // this requires a RC filter on PORTB.F3, please see the web page;
  // www.RomanBlack.com/onesec/High_Acc_Timing.htm
  sine_mode:
  PORTB = 0b00000100;     // SINE mode LED is ON
  CCP1CON = 0b00001111;   // CCP1 ON, and set to PWM mode
  while(1)
  {   	
    //-------------------------------------------------
    // just sit in loop and load new PWM value every TMR2 cycle
    //-------------------------------------------------
    while(!PIR1.TMR2IF);            // wait for TMR2 cycle to restart
    CCPR1L = pwmval;                // store PWM value for this cycle
    PIR1.TMR2IF = 0;                // clear TMR2 int flag
        
    pwmstep++;                      // inc to next step in sinewave
    if(pwmstep >= 50) pwmstep = 0;  // sine has only 50 steps
    pwmval = sine[pwmstep];         // prepare PWM value ready for next step    

    // also check for switch being low, if so go to calibrate mode
    if(!PORTB.F0)       // if switch is in CAL mode
    {
      debounce++;
      if(debounce > 250) goto cal_mode;   // needs switch low for 5mS continuous
    }
    else debounce = 0;
  }

  //-------------------------------------------------------
  // CALIBRATE MODE! make a 1MHz fixed freq out pin PORTB.F3
  // this uses assembler to get exactly 5 PIC instructions per output cycle,
  // so output squarewave is exactly 1MHz with 40% ON duty.
  cal_mode:
  CCP1CON = 0b00000000;   // CCP1 module is turned OFF
  asm nop;                // time for CCP to stabilise
  PORTB = 0b00000010;     // CAL mode LED is ON
  
  asm {
      ;------------------------------------------
      loop_1MHz:

      bsf PORTB,3         ; output high
      nop                 ;
      bcf PORTB,3         ; output low
      btfsc PORTB,0       ; test switch for SINE mode
      goto done_1MHz      ; switch HI, so exit this mode and go back to SINE
  
      bsf PORTB,3         ; output high
      nop                 ;
      bcf PORTB,3         ; output low
      goto loop_1MHz      ; 
      
      done_1MHz:
  }
  goto sine_mode;   // cal mode is done, back to sine mode
  
}
//-----------------------------------------------------------------------------   	
   	
   	
   	

   	
   	