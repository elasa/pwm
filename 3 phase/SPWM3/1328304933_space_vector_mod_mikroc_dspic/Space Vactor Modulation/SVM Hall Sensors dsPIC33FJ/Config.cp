#line 1 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/Config.c"
#line 1 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/config.h"
#line 5 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/config.h"
void IOMap(void);
void InitADC(void);
void InitMCPWM(void);
void InitIC(void);
void InitTMR1(void);
void InitTMR3(void);
#line 15 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/Config.c"
void IOMap()
{
 NSTDIS_bit = 1;
 AD1PCFGL = 0xFFFF;



 Unlock_IOLOCK();
 PPS_Mapping_NoLock(8,_INPUT ,_FLTA1);
 PPS_Mapping_NoLock(1,_INPUT ,_IC1);
 PPS_Mapping_NoLock(2,_INPUT ,_IC2);
 PPS_Mapping_NoLock(3,_INPUT ,_IC7);
 PPS_Mapping_NoLock(9,_OUTPUT,_C1TX);
 PPS_Mapping_NoLock(7,_INPUT,_CIRX);
 Lock_IOLOCK();



 TRISBbits.TRISB15 = 0;
 TRISBbits.TRISB14 = 0;
 TRISBbits.TRISB13 = 0;
 TRISBbits.TRISB12 = 0;
 TRISBbits.TRISB11 = 0;
 TRISBbits.TRISB10 = 0;
 TRISBbits.TRISB1 = 1;
 TRISBbits.TRISB2 = 1;
 TRISBbits.TRISB3 = 1;
 TRISBbits.TRISB9 = 0;
 TRISAbits.TRISA8 = 1;
}



void InitADC()
{
 AD1PCFGL.PCFG8 = 0;

 AD1CON1bits.FORM = 3;

 AD1CON1bits.SSRC = 3;

 AD1CON1bits.ASAM = 1;

 AD1CON1bits.AD12B = 1;
#line 61 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/Config.c"
 AD1CON2 = 0x0000;

 AD1CON3 = 0x032F;

 AD1CSSL = 0x0000;

 AD1CHS0 = 0x0008;

 IFS0bits.AD1IF = 0;

 IEC0bits.AD1IE = 1;

 AD1CON1bits.ADON = 1;
}



void InitMCPWM(void)
{
 PTPER = (( 20000000 / 20000  - 1) >> 1);

 OVDCON = 0x0000;
 DTCON1 = 0x0028;
 PWMCON1 = 0x0077;

 PDC1 = PTPER;
 PDC2 = PTPER;
 PDC3 = PTPER;
 SEVTCMP = 1;
 PWMCON2 = 0x0F02;
 PTCON = 0x8002;
 FLTACON = 0x0087;
}



void InitIC()
{



 IC1CON = 1;
 IC2CON = 1;
 IC7CON = 1;
 IFS0bits.IC1IF = 0;
 IFS0bits.IC2IF = 0;
 IFS1bits.IC7IF = 0;
}



void InitTMR1(void)
{
 T1CON = 0x0020;
 TMR1 = 0;
 PR1 = 313;
 T1CON.TON = 1;
}



void InitTMR3(void)
{
 T3CON = 0x0020;
 TMR3 = 0;
 PR3 = 0xFFFF;
 T3CON.TON = 1;
}
