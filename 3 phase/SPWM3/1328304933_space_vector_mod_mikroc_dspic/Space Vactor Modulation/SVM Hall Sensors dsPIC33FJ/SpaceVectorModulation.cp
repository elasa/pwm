#line 1 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/SpaceVectorModulation.c"
#line 1 "c:/users/public/documents/mikroelektronika/mikroc pro for dspic/include/built_in.h"
#line 1 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/config.h"
#line 5 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/config.h"
void IOMap(void);
void InitADC(void);
void InitMCPWM(void);
void InitIC(void);
void InitTMR1(void);
void InitTMR3(void);
#line 1 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/isr.h"
#line 5 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/isr.h"
void SpeedControl(void);
void ForceCommutation(void);
void StopMotor(void);
void RunMotor(void);
void ChargeBootstraps(void);
void Delay(unsigned int delay_count);
void SetPID(void);
void debug(void);
#line 1 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/io_can.h"




void InitCAN(void);
void ReadCAN(void);
void WriteCAN(void);
#line 12 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/SpaceVectorModulation.c"
extern struct MotorFlags
{
 unsigned MotorRunning :1;
 unsigned unused :15;
}Flags;









void main()
{
#line 36 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/SpaceVectorModulation.c"
 PLLFBD = 18;
 CLKDIVbits.PLLPOST = 0;
 CLKDIVbits.PLLPRE = 0;



 IOMap();



 InitIC();



 InitADC();



 InitMCPWM();



 InitTMR1();



 InitTMR3(void);



 InitCAN();



 SetPID();



 Flags.MotorRunning = 0;
 while(1)
 {
 if ((! PORTAbits.RA8 ) && (!Flags.MotorRunning))
 {
 Delay(200);
 if(! PORTAbits.RA8 )
 {
 RunMotor();
 while(! PORTAbits.RA8 );
 }
 }
 else if ((! PORTAbits.RA8 ) && (Flags.MotorRunning))
 {
 Delay(200);
 if(! PORTAbits.RA8 )
 {
 StopMotor();
 while(! PORTAbits.RA8 );
 }
 }

 if(Flags.MotorRunning){
 debug();
 WriteCAN();}
 }
}
