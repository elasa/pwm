/*************************************************************************
*  ISR.h
*  ISR header File
**************************************************************************/
void SpeedControl(void);
void ForceCommutation(void);
void StopMotor(void);
void RunMotor(void);
void ChargeBootstraps(void);
void Delay(unsigned int delay_count);
void SetPID(void);
void debug(void);

//---------------------------------------------------------------------------