#line 1 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/SVM.c"
#line 25 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/SVM.c"
const int sinetable[] =
{0,201,401,602,803,1003,1204,1404,1605,1805,
2005,2206,2406,2606,2806,3006,3205,3405,3605,3804,4003,4202,4401,4600,
4799,4997,5195,5393,5591,5789,5986,6183,6380,6577,6773,6970,7166,7361,
7557,7752,7947,8141,8335,8529,8723,8916,9109,9302,9494,9686,9877,10068,
10259,10449,10639,10829,11018,11207,11395,11583,11771,11958,12144,
12331,12516,12701,12886,13070,13254,13437,13620,13802,13984,14165,
14346,14526,14706,14885,15063,15241,15419,15595,15772,15947,16122,
16297,16470,16643,16816,16988,17159,17330,17500,17669,17838,18006,
18173,18340,18506,18671,18835,18999,19162,19325,19487,19647,19808,
19967,20126,20284,20441,20598,20753,20908,21062,21216,21368,21520,
21671,21821,21970,22119,22266,22413,22559,22704,22848,22992,23134,
23276,23417,23557,23696,23834,23971,24107,24243,24377,24511,24644,
24776,24906,25036,25165,25293,25420,25547,25672,25796,25919,26042,
26163,26283,26403,26521,26638,26755,26870,26984,27098,27210,27321,
27431,27541,27649,27756,27862,27967,28071,28174,28276,28377
};
#line 56 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/SVM.c"
void SVM(int volts,unsigned int angle)
{


 volatile unsigned int angle1, angle2;


 volatile unsigned int half_t0,t1,t2,tpwm;



 tpwm = PTPER << 1;


if(volts >  28300 ) volts =  28300 ;

if(angle <  0x2aaa )
 {
 angle2 = angle -  0 ;

 angle1 =  0x2aaa  - angle2;

 t1 = sinetable[(unsigned char)(angle1 >> 6)];

 t2 = sinetable[(unsigned char)(angle2 >> 6)];

 t1 = ((long)t1*(long)volts) >> 15;

 t1 = ((long)t1*(long)tpwm) >> 15;

 t2 = ((long)t2*(long)volts) >> 15;
 t2 = ((long)t2*(long)tpwm) >> 15;
 half_t0 = (tpwm - t1 - t2) >> 1;


 PDC1 = t1 + t2 + half_t0;
 PDC2 = t2 + half_t0;
 PDC3 = half_t0;
 }
else if(angle <  0x5555 )
 {
 angle2 = angle -  0x2aaa ;

 angle1 =  0x2aaa  - angle2;

 t1 = sinetable[(unsigned char)(angle1 >> 6)];

 t2 = sinetable[(unsigned char)(angle2 >> 6)];

 t1 = ((long)t1*(long)volts) >> 15;

 t1 = ((long)t1*(long)tpwm) >> 15;

 t2 = ((long)t2*(long)volts) >> 15;
 t2 = ((long)t2*(long)tpwm) >> 15;
 half_t0 = (tpwm - t1 - t2) >> 1;


 PDC1 = t1 + half_t0;
 PDC2 = t1 + t2 + half_t0;
 PDC3 = half_t0;
 }
else if(angle <  0x8000 )
 {
 angle2 = angle -  0x5555 ;

 angle1 =  0x2aaa  - angle2;

 t1 = sinetable[(unsigned char)(angle1 >> 6)];

 t2 = sinetable[(unsigned char)(angle2 >> 6)];

 t1 = ((long)t1*(long)volts) >> 15;

 t1 = ((long)t1*(long)tpwm) >> 15;

 t2 = ((long)t2*(long)volts) >> 15;
 t2 = ((long)t2*(long)tpwm) >> 15;
 half_t0 = (tpwm - t1 - t2) >> 1;


 PDC1 = half_t0;
 PDC2 = t1 + t2 + half_t0;
 PDC3 = t2 + half_t0;
 }
else if(angle <  0xaaaa )
 {
 angle2 = angle -  0x8000 ;

 angle1 =  0x2aaa  - angle2;

 t1 = sinetable[(unsigned char)(angle1 >> 6)];

 t2 = sinetable[(unsigned char)(angle2 >> 6)];

 t1 = ((long)t1*(long)volts) >> 15;

 t1 = ((long)t1*(long)tpwm) >> 15;

 t2 = ((long)t2*(long)volts) >> 15;
 t2 = ((long)t2*(long)tpwm) >> 15;
 half_t0 = (tpwm - t1 - t2) >> 1;


 PDC1 = half_t0;
 PDC2 = t1 + half_t0;
 PDC3 = t1 + t2 + half_t0;
 }
else if(angle <  0xd555 )
 {
 angle2 = angle -  0xaaaa ;

 angle1 =  0x2aaa  - angle2;

 t1 = sinetable[(unsigned char)(angle1 >> 6)];

 t2 = sinetable[(unsigned char)(angle2 >> 6)];

 t1 = ((long)t1*(long)volts) >> 15;

 t1 = ((long)t1*(long)tpwm) >> 15;

 t2 = ((long)t2*(long)volts) >> 15;
 t2 = ((long)t2*(long)tpwm) >> 15;
 half_t0 = (tpwm - t1 - t2) >> 1;


 PDC1 = t2 + half_t0;
 PDC2 = half_t0;
 PDC3 = t1 + t2 + half_t0;
 }
else
 {
 angle2 = angle -  0xd555 ;

 angle1 =  0x2aaa  - angle2;

 t1 = sinetable[(unsigned char)(angle1 >> 6)];

 t2 = sinetable[(unsigned char)(angle2 >> 6)];

 t1 = ((long)t1*(long)volts) >> 15;

 t1 = ((long)t1*(long)tpwm) >> 15;

 t2 = ((long)t2*(long)volts) >> 15;
 t2 = ((long)t2*(long)tpwm) >> 15;
 half_t0 = (tpwm - t1 - t2) >> 1;


 PDC1 = t1 + t2 + half_t0;
 PDC2 = half_t0;
 PDC3 = t1 + half_t0;
 }
}
