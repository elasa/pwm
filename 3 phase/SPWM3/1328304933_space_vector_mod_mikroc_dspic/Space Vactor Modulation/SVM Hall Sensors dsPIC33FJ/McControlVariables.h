//------------------------------------------------------------------------
// Motor Control Definitions and variables
//------------------------------------------------------------------------
typedef signed int SFRAC16;
//--------------------------------------------------------------------------
//#define CLOSED_LOOP      // if defined the speed controller will be enabled
//#define PHASE_ADVANCE    // for extended speed ranges this should be defined
#define _10MILLISEC 10    // Used as a timeout with no hall effect sensors
                         // transitions and Forcing steps according to the
                         // actual position of the motor
#define _100MILLISEC 100 // after this time has elapsed, the motor is
                         // consider stalled and it's stopped
//----------------------------------------------------------------------------
// These Phase values represent the base Phase value of the sinewave for each
// one of the sectors (each sector is a translation of the hall effect sensors
// reading
#define PHASE_ZERO  57344
#define PHASE_ONE   ((PHASE_ZERO + 65536/6) % 65536)
#define PHASE_TWO   ((PHASE_ONE + 65536/6) % 65536)
#define PHASE_THREE ((PHASE_TWO + 65536/6) % 65536)
#define PHASE_FOUR  ((PHASE_THREE + 65536/6) % 65536)
#define PHASE_FIVE  ((PHASE_FOUR + 65536/6) % 65536)
//----------------------------------------------------------------------------
#define MAX_PH_ADV_DEG 40  // This value represents the maximum allowed phase
                           // advance in electrical degrees. Set a value from
                           // 0 to 60. This value will be used to calculate
                           // phase advance only if PHASE_ADVANCE is defined
//----------------------------------------------------------------------------
// This is the calculation from the required phase advance to the actual
// value to be multiplied by the speed of the motor. So, if PHASE_ADVANCE is
// enabled, a certain amount of shit angle will be added to the generated
// sine wave, up to a maximum of the specified value on MAX_PH_ADV_DEG. This
// maximum phase shift will be present when the MeasuredSpeed variable is a
// fractional 1.0 (for CW) or -1.0 (for CCW).
#define MAX_PH_ADV (int)(((float)MAX_PH_ADV_DEG / 360.0) * 65536.0)
#define HALLA     1        // Connected to RB1
#define HALLB     2        // Connected to RB2
#define HALLC     4        // Connected to RB3
#define CW        0        // Counter Clock Wise direction
#define CCW       1        // Clock Wise direction
//--------------------------------------------------------------------------
#define MINPERIOD  313        // For 6000 max rpm and 10 poles motor
#define MAXPERIOD  31250      // For 60 min rpm and 10 poles motor
//-----------------------------------------------------------------------------
// Use this MACRO when using floats to initialize signed 16-bit fractional
// variables
#define SFloat_To_SFrac16(Float_Value)\
   ((Float_Value < 0.0) ? (SFRAC16)(32768 * (Float_Value) - 0.5)\
   :(SFRAC16)(32767 * (Float_Value) + 0.5))
//----------------------------------------------------------------------------
// Constants used for properly energizing the motor depending on the
// rotor's position
const int PhaseValues[6] =
{PHASE_ZERO, PHASE_ONE, PHASE_TWO, PHASE_THREE, PHASE_FOUR, PHASE_FIVE};
//----------------------------------------------------------------------------
// In the sinewave generation algorithm we need an offset to be added to the
// pointer when energizing the motor in CCW. This is done to compensate an
// asymetry of the sinewave
int PhaseOffset = 6000;
//-----------------------------------------------------------------------------
unsigned int Phase;  // This variable is incremented by the PWM interrupt
                     // in order to generate a proper sinewave. Its value
                     // is incremented by a value of PhaseInc, which
                     // represents the frequency of the generated sinewave
signed int PhaseInc; // Delta increments of the Phase variable, calculated
                     // in the TIMER1 interrupt (each 1 ms) and used in
                     // the PWM interrupt (each 50 us)
signed int PhaseAdvance;// Used for extending motor speed range. This value
                        // is added directly to the parameters passed to the
                        // SVM function (the sine wave generation subroutine)
unsigned int HallValue; // This variable holds the hall sensor input readings
unsigned int Sector;  // This variables holds present sector value, which is
                      // the rotor position
unsigned int LastSector; // This variable holds the last sector value. This
                         // is critical to filter slow slew rate on the Hall
                         // effect sensors hardware
unsigned int MotorStalledCounter = 0; // This variable gets incremented each
                                      // 1 ms, and is cleared everytime a new
                                      // sector is detected. Used for
                                      // ForceCommutation and MotorStalled
                                      // protection functions
 //----------------------------------------------------------------------------
// This array translates the hall state value read from the digital I/O to the
// proper sector.  Hall values of 0 or 7 represent illegal values and therefore
// return -1.
char SectorTable[] = {-1,4,2,3,0,5,1,-1};
//----------------------------------------------------------------------------
unsigned char Current_Direction;  // Current mechanical motor direction of
                                  // rotation Calculated in halls interrupts
unsigned char Required_Direction; // Required mechanical motor direction of
                                  // rotation, will have the same sign as the
                                  // ControlOutput variable from the Speed
                                   // Controller
// Variables containing the Period of half an electrical cycle, which is an
// interrupt each edge of one of the hall sensor input
unsigned int PastCapture, ActualCapture, Period;
// Used as a temporal variable to perform a fractional divide operation in
// assembly
unsigned int temp_count;        // Variable used for Delay Count
SFRAC16 _MINPERIOD = MINPERIOD - 1;
SFRAC16 MeasuredSpeed,RefSpeed = 200;//Actual and Desired speeds for the PID
                                // controller, that will generate the error
SFRAC16 ControlOutput = 0;      // Controller output, used as a voltage output,
                                // use its sign for the required direction
// Absolute PID gains used by the controller. Position form implementation of
// a digital PID. See SpeedControl subroutine for details
SFRAC16 Kp ;
SFRAC16 Ki ;
SFRAC16 Kd ;

//-------------------------------------------------------------------------
// Used as a temporal variable to perform a fractional divide operation in
// assembly
//-------------------------------------------------------------------------
SFRAC16 _MAX_PH_ADV = MAX_PH_ADV;
//--------------------------------------------------------------
// Flags used for the application
struct MotorFlags
{
 unsigned MotorRunning :1;
 unsigned unused       :15;
}Flags;
//----------------------------------------------------------------------------