
_ReadCAN:

;IO_CAN.c,33 :: 		void ReadCAN(void)
;IO_CAN.c,38 :: 		Msg_Rcvd = ECAN1Read(&RxID,RxData,&RxDataLen,&Can_Rcv_Flags);
	PUSH	W10
	PUSH	W11
	PUSH	W12
	PUSH	W13
	MOV	#lo_addr(_Can_Rcv_Flags), W13
	MOV	#lo_addr(_RxDataLen), W12
	MOV	#lo_addr(_RxData), W11
	MOV	#lo_addr(_RxID), W10
	CALL	_ECAN1Read
	MOV	#lo_addr(_Msg_Rcvd), W1
	MOV.B	W0, [W1]
;IO_CAN.c,39 :: 		if( Msg_Rcvd!=0)
	CP.B	W0, #0
	BRA NZ	L__ReadCAN9
	GOTO	L_ReadCAN0
L__ReadCAN9:
;IO_CAN.c,41 :: 		switch(RxID)
	GOTO	L_ReadCAN1
;IO_CAN.c,44 :: 		case 0x0000:
L_ReadCAN3:
;IO_CAN.c,45 :: 		asm nop;
	NOP
;IO_CAN.c,46 :: 		break;
	GOTO	L_ReadCAN2
;IO_CAN.c,48 :: 		case 0x211:
L_ReadCAN4:
;IO_CAN.c,49 :: 		asm nop;
	NOP
;IO_CAN.c,50 :: 		break;
	GOTO	L_ReadCAN2
;IO_CAN.c,52 :: 		case 0x610:
L_ReadCAN5:
;IO_CAN.c,53 :: 		asm nop;
	NOP
;IO_CAN.c,54 :: 		break;
	GOTO	L_ReadCAN2
;IO_CAN.c,55 :: 		}
L_ReadCAN1:
	MOV	_RxID, W0
	MOV	_RxID+2, W1
	CP	W0, #0
	CPB	W1, #0
	BRA NZ	L__ReadCAN10
	GOTO	L_ReadCAN3
L__ReadCAN10:
	MOV	#529, W1
	MOV	#0, W2
	MOV	#lo_addr(_RxID), W0
	CP	W1, [W0++]
	CPB	W2, [W0--]
	BRA NZ	L__ReadCAN11
	GOTO	L_ReadCAN4
L__ReadCAN11:
	MOV	#1552, W1
	MOV	#0, W2
	MOV	#lo_addr(_RxID), W0
	CP	W1, [W0++]
	CPB	W2, [W0--]
	BRA NZ	L__ReadCAN12
	GOTO	L_ReadCAN5
L__ReadCAN12:
L_ReadCAN2:
;IO_CAN.c,56 :: 		}
L_ReadCAN0:
;IO_CAN.c,57 :: 		}
L_end_ReadCAN:
	POP	W13
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _ReadCAN

_WriteCAN:

;IO_CAN.c,59 :: 		void WriteCAN(void)
;IO_CAN.c,64 :: 		TxData[0] = (MotorSpeed &0xFF);
	PUSH	W10
	PUSH	W11
	PUSH	W12
	PUSH	W13
	MOV	_MotorSpeed, W2
	MOV.B	#255, W1
	MOV	#lo_addr(_TxData), W0
	AND.B	W2, W1, [W0]
;IO_CAN.c,65 :: 		TxData[1] = (MotorSpeed >> 8);
	MOV	_MotorSpeed, W0
	ASR	W0, #8, W1
	MOV	#lo_addr(_TxData+1), W0
	MOV.B	W1, [W0]
;IO_CAN.c,66 :: 		TxData[2] = (Period &0xFF);
	MOV	_Period, W2
	MOV.B	#255, W1
	MOV	#lo_addr(_TxData+2), W0
	AND.B	W2, W1, [W0]
;IO_CAN.c,67 :: 		TxData[3] = (Period >> 8);
	MOV	_Period, W0
	LSR	W0, #8, W1
	MOV	#lo_addr(_TxData+3), W0
	MOV.B	W1, [W0]
;IO_CAN.c,68 :: 		TxData[4] = 0;
	MOV	#lo_addr(_TxData+4), W1
	CLR	W0
	MOV.B	W0, [W1]
;IO_CAN.c,69 :: 		TxData[5] = 0;
	MOV	#lo_addr(_TxData+5), W1
	CLR	W0
	MOV.B	W0, [W1]
;IO_CAN.c,70 :: 		TxData[6] = 0;
	MOV	#lo_addr(_TxData+6), W1
	CLR	W0
	MOV.B	W0, [W1]
;IO_CAN.c,71 :: 		TxData[7] = 0;
	MOV	#lo_addr(_TxData+7), W1
	CLR	W0
	MOV.B	W0, [W1]
;IO_CAN.c,72 :: 		TxDataLen = 8;
	MOV	#8, W0
	MOV	W0, _TxDataLen
;IO_CAN.c,73 :: 		TxID = 0x191; //0x180 + Node ID (0x11) = 0x191
	MOV	#401, W0
	MOV	#0, W1
	MOV	W0, _TxID
	MOV	W1, _TxID+2
;IO_CAN.c,74 :: 		ECAN1Write(TxID,TxData,TxDataLen,Can_Send_Flags);
	MOV	#8, W13
	MOV	#lo_addr(_TxData), W12
	MOV	#401, W10
	MOV	#0, W11
	PUSH	_Can_Send_Flags
	CALL	_ECAN1Write
	SUB	#2, W15
;IO_CAN.c,75 :: 		Delay_ms(20);
	MOV	#3, W8
	MOV	#2261, W7
L_WriteCAN6:
	DEC	W7
	BRA NZ	L_WriteCAN6
	DEC	W8
	BRA NZ	L_WriteCAN6
;IO_CAN.c,76 :: 		}
L_end_WriteCAN:
	POP	W13
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _WriteCAN

_InitCAN:

;IO_CAN.c,79 :: 		void InitCAN(void)
;IO_CAN.c,82 :: 		IFS2bits.C1IF = 0;
	PUSH	W10
	PUSH	W11
	PUSH	W12
	PUSH	W13
	BCLR	IFS2bits, #3
;IO_CAN.c,84 :: 		IEC2bits.C1IE   = 1;             //enable ECAN1 interrupts
	BSET	IEC2bits, #3
;IO_CAN.c,85 :: 		C1INTEbits.TBIE = 1;             //enable ECAN1 tx interrupt
	BSET.B	C1INTEbits, #0
;IO_CAN.c,86 :: 		C1INTEbits.RBIE = 1;             //enable ECAN1 rx interrupt
	BSET.B	C1INTEbits, #1
;IO_CAN.c,87 :: 		Can_Init_Flags = 0;              //
	CLR	W0
	MOV	W0, _Can_Init_Flags
;IO_CAN.c,88 :: 		Can_Send_Flags = 0;              // clear flags
	CLR	W0
	MOV	W0, _Can_Send_Flags
;IO_CAN.c,89 :: 		Can_Rcv_Flags  = 0;              //
	CLR	W0
	MOV	W0, _Can_Rcv_Flags
;IO_CAN.c,93 :: 		_ECAN_TX_NO_RTR_FRAME;
	MOV	#252, W0
	MOV	W0, _Can_Send_Flags
;IO_CAN.c,99 :: 		_ECAN_CONFIG_LINE_FILTER_OFF;
	MOV	#249, W0
	MOV	W0, _Can_Init_Flags
;IO_CAN.c,102 :: 		ECAN1DmaChannelInit(0, 1, &ECAN1RxTxRAMBuffer);
	MOV	#16384, W12
	MOV	#1, W11
	CLR	W10
	CALL	_ECAN1DmaChannelInit
;IO_CAN.c,104 :: 		ECAN1DmaChannelInit(2, 0, &ECAN1RxTxRAMBuffer);
	MOV	#16384, W12
	CLR	W11
	MOV	#2, W10
	CALL	_ECAN1DmaChannelInit
;IO_CAN.c,106 :: 		ECAN1Initialize(1, 4, 3, 3, 3, Can_Init_Flags);
	MOV	#3, W13
	MOV	#3, W12
	MOV	#4, W11
	MOV	#1, W10
	PUSH	_Can_Init_Flags
	MOV	#3, W0
	PUSH	W0
	CALL	_ECAN1Initialize
	SUB	#4, W15
;IO_CAN.c,108 :: 		ECAN1SetBufferSize(ECAN1RAMBUFFERSIZE);
	MOV	#16, W10
	CALL	_ECAN1SetBufferSize
;IO_CAN.c,110 :: 		ECAN1SelectTxBuffers(0x000F);
	MOV	#15, W10
	CALL	_ECAN1SelectTxBuffers
;IO_CAN.c,111 :: 		ECAN1SetOperationMode(_ECAN_MODE_CONFIG,0xFF);   // set CONFIGURATION mode
	MOV	#255, W11
	MOV	#4, W10
	CALL	_ECAN1SetOperationMode
;IO_CAN.c,114 :: 		_ECAN_CONFIG_STD_MSG);
	MOV	#255, W13
;IO_CAN.c,113 :: 		ECAN1SetMask(_ECAN_MASK_0, -1,_ECAN_CONFIG_MATCH_MSG_TYPE &
	MOV	#65535, W11
	MOV	#65535, W12
	CLR	W10
;IO_CAN.c,114 :: 		_ECAN_CONFIG_STD_MSG);
	CALL	_ECAN1SetMask
;IO_CAN.c,117 :: 		_ECAN_CONFIG_STD_MSG);
	MOV	#255, W13
;IO_CAN.c,116 :: 		ECAN1SetMask(_ECAN_MASK_1, -1,_ECAN_CONFIG_MATCH_MSG_TYPE &
	MOV	#65535, W11
	MOV	#65535, W12
	MOV	#1, W10
;IO_CAN.c,117 :: 		_ECAN_CONFIG_STD_MSG);
	CALL	_ECAN1SetMask
;IO_CAN.c,120 :: 		_ECAN_CONFIG_STD_MSG);
	MOV	#255, W13
;IO_CAN.c,119 :: 		ECAN1SetMask(_ECAN_MASK_2, -1, _ECAN_CONFIG_MATCH_MSG_TYPE &
	MOV	#65535, W11
	MOV	#65535, W12
	MOV	#2, W10
;IO_CAN.c,120 :: 		_ECAN_CONFIG_STD_MSG);
	CALL	_ECAN1SetMask
;IO_CAN.c,121 :: 		ECAN1SetFilter(_ECAN_FILTER_10, 0x211,_ECAN_MASK_2,_ECAN_RX_BUFFER_7,
	MOV	#2, W13
	MOV	#529, W11
	MOV	#0, W12
	MOV	#10, W10
;IO_CAN.c,122 :: 		_ECAN_CONFIG_STD_MSG);
	MOV	#255, W0
	PUSH	W0
;IO_CAN.c,121 :: 		ECAN1SetFilter(_ECAN_FILTER_10, 0x211,_ECAN_MASK_2,_ECAN_RX_BUFFER_7,
	MOV	#7, W0
	PUSH	W0
;IO_CAN.c,122 :: 		_ECAN_CONFIG_STD_MSG);
	CALL	_ECAN1SetFilter
	SUB	#4, W15
;IO_CAN.c,124 :: 		ECAN1SetOperationMode(_ECAN_MODE_NORMAL, 0xFF);
	MOV	#255, W11
	CLR	W10
	CALL	_ECAN1SetOperationMode
;IO_CAN.c,125 :: 		Msg_Rcvd = 0;
	MOV	#lo_addr(_Msg_Rcvd), W1
	CLR	W0
	MOV.B	W0, [W1]
;IO_CAN.c,126 :: 		}
L_end_InitCAN:
	POP	W13
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _InitCAN
