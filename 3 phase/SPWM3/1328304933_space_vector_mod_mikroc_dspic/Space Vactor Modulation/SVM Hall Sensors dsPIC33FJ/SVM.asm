
_SVM:

;SVM.c,56 :: 		void SVM(int volts,unsigned int angle)
;SVM.c,67 :: 		tpwm = PTPER << 1;
	MOV	PTPER, WREG
	SL	W0, #1, W0
; tpwm start address is: 12 (W6)
	MOV	W0, W6
;SVM.c,70 :: 		if(volts > VOLTS_LIMIT) volts = VOLTS_LIMIT;
	MOV	#28300, W0
	CP	W10, W0
	BRA GT	L__SVM12
	GOTO	L_SVM0
L__SVM12:
	MOV	#28300, W10
L_SVM0:
;SVM.c,72 :: 		if(angle < VECTOR2)
	MOV	#10922, W0
	CP	W11, W0
	BRA LTU	L__SVM13
	GOTO	L_SVM1
L__SVM13:
;SVM.c,74 :: 		angle2 = angle - VECTOR1;   // Reference SVM angle to the current
; angle2 start address is: 4 (W2)
	MOV	W11, W2
;SVM.c,76 :: 		angle1 = SIXTY_DEG - angle2; // Calculate second angle referenced to
	MOV	#10922, W0
; angle1 start address is: 2 (W1)
	SUB	W0, W2, W1
;SVM.c,78 :: 		t1 = sinetable[(unsigned char)(angle1 >> 6)];// Look up values from
	LSR	W1, #6, W0
; angle1 end address is: 2 (W1)
	ZE	W0, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_sinetable), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
; t1 start address is: 8 (W4)
	MOV	W0, W4
;SVM.c,80 :: 		t2 = sinetable[(unsigned char)(angle2 >> 6)];
	LSR	W2, #6, W0
; angle2 end address is: 4 (W2)
	ZE	W0, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_sinetable), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
; t2 start address is: 16 (W8)
	MOV	W0, W8
;SVM.c,82 :: 		t1 = ((long)t1*(long)volts) >> 15;
	MOV	W4, W2
	CLR	W3
; t1 end address is: 8 (W4)
	MOV	W10, W0
	ASR	W0, #15, W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM14:
	DEC	W4, W4
	BRA LT	L__SVM15
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM14
L__SVM15:
; t1 start address is: 0 (W0)
	MOV	W2, W0
;SVM.c,84 :: 		t1 = ((long)t1*(long)tpwm) >> 15;
	MOV	W0, W2
	CLR	W3
; t1 end address is: 0 (W0)
	MOV	W6, W0
	CLR	W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM16:
	DEC	W4, W4
	BRA LT	L__SVM17
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM16
L__SVM17:
; t1 start address is: 14 (W7)
	MOV	W2, W7
;SVM.c,86 :: 		t2 = ((long)t2*(long)volts) >> 15;
	MOV	W8, W2
	CLR	W3
; t2 end address is: 16 (W8)
	MOV	W10, W0
	ASR	W0, #15, W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM18:
	DEC	W4, W4
	BRA LT	L__SVM19
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM18
L__SVM19:
; t2 start address is: 0 (W0)
	MOV	W2, W0
;SVM.c,87 :: 		t2 = ((long)t2*(long)tpwm) >> 15;
	MOV	W0, W2
	CLR	W3
; t2 end address is: 0 (W0)
	MOV	W6, W0
	CLR	W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM20:
	DEC	W4, W4
	BRA LT	L__SVM21
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM20
L__SVM21:
; t2 start address is: 4 (W2)
;SVM.c,88 :: 		half_t0 = (tpwm - t1 - t2) >> 1;// Calculate half_t0 null time from
	SUB	W6, W7, W0
; tpwm end address is: 12 (W6)
	SUB	W0, W2, W0
	LSR	W0, #1, W0
; half_t0 start address is: 6 (W3)
	MOV	W0, W3
;SVM.c,91 :: 		PDC1 = t1 + t2 + half_t0;
	ADD	W7, W2, W1
; t1 end address is: 14 (W7)
	MOV	#lo_addr(PDC1), W0
	ADD	W1, W3, [W0]
;SVM.c,92 :: 		PDC2 = t2 + half_t0;
	MOV	#lo_addr(PDC2), W0
	ADD	W2, W3, [W0]
; t2 end address is: 4 (W2)
;SVM.c,93 :: 		PDC3 = half_t0;
	MOV	W3, PDC3
; half_t0 end address is: 6 (W3)
;SVM.c,94 :: 		}
	GOTO	L_SVM2
L_SVM1:
;SVM.c,95 :: 		else if(angle < VECTOR3)
; tpwm start address is: 12 (W6)
	MOV	#21845, W0
	CP	W11, W0
	BRA LTU	L__SVM22
	GOTO	L_SVM3
L__SVM22:
;SVM.c,97 :: 		angle2 = angle - VECTOR2; // Reference SVM angle to the current
	MOV	#10922, W0
; angle2 start address is: 4 (W2)
	SUB	W11, W0, W2
;SVM.c,99 :: 		angle1 = SIXTY_DEG - angle2;// Calculate second angle referenced to
	MOV	#10922, W0
; angle1 start address is: 2 (W1)
	SUB	W0, W2, W1
;SVM.c,101 :: 		t1 = sinetable[(unsigned char)(angle1 >> 6)];// Look up values from
	LSR	W1, #6, W0
; angle1 end address is: 2 (W1)
	ZE	W0, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_sinetable), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
; t1 start address is: 8 (W4)
	MOV	W0, W4
;SVM.c,103 :: 		t2 = sinetable[(unsigned char)(angle2 >> 6)];
	LSR	W2, #6, W0
; angle2 end address is: 4 (W2)
	ZE	W0, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_sinetable), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
; t2 start address is: 16 (W8)
	MOV	W0, W8
;SVM.c,105 :: 		t1 = ((long)t1*(long)volts) >> 15;
	MOV	W4, W2
	CLR	W3
; t1 end address is: 8 (W4)
	MOV	W10, W0
	ASR	W0, #15, W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM23:
	DEC	W4, W4
	BRA LT	L__SVM24
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM23
L__SVM24:
; t1 start address is: 0 (W0)
	MOV	W2, W0
;SVM.c,107 :: 		t1 = ((long)t1*(long)tpwm) >> 15;
	MOV	W0, W2
	CLR	W3
; t1 end address is: 0 (W0)
	MOV	W6, W0
	CLR	W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM25:
	DEC	W4, W4
	BRA LT	L__SVM26
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM25
L__SVM26:
; t1 start address is: 14 (W7)
	MOV	W2, W7
;SVM.c,109 :: 		t2 = ((long)t2*(long)volts) >> 15;
	MOV	W8, W2
	CLR	W3
; t2 end address is: 16 (W8)
	MOV	W10, W0
	ASR	W0, #15, W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM27:
	DEC	W4, W4
	BRA LT	L__SVM28
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM27
L__SVM28:
; t2 start address is: 0 (W0)
	MOV	W2, W0
;SVM.c,110 :: 		t2 = ((long)t2*(long)tpwm) >> 15;
	MOV	W0, W2
	CLR	W3
; t2 end address is: 0 (W0)
	MOV	W6, W0
	CLR	W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM29:
	DEC	W4, W4
	BRA LT	L__SVM30
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM29
L__SVM30:
; t2 start address is: 4 (W2)
;SVM.c,111 :: 		half_t0 = (tpwm - t1 - t2) >> 1; // Calculate half_t0 null time from
	SUB	W6, W7, W0
; tpwm end address is: 12 (W6)
	SUB	W0, W2, W0
	LSR	W0, #1, W0
; half_t0 start address is: 6 (W3)
	MOV	W0, W3
;SVM.c,114 :: 		PDC1 = t1 + half_t0;
	MOV	#lo_addr(PDC1), W0
	ADD	W7, W3, [W0]
;SVM.c,115 :: 		PDC2 = t1 + t2 + half_t0;
	ADD	W7, W2, W1
; t1 end address is: 14 (W7)
; t2 end address is: 4 (W2)
	MOV	#lo_addr(PDC2), W0
	ADD	W1, W3, [W0]
;SVM.c,116 :: 		PDC3 = half_t0;
	MOV	W3, PDC3
; half_t0 end address is: 6 (W3)
;SVM.c,117 :: 		}
	GOTO	L_SVM4
L_SVM3:
;SVM.c,118 :: 		else if(angle < VECTOR4)
; tpwm start address is: 12 (W6)
	MOV	#32768, W0
	CP	W11, W0
	BRA LTU	L__SVM31
	GOTO	L_SVM5
L__SVM31:
;SVM.c,120 :: 		angle2 = angle - VECTOR3;        // Reference SVM angle to the current
	MOV	#21845, W0
; angle2 start address is: 4 (W2)
	SUB	W11, W0, W2
;SVM.c,122 :: 		angle1 = SIXTY_DEG - angle2;// Calculate second angle referenced to
	MOV	#10922, W0
; angle1 start address is: 2 (W1)
	SUB	W0, W2, W1
;SVM.c,124 :: 		t1 = sinetable[(unsigned char)(angle1 >> 6)];// Look up values from
	LSR	W1, #6, W0
; angle1 end address is: 2 (W1)
	ZE	W0, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_sinetable), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
; t1 start address is: 8 (W4)
	MOV	W0, W4
;SVM.c,126 :: 		t2 = sinetable[(unsigned char)(angle2 >> 6)];
	LSR	W2, #6, W0
; angle2 end address is: 4 (W2)
	ZE	W0, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_sinetable), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
; t2 start address is: 16 (W8)
	MOV	W0, W8
;SVM.c,128 :: 		t1 = ((long)t1*(long)volts) >> 15;
	MOV	W4, W2
	CLR	W3
; t1 end address is: 8 (W4)
	MOV	W10, W0
	ASR	W0, #15, W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM32:
	DEC	W4, W4
	BRA LT	L__SVM33
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM32
L__SVM33:
; t1 start address is: 0 (W0)
	MOV	W2, W0
;SVM.c,130 :: 		t1 = ((long)t1*(long)tpwm) >> 15;
	MOV	W0, W2
	CLR	W3
; t1 end address is: 0 (W0)
	MOV	W6, W0
	CLR	W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM34:
	DEC	W4, W4
	BRA LT	L__SVM35
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM34
L__SVM35:
; t1 start address is: 14 (W7)
	MOV	W2, W7
;SVM.c,132 :: 		t2 = ((long)t2*(long)volts) >> 15;
	MOV	W8, W2
	CLR	W3
; t2 end address is: 16 (W8)
	MOV	W10, W0
	ASR	W0, #15, W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM36:
	DEC	W4, W4
	BRA LT	L__SVM37
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM36
L__SVM37:
; t2 start address is: 0 (W0)
	MOV	W2, W0
;SVM.c,133 :: 		t2 = ((long)t2*(long)tpwm) >> 15;
	MOV	W0, W2
	CLR	W3
; t2 end address is: 0 (W0)
	MOV	W6, W0
	CLR	W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM38:
	DEC	W4, W4
	BRA LT	L__SVM39
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM38
L__SVM39:
; t2 start address is: 4 (W2)
;SVM.c,134 :: 		half_t0 = (tpwm - t1 - t2) >> 1;   // Calculate half_t0 null time from
	SUB	W6, W7, W0
; tpwm end address is: 12 (W6)
	SUB	W0, W2, W0
	LSR	W0, #1, W0
; half_t0 start address is: 6 (W3)
	MOV	W0, W3
;SVM.c,137 :: 		PDC1 = half_t0;
	MOV	W3, PDC1
;SVM.c,138 :: 		PDC2 = t1 + t2 + half_t0;
	ADD	W7, W2, W1
; t1 end address is: 14 (W7)
	MOV	#lo_addr(PDC2), W0
	ADD	W1, W3, [W0]
;SVM.c,139 :: 		PDC3 = t2 + half_t0;
	MOV	#lo_addr(PDC3), W0
	ADD	W2, W3, [W0]
; t2 end address is: 4 (W2)
; half_t0 end address is: 6 (W3)
;SVM.c,140 :: 		}
	GOTO	L_SVM6
L_SVM5:
;SVM.c,141 :: 		else if(angle < VECTOR5)
; tpwm start address is: 12 (W6)
	MOV	#43690, W0
	CP	W11, W0
	BRA LTU	L__SVM40
	GOTO	L_SVM7
L__SVM40:
;SVM.c,143 :: 		angle2 = angle - VECTOR4;        // Reference SVM angle to the current
	MOV	#32768, W0
; angle2 start address is: 4 (W2)
	SUB	W11, W0, W2
;SVM.c,145 :: 		angle1 = SIXTY_DEG - angle2;// Calculate second angle referenced to
	MOV	#10922, W0
; angle1 start address is: 2 (W1)
	SUB	W0, W2, W1
;SVM.c,147 :: 		t1 = sinetable[(unsigned char)(angle1 >> 6)];// Look up values from
	LSR	W1, #6, W0
; angle1 end address is: 2 (W1)
	ZE	W0, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_sinetable), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
; t1 start address is: 8 (W4)
	MOV	W0, W4
;SVM.c,149 :: 		t2 = sinetable[(unsigned char)(angle2 >> 6)];
	LSR	W2, #6, W0
; angle2 end address is: 4 (W2)
	ZE	W0, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_sinetable), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
; t2 start address is: 16 (W8)
	MOV	W0, W8
;SVM.c,151 :: 		t1 = ((long)t1*(long)volts) >> 15;
	MOV	W4, W2
	CLR	W3
; t1 end address is: 8 (W4)
	MOV	W10, W0
	ASR	W0, #15, W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM41:
	DEC	W4, W4
	BRA LT	L__SVM42
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM41
L__SVM42:
; t1 start address is: 0 (W0)
	MOV	W2, W0
;SVM.c,153 :: 		t1 = ((long)t1*(long)tpwm) >> 15;
	MOV	W0, W2
	CLR	W3
; t1 end address is: 0 (W0)
	MOV	W6, W0
	CLR	W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM43:
	DEC	W4, W4
	BRA LT	L__SVM44
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM43
L__SVM44:
; t1 start address is: 14 (W7)
	MOV	W2, W7
;SVM.c,155 :: 		t2 = ((long)t2*(long)volts) >> 15;
	MOV	W8, W2
	CLR	W3
; t2 end address is: 16 (W8)
	MOV	W10, W0
	ASR	W0, #15, W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM45:
	DEC	W4, W4
	BRA LT	L__SVM46
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM45
L__SVM46:
; t2 start address is: 0 (W0)
	MOV	W2, W0
;SVM.c,156 :: 		t2 = ((long)t2*(long)tpwm) >> 15;
	MOV	W0, W2
	CLR	W3
; t2 end address is: 0 (W0)
	MOV	W6, W0
	CLR	W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM47:
	DEC	W4, W4
	BRA LT	L__SVM48
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM47
L__SVM48:
; t2 start address is: 4 (W2)
;SVM.c,157 :: 		half_t0 = (tpwm - t1 - t2) >> 1;   // Calculate half_t0 null time from
	SUB	W6, W7, W0
; tpwm end address is: 12 (W6)
	SUB	W0, W2, W0
	LSR	W0, #1, W0
; half_t0 start address is: 6 (W3)
	MOV	W0, W3
;SVM.c,160 :: 		PDC1 = half_t0;
	MOV	W3, PDC1
;SVM.c,161 :: 		PDC2 = t1 + half_t0;
	MOV	#lo_addr(PDC2), W0
	ADD	W7, W3, [W0]
;SVM.c,162 :: 		PDC3 = t1 + t2 + half_t0;
	ADD	W7, W2, W1
; t1 end address is: 14 (W7)
; t2 end address is: 4 (W2)
	MOV	#lo_addr(PDC3), W0
	ADD	W1, W3, [W0]
; half_t0 end address is: 6 (W3)
;SVM.c,163 :: 		}
	GOTO	L_SVM8
L_SVM7:
;SVM.c,164 :: 		else if(angle < VECTOR6)
; tpwm start address is: 12 (W6)
	MOV	#54613, W0
	CP	W11, W0
	BRA LTU	L__SVM49
	GOTO	L_SVM9
L__SVM49:
;SVM.c,166 :: 		angle2 = angle - VECTOR5;        // Reference SVM angle to the current
	MOV	#43690, W0
; angle2 start address is: 4 (W2)
	SUB	W11, W0, W2
;SVM.c,168 :: 		angle1 = SIXTY_DEG - angle2;// Calculate second angle referenced to
	MOV	#10922, W0
; angle1 start address is: 2 (W1)
	SUB	W0, W2, W1
;SVM.c,170 :: 		t1 = sinetable[(unsigned char)(angle1 >> 6)];// Look up values from
	LSR	W1, #6, W0
; angle1 end address is: 2 (W1)
	ZE	W0, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_sinetable), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
; t1 start address is: 8 (W4)
	MOV	W0, W4
;SVM.c,172 :: 		t2 = sinetable[(unsigned char)(angle2 >> 6)];
	LSR	W2, #6, W0
; angle2 end address is: 4 (W2)
	ZE	W0, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_sinetable), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
; t2 start address is: 16 (W8)
	MOV	W0, W8
;SVM.c,174 :: 		t1 = ((long)t1*(long)volts) >> 15;
	MOV	W4, W2
	CLR	W3
; t1 end address is: 8 (W4)
	MOV	W10, W0
	ASR	W0, #15, W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM50:
	DEC	W4, W4
	BRA LT	L__SVM51
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM50
L__SVM51:
; t1 start address is: 0 (W0)
	MOV	W2, W0
;SVM.c,176 :: 		t1 = ((long)t1*(long)tpwm) >> 15;
	MOV	W0, W2
	CLR	W3
; t1 end address is: 0 (W0)
	MOV	W6, W0
	CLR	W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM52:
	DEC	W4, W4
	BRA LT	L__SVM53
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM52
L__SVM53:
; t1 start address is: 14 (W7)
	MOV	W2, W7
;SVM.c,178 :: 		t2 = ((long)t2*(long)volts) >> 15;
	MOV	W8, W2
	CLR	W3
; t2 end address is: 16 (W8)
	MOV	W10, W0
	ASR	W0, #15, W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM54:
	DEC	W4, W4
	BRA LT	L__SVM55
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM54
L__SVM55:
; t2 start address is: 0 (W0)
	MOV	W2, W0
;SVM.c,179 :: 		t2 = ((long)t2*(long)tpwm) >> 15;
	MOV	W0, W2
	CLR	W3
; t2 end address is: 0 (W0)
	MOV	W6, W0
	CLR	W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM56:
	DEC	W4, W4
	BRA LT	L__SVM57
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM56
L__SVM57:
; t2 start address is: 4 (W2)
;SVM.c,180 :: 		half_t0 = (tpwm - t1 - t2) >> 1;  // Calculate half_t0 null time from
	SUB	W6, W7, W0
; tpwm end address is: 12 (W6)
	SUB	W0, W2, W0
	LSR	W0, #1, W0
; half_t0 start address is: 6 (W3)
	MOV	W0, W3
;SVM.c,183 :: 		PDC1 = t2 + half_t0;
	MOV	#lo_addr(PDC1), W0
	ADD	W2, W3, [W0]
;SVM.c,184 :: 		PDC2 = half_t0;
	MOV	W3, PDC2
;SVM.c,185 :: 		PDC3 = t1 + t2 + half_t0;
	ADD	W7, W2, W1
; t1 end address is: 14 (W7)
; t2 end address is: 4 (W2)
	MOV	#lo_addr(PDC3), W0
	ADD	W1, W3, [W0]
; half_t0 end address is: 6 (W3)
;SVM.c,186 :: 		}
	GOTO	L_SVM10
L_SVM9:
;SVM.c,189 :: 		angle2 = angle - VECTOR6;  // Reference SVM angle to the current
; tpwm start address is: 12 (W6)
	MOV	#54613, W0
; angle2 start address is: 4 (W2)
	SUB	W11, W0, W2
;SVM.c,191 :: 		angle1 = SIXTY_DEG - angle2;// Calculate second angle referenced to
	MOV	#10922, W0
; angle1 start address is: 2 (W1)
	SUB	W0, W2, W1
;SVM.c,193 :: 		t1 = sinetable[(unsigned char)(angle1 >> 6)];// Look up values from
	LSR	W1, #6, W0
; angle1 end address is: 2 (W1)
	ZE	W0, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_sinetable), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
; t1 start address is: 8 (W4)
	MOV	W0, W4
;SVM.c,195 :: 		t2 = sinetable[(unsigned char)(angle2 >> 6)];
	LSR	W2, #6, W0
; angle2 end address is: 4 (W2)
	ZE	W0, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_sinetable), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
; t2 start address is: 16 (W8)
	MOV	W0, W8
;SVM.c,197 :: 		t1 = ((long)t1*(long)volts) >> 15;
	MOV	W4, W2
	CLR	W3
; t1 end address is: 8 (W4)
	MOV	W10, W0
	ASR	W0, #15, W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM58:
	DEC	W4, W4
	BRA LT	L__SVM59
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM58
L__SVM59:
; t1 start address is: 0 (W0)
	MOV	W2, W0
;SVM.c,199 :: 		t1 = ((long)t1*(long)tpwm) >> 15;
	MOV	W0, W2
	CLR	W3
; t1 end address is: 0 (W0)
	MOV	W6, W0
	CLR	W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM60:
	DEC	W4, W4
	BRA LT	L__SVM61
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM60
L__SVM61:
; t1 start address is: 14 (W7)
	MOV	W2, W7
;SVM.c,201 :: 		t2 = ((long)t2*(long)volts) >> 15;
	MOV	W8, W2
	CLR	W3
; t2 end address is: 16 (W8)
	MOV	W10, W0
	ASR	W0, #15, W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM62:
	DEC	W4, W4
	BRA LT	L__SVM63
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM62
L__SVM63:
; t2 start address is: 0 (W0)
	MOV	W2, W0
;SVM.c,202 :: 		t2 = ((long)t2*(long)tpwm) >> 15;
	MOV	W0, W2
	CLR	W3
; t2 end address is: 0 (W0)
	MOV	W6, W0
	CLR	W1
	CALL	__Multiply_32x32
	MOV	#15, W4
	MOV.D	W0, W2
L__SVM64:
	DEC	W4, W4
	BRA LT	L__SVM65
	ASR	W3, W3
	RRC	W2, W2
	BRA	L__SVM64
L__SVM65:
; t2 start address is: 4 (W2)
;SVM.c,203 :: 		half_t0 = (tpwm - t1 - t2) >> 1;   // Calculate half_t0 null time from
	SUB	W6, W7, W0
; tpwm end address is: 12 (W6)
	SUB	W0, W2, W0
	LSR	W0, #1, W0
; half_t0 start address is: 6 (W3)
	MOV	W0, W3
;SVM.c,206 :: 		PDC1 = t1 + t2 + half_t0;
	ADD	W7, W2, W1
; t2 end address is: 4 (W2)
	MOV	#lo_addr(PDC1), W0
	ADD	W1, W3, [W0]
;SVM.c,207 :: 		PDC2 = half_t0;
	MOV	W3, PDC2
;SVM.c,208 :: 		PDC3 = t1 + half_t0;
	MOV	#lo_addr(PDC3), W0
	ADD	W7, W3, [W0]
; t1 end address is: 14 (W7)
; half_t0 end address is: 6 (W3)
;SVM.c,209 :: 		}
L_SVM10:
L_SVM8:
L_SVM6:
L_SVM4:
L_SVM2:
;SVM.c,210 :: 		}// end SVM()
L_end_SVM:
	RETURN
; end of _SVM
