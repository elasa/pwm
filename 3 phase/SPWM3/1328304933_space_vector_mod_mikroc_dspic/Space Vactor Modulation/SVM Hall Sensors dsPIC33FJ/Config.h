/**************************************************************************
* Config.h
* Configurations header File
***************************************************************************/
void IOMap(void);
void InitADC(void);
void InitMCPWM(void);
void InitIC(void);
void InitTMR1(void);
void InitTMR3(void);
//--------------------------------------------------------------------------