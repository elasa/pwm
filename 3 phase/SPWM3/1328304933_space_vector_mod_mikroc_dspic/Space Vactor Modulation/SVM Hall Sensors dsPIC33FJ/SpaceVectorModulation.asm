
_main:
	MOV	#2048, W15
	MOV	#6142, W0
	MOV	WREG, 32
	MOV	#1, W0
	MOV	WREG, 52
	MOV	#4, W0
	IOR	68

;SpaceVectorModulation.c,26 :: 		void main()
;SpaceVectorModulation.c,36 :: 		PLLFBD =  18;           // M=20
	PUSH	W10
	MOV	#18, W0
	MOV	WREG, PLLFBD
;SpaceVectorModulation.c,37 :: 		CLKDIVbits.PLLPOST = 0; // N1=2
	MOV	#lo_addr(CLKDIVbits), W0
	MOV.B	[W0], W1
	MOV.B	#63, W0
	AND.B	W1, W0, W1
	MOV	#lo_addr(CLKDIVbits), W0
	MOV.B	W1, [W0]
;SpaceVectorModulation.c,38 :: 		CLKDIVbits.PLLPRE = 0;  // N2=2
	MOV	#lo_addr(CLKDIVbits), W0
	MOV.B	[W0], W1
	MOV.B	#224, W0
	AND.B	W1, W0, W1
	MOV	#lo_addr(CLKDIVbits), W0
	MOV.B	W1, [W0]
;SpaceVectorModulation.c,42 :: 		IOMap();
	CALL	_IOMap
;SpaceVectorModulation.c,46 :: 		InitIC();
	CALL	_InitIC
;SpaceVectorModulation.c,50 :: 		InitADC();
	CALL	_InitADC
;SpaceVectorModulation.c,54 :: 		InitMCPWM();
	CALL	_InitMCPWM
;SpaceVectorModulation.c,58 :: 		InitTMR1();
	CALL	_InitTMR1
;SpaceVectorModulation.c,62 :: 		InitTMR3(void);
	CALL	_InitTMR3
;SpaceVectorModulation.c,66 :: 		InitCAN();
	CALL	_InitCAN
;SpaceVectorModulation.c,70 :: 		SetPID();
	CALL	_SetPID
;SpaceVectorModulation.c,74 :: 		Flags.MotorRunning = 0;
	MOV	#lo_addr(_Flags), W0
	BCLR	[W0], #0
;SpaceVectorModulation.c,75 :: 		while(1)
L_main0:
;SpaceVectorModulation.c,77 :: 		if ((!S2) && (!Flags.MotorRunning))
	BTSC	PORTAbits, #8
	GOTO	L__main19
	MOV	#lo_addr(_Flags), W0
	BTSC	[W0], #0
	GOTO	L__main18
L__main17:
;SpaceVectorModulation.c,79 :: 		Delay(200);
	MOV	#200, W10
	CALL	_Delay
;SpaceVectorModulation.c,80 :: 		if(!S2)
	BTSC	PORTAbits, #8
	GOTO	L_main5
;SpaceVectorModulation.c,82 :: 		RunMotor();  // Run motor if button is pressed and motor is stopped
	CALL	_RunMotor
;SpaceVectorModulation.c,83 :: 		while(!S2);
L_main6:
	BTSC	PORTAbits, #8
	GOTO	L_main7
	GOTO	L_main6
L_main7:
;SpaceVectorModulation.c,84 :: 		}
L_main5:
;SpaceVectorModulation.c,85 :: 		}
	GOTO	L_main8
;SpaceVectorModulation.c,77 :: 		if ((!S2) && (!Flags.MotorRunning))
L__main19:
L__main18:
;SpaceVectorModulation.c,86 :: 		else if ((!S2) && (Flags.MotorRunning))
	BTSC	PORTAbits, #8
	GOTO	L__main21
	MOV	#lo_addr(_Flags), W0
	BTSS	[W0], #0
	GOTO	L__main20
L__main16:
;SpaceVectorModulation.c,88 :: 		Delay(200);
	MOV	#200, W10
	CALL	_Delay
;SpaceVectorModulation.c,89 :: 		if(!S2)
	BTSC	PORTAbits, #8
	GOTO	L_main12
;SpaceVectorModulation.c,91 :: 		StopMotor(); // Stop motor button pressed and motor is running
	CALL	_StopMotor
;SpaceVectorModulation.c,92 :: 		while(!S2);
L_main13:
	BTSC	PORTAbits, #8
	GOTO	L_main14
	GOTO	L_main13
L_main14:
;SpaceVectorModulation.c,93 :: 		}
L_main12:
;SpaceVectorModulation.c,86 :: 		else if ((!S2) && (Flags.MotorRunning))
L__main21:
L__main20:
;SpaceVectorModulation.c,94 :: 		}
L_main8:
;SpaceVectorModulation.c,96 :: 		if(Flags.MotorRunning){
	MOV	#lo_addr(_Flags), W0
	BTSS	[W0], #0
	GOTO	L_main15
;SpaceVectorModulation.c,97 :: 		debug();
	CALL	_debug
;SpaceVectorModulation.c,98 :: 		WriteCAN();}
	CALL	_WriteCAN
L_main15:
;SpaceVectorModulation.c,99 :: 		}
	GOTO	L_main0
;SpaceVectorModulation.c,100 :: 		}
L_end_main:
L__main_end_loop:
	BRA	L__main_end_loop
; end of _main
