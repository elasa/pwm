//-------------------------------------------------------------------------
// IO_CAN.c
// This module handles all CAN communication and Configure the ECAN Module
//--------------------------------------------------------------------------
#include "IO_CAN.h"
#include "ECAN_Defs.h"
//--------------------------------------------------------------------------
unsigned int Can_Init_Flags, Can_Send_Flags, Can_Rcv_Flags;  // can flags
unsigned int RxDataLen;     // received data length in bytes
unsigned int TxDataLen;
char RxData[8];            // can rx/tx data buffer
char TxData[8];
char Msg_Rcvd;             // reception flag
unsigned long RxID;
unsigned long TxID;
//---------------------------------------------------------------------------
struct CanControlFlags
{
unsigned SetOperational :1;
unsigned NonOperational :1;
unsigned ErrorState     :1;
unsigned Tx             :1;
unsigned unused         :12;
}ControlFlag;
//--------------------------------------------------------------------------

//---------------------------------------------------------------------------
//import needed data from other modules
//---------------------------------------------------------------------------
extern signed int MotorSpeed;
extern unsigned int Period;
//---------------------------------------------------------------------------
void ReadCAN(void)
{
 //----------------------------------------------
 // Read CAN
 //----------------------------------------------
  Msg_Rcvd = ECAN1Read(&RxID,RxData,&RxDataLen,&Can_Rcv_Flags);
  if( Msg_Rcvd!=0)
 {
   switch(RxID)
   {
     /*---Set Unit in operational mode----*/
     case 0x0000:
       asm nop;
     break;
     /*----Control Command-----------*/
     case 0x211:
     asm nop;
     break;
    /*----Clear Errors---------*/
     case 0x610:
     asm nop;
     break;
   }
  }
}
//---------------------------------------------------------------------------
void WriteCAN(void)
{
 //----------------------------------------------
 // Write CAN
 //----------------------------------------------
   TxData[0] = (MotorSpeed &0xFF);
   TxData[1] = (MotorSpeed >> 8);
   TxData[2] = (Period &0xFF);
   TxData[3] = (Period >> 8);
   TxData[4] = 0;
   TxData[5] = 0;
   TxData[6] = 0;
   TxData[7] = 0;
   TxDataLen = 8;
   TxID = 0x191; //0x180 + Node ID (0x11) = 0x191
   ECAN1Write(TxID,TxData,TxDataLen,Can_Send_Flags);
   Delay_ms(20);
}

//---------------------------------------------------------------------------
void InitCAN(void)
{
  /*Clear CAN Interrupt Flag */
   IFS2bits.C1IF = 0;
  /*Enable ECAN1 Interrupt */
  IEC2bits.C1IE   = 1;             //enable ECAN1 interrupts
  C1INTEbits.TBIE = 1;             //enable ECAN1 tx interrupt
  C1INTEbits.RBIE = 1;             //enable ECAN1 rx interrupt
  Can_Init_Flags = 0;              //
  Can_Send_Flags = 0;              // clear flags
  Can_Rcv_Flags  = 0;              //

  Can_Send_Flags = _ECAN_TX_PRIORITY_0 &
                   _ECAN_TX_STD_FRAME &
                   _ECAN_TX_NO_RTR_FRAME;

  Can_Init_Flags = _ECAN_CONFIG_SAMPLE_THRICE &
                   _ECAN_CONFIG_PHSEG2_PRG_ON &
                   _ECAN_CONFIG_STD_MSG &
                   _ECAN_CONFIG_MATCH_MSG_TYPE &
                   _ECAN_CONFIG_LINE_FILTER_OFF;

   //init dma channel 0 for dma to ECAN peripheral transfer
  ECAN1DmaChannelInit(0, 1, &ECAN1RxTxRAMBuffer);
  // init dma channel 2 for ECAN peripheral to dma transfer
  ECAN1DmaChannelInit(2, 0, &ECAN1RxTxRAMBuffer);
  //250Kb/sec@40MHz(20MIPS CPU)
  ECAN1Initialize(1, 4, 3, 3, 3, Can_Init_Flags);
  //set number of rx+tx buffers in DMA RAM
  ECAN1SetBufferSize(ECAN1RAMBUFFERSIZE);
  //select transmit buffers 0x000F = buffers 0:3 are transmit buffers
  ECAN1SelectTxBuffers(0x000F);
  ECAN1SetOperationMode(_ECAN_MODE_CONFIG,0xFF);   // set CONFIGURATION mode
   // set all mask1 bits to ones
  ECAN1SetMask(_ECAN_MASK_0, -1,_ECAN_CONFIG_MATCH_MSG_TYPE &
                _ECAN_CONFIG_STD_MSG);
  // set all mask2 bits to ones
  ECAN1SetMask(_ECAN_MASK_1, -1,_ECAN_CONFIG_MATCH_MSG_TYPE &
               _ECAN_CONFIG_STD_MSG);
  // set all mask3 bits to ones
  ECAN1SetMask(_ECAN_MASK_2, -1, _ECAN_CONFIG_MATCH_MSG_TYPE &
               _ECAN_CONFIG_STD_MSG);
  ECAN1SetFilter(_ECAN_FILTER_10, 0x211,_ECAN_MASK_2,_ECAN_RX_BUFFER_7,
                 _ECAN_CONFIG_STD_MSG);
   // set NORMAL mode
  ECAN1SetOperationMode(_ECAN_MODE_NORMAL, 0xFF);
  Msg_Rcvd = 0;
}