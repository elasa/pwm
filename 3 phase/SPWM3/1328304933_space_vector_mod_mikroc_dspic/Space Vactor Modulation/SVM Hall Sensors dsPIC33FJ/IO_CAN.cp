#line 1 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/IO_CAN.c"
#line 1 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/io_can.h"




void InitCAN(void);
void ReadCAN(void);
void WriteCAN(void);
#line 1 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/ecan_defs.h"
#line 13 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/ecan_defs.h"
typedef
 struct {
 struct {
 IDE : 1;
 SRR : 1;
 SID : 11;
 : 3;
 } CxTRBnSID;
 struct {
 EID17_6 : 12;
 : 4;
 } CxTRBnEID;
 struct {
 DLC : 4;
 RB0 : 1;
 : 3;
 RB1 : 1;
 RTR : 1;
 EID5_0 : 6;
 } CxTRBnDLC;
 struct d{
 char Data[8];
 } CxTRBnData;
 struct {
 : 8;
 FILHIT : 5;

 : 3;
 } CxTRBnSTAT;
 } __RxTxBuffer;
#line 57 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/ecan_defs.h"
unsigned dma ECAN1RamStartAddress = 0x4000;

__RxTxBuffer dma ECAN1RxTxRAMBuffer[ 16 ] absolute 0x4000;
#line 8 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/IO_CAN.c"
unsigned int Can_Init_Flags, Can_Send_Flags, Can_Rcv_Flags;
unsigned int RxDataLen;
unsigned int TxDataLen;
char RxData[8];
char TxData[8];
char Msg_Rcvd;
unsigned long RxID;
unsigned long TxID;

struct CanControlFlags
{
unsigned SetOperational :1;
unsigned NonOperational :1;
unsigned ErrorState :1;
unsigned Tx :1;
unsigned unused :12;
}ControlFlag;





extern signed int MotorSpeed;
extern unsigned int Period;

void ReadCAN(void)
{



 Msg_Rcvd = ECAN1Read(&RxID,RxData,&RxDataLen,&Can_Rcv_Flags);
 if( Msg_Rcvd!=0)
 {
 switch(RxID)
 {

 case 0x0000:
 asm nop;
 break;

 case 0x211:
 asm nop;
 break;

 case 0x610:
 asm nop;
 break;
 }
 }
}

void WriteCAN(void)
{



 TxData[0] = (MotorSpeed &0xFF);
 TxData[1] = (MotorSpeed >> 8);
 TxData[2] = (Period &0xFF);
 TxData[3] = (Period >> 8);
 TxData[4] = 0;
 TxData[5] = 0;
 TxData[6] = 0;
 TxData[7] = 0;
 TxDataLen = 8;
 TxID = 0x191;
 ECAN1Write(TxID,TxData,TxDataLen,Can_Send_Flags);
 Delay_ms(20);
}


void InitCAN(void)
{

 IFS2bits.C1IF = 0;

 IEC2bits.C1IE = 1;
 C1INTEbits.TBIE = 1;
 C1INTEbits.RBIE = 1;
 Can_Init_Flags = 0;
 Can_Send_Flags = 0;
 Can_Rcv_Flags = 0;

 Can_Send_Flags = _ECAN_TX_PRIORITY_0 &
 _ECAN_TX_STD_FRAME &
 _ECAN_TX_NO_RTR_FRAME;

 Can_Init_Flags = _ECAN_CONFIG_SAMPLE_THRICE &
 _ECAN_CONFIG_PHSEG2_PRG_ON &
 _ECAN_CONFIG_STD_MSG &
 _ECAN_CONFIG_MATCH_MSG_TYPE &
 _ECAN_CONFIG_LINE_FILTER_OFF;


 ECAN1DmaChannelInit(0, 1, &ECAN1RxTxRAMBuffer);

 ECAN1DmaChannelInit(2, 0, &ECAN1RxTxRAMBuffer);

 ECAN1Initialize(1, 4, 3, 3, 3, Can_Init_Flags);

 ECAN1SetBufferSize( 16 );

 ECAN1SelectTxBuffers(0x000F);
 ECAN1SetOperationMode(_ECAN_MODE_CONFIG,0xFF);

 ECAN1SetMask(_ECAN_MASK_0, -1,_ECAN_CONFIG_MATCH_MSG_TYPE &
 _ECAN_CONFIG_STD_MSG);

 ECAN1SetMask(_ECAN_MASK_1, -1,_ECAN_CONFIG_MATCH_MSG_TYPE &
 _ECAN_CONFIG_STD_MSG);

 ECAN1SetMask(_ECAN_MASK_2, -1, _ECAN_CONFIG_MATCH_MSG_TYPE &
 _ECAN_CONFIG_STD_MSG);
 ECAN1SetFilter(_ECAN_FILTER_10, 0x211,_ECAN_MASK_2,_ECAN_RX_BUFFER_7,
 _ECAN_CONFIG_STD_MSG);

 ECAN1SetOperationMode(_ECAN_MODE_NORMAL, 0xFF);
 Msg_Rcvd = 0;
}
