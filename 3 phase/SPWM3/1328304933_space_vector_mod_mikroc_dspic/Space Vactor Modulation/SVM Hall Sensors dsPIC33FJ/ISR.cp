#line 1 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/ISR.c"
#line 1 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/isr.h"
#line 5 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/isr.h"
void SpeedControl(void);
void ForceCommutation(void);
void StopMotor(void);
void RunMotor(void);
void ChargeBootstraps(void);
void Delay(unsigned int delay_count);
void SetPID(void);
void debug(void);
#line 1 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/mccontrolvariables.h"



typedef signed int SFRAC16;
#line 53 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/mccontrolvariables.h"
const int PhaseValues[6] =
{ 57344 ,  (( 57344  + 65536/6) % 65536) ,  (( (( 57344  + 65536/6) % 65536)  + 65536/6) % 65536) ,  (( (( (( 57344  + 65536/6) % 65536)  + 65536/6) % 65536)  + 65536/6) % 65536) ,  (( (( (( (( 57344  + 65536/6) % 65536)  + 65536/6) % 65536)  + 65536/6) % 65536)  + 65536/6) % 65536) ,  (( (( (( (( (( 57344  + 65536/6) % 65536)  + 65536/6) % 65536)  + 65536/6) % 65536)  + 65536/6) % 65536)  + 65536/6) % 65536) };




int PhaseOffset = 6000;

unsigned int Phase;



signed int PhaseInc;


signed int PhaseAdvance;


unsigned int HallValue;
unsigned int Sector;

unsigned int LastSector;


unsigned int MotorStalledCounter = 0;








char SectorTable[] = {-1,4,2,3,0,5,1,-1};

unsigned char Current_Direction;

unsigned char Required_Direction;





unsigned int PastCapture, ActualCapture, Period;


unsigned int temp_count;
SFRAC16 _MINPERIOD =  313  - 1;
SFRAC16 MeasuredSpeed,RefSpeed = 200;

SFRAC16 ControlOutput = 0;



SFRAC16 Kp ;
SFRAC16 Ki ;
SFRAC16 Kd ;





SFRAC16 _MAX_PH_ADV =  (int)(((float) 40  / 360.0) * 65536.0) ;


struct MotorFlags
{
 unsigned MotorRunning :1;
 unsigned unused :15;
}Flags;
#line 1 "d:/mikroe projects/mikroc for dspic projects/svm hall sensors dspic33fj/svm.h"





void SVM(int volts, unsigned int angle);
#line 9 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/ISR.c"
signed int MotorSpeed;
float float_measuredspeed;



void C1Interrupt(void)iv IVT_ADDR_C1INTERRUPT
{
 IFS2bits.C1IF = 0;
 if(C1INTFbits.TBIF){
 C1INTFbits.TBIF = 0;}

 if(C1INTFbits.RBIF){
 C1INTFbits.RBIF = 0;}
}



void Timer1Int(void) iv IVT_ADDR_T1INTERRUPT
{
 IFS0bits.T1IF = 0;
 Period = ActualCapture - PastCapture;




 if (Period < (unsigned int) 313 )
 Period =  313 ;
 else if (Period > (unsigned int) 31250 )
 Period =  31250 ;









 PhaseInc = (signed int)(512000UL/Period);







 float_measuredspeed = (float) 313 /Period ;
 MeasuredSpeed = Q15_Ftoi(float_measuredspeed);




 if (Current_Direction ==  1 )
 { MeasuredSpeed = -MeasuredSpeed ;}










 SpeedControl();
#line 85 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/ISR.c"
 A = DSP_MPY(_MAX_PH_ADV,MeasuredSpeed,0, 0, 0, 0,0, 0);
 PhaseAdvance = DSP_SAC(A,0);


 MotorStalledCounter++;




 if ((MotorStalledCounter %  10 ) == 0)
 {
 ForceCommutation();

 }
 else if (MotorStalledCounter >=  100 )
 {
 StopMotor();

 }
}



void IC1Interrupt(void) iv IVT_ADDR_IC1INTERRUPT
{
 IFS0bits.IC1IF = 0;
 HallValue = (unsigned int)((PORTB >> 1) & 0x0007);
 Sector = SectorTable[HallValue];


 if (Sector != LastSector)
 {


 MotorStalledCounter = 0;

 if ((Sector == 5) || (Sector == 2))
 Current_Direction =  1 ;
 else
 Current_Direction =  0 ;


 if (Required_Direction ==  0 )
 {
 Phase = PhaseValues[Sector];
 }
 else
 {


 Phase = PhaseValues[(Sector + 3) % 6] + PhaseOffset;
 }
 LastSector = Sector;
 }
}



void IC2Interrupt(void) iv IVT_ADDR_IC2INTERRUPT
{
 IFS0.IC2IF = 0;
 HallValue = (unsigned int)((PORTB >> 1) & 0x0007);
 Sector = SectorTable[HallValue];

 if (Sector != LastSector)
 {

 PastCapture = ActualCapture;
 ActualCapture = IC2BUF;


 MotorStalledCounter = 0;

 if ((Sector == 3) || (Sector == 0))
 Current_Direction =  1 ;
 else
 Current_Direction =  0 ;


 if (Required_Direction ==  0 )
 {
 Phase = PhaseValues[Sector];
 }
 else
 {


 Phase = PhaseValues[(Sector + 3) % 6] + PhaseOffset;
 }
 LastSector = Sector;
 }
}



void IC7Interrupt(void) iv IVT_ADDR_IC7INTERRUPT
{
 IFS1.IC7IF = 0;
 HallValue = (unsigned int)((PORTB >> 1) & 0x0007);
 Sector = SectorTable[HallValue];

 if (Sector != LastSector)
 {


 MotorStalledCounter = 0;

 if ((Sector == 1) || (Sector == 4))
 Current_Direction =  1 ;
 else
 Current_Direction =  0 ;


 if (Required_Direction ==  0 )
 {
 Phase = PhaseValues[Sector];
 }
 else
 {


 Phase = PhaseValues[(Sector + 3) % 6] + PhaseOffset;
 }
 LastSector = Sector;
 }
}



void PmwInterrupt(void) iv IVT_ADDR_MPWM1INTERRUPT
{
 IFS3bits.PWM1IF = 0;
 if (Required_Direction ==  0 )
 {
 if (Current_Direction ==  0 )
 Phase += PhaseInc;



 asm nop;
 SVM(ControlOutput, Phase + PhaseAdvance);
#line 232 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/ISR.c"
 }
 else
 {
 if (Current_Direction ==  1 )
 Phase -= PhaseInc;




 SVM(-(ControlOutput+1),Phase + PhaseAdvance);
#line 247 "D:/mikroE Projects/mikroC for dsPIC Projects/SVM Hall Sensors dsPIC33FJ/ISR.c"
 }
}



void AdcInterrupt(void) iv IVT_ADDR_ADC1INTERRUPT
{
 IFS0bits.AD1IF = 0;
 RefSpeed = ADC1BUF0;

}

void SpeedControl(void)
{








 ControlOutput = RefSpeed;


 if (ControlOutput < 0)
 Required_Direction =  1 ;
 else
 Required_Direction =  0 ;

}

void ForceCommutation(void)
{
 HallValue = (unsigned int)((PORTB >> 1) & 0x0007);
 Sector = SectorTable[HallValue];
 if (Sector != -1)
 {

 if (Required_Direction ==  0 )
 {

 Phase = PhaseValues[Sector];
 }
 else
 {


 Phase = PhaseValues[(Sector + 3) % 6] + PhaseOffset;
 }
 }
}



void RunMotor(void)
{
 ChargeBootstraps();
 TMR1 = 0;
 TMR3 = 0;
 ActualCapture =  31250 ;

 PastCapture = 0;


 HallValue = (unsigned int)((PORTB >> 1) & 0x0007);
 LastSector = Sector = SectorTable[HallValue];





 if (RefSpeed < 0)
 {
 ControlOutput = 0;
 Current_Direction = Required_Direction =  1 ;
 Phase = PhaseValues[(Sector + 3) % 6] + PhaseOffset;
 }
 else
 {
 ControlOutput = 0;
 Current_Direction = Required_Direction =  0 ;
 Phase = PhaseValues[Sector];
 }

 MotorStalledCounter = 0;


 PhaseInc = (signed int)(512000UL/ 31250 );


 IFS0.T1IF = 0;
 IFS0bits.IC1IF = 0;
 IFS0bits.IC2IF = 0;
 IFS1bits.IC7IF = 0;
 IFS3bits.PWM1IF = 0;

 IEC0bits.T1IE = 1;
 IEC0bits.IC1IE = 1;
 IEC0bits.IC2IE = 1;
 IEC1bits.IC7IE = 1;
 IEC3bits.PWM1IE = 1;
 DISICNT = 0;
 Flags.MotorRunning = 1;
}

void StopMotor(void)
{
 OVDCON = 0x0000;

 IEC0bits.T1IE = 0;
 IEC0bits.IC1IE = 0;
 IEC0bits.IC2IE = 0;
 IEC1bits.IC7IE = 0;
 IEC3bits.PWM1IE = 0;
 DISICNT = 0;
 Flags.MotorRunning = 0;
}



void ChargeBootstraps(void)
{
 unsigned int i;
 OVDCON = 0x0015;
 for (i = 0; i < 33330; i++);
 PWMCON2bits.UDIS = 1;
 PDC1 = PTPER;
 PDC2 = PTPER;
 PDC3 = PTPER;
 OVDCON = 0x3F00;
 PWMCON2bits.UDIS = 0;
}

void Delay(unsigned int delay_count)
{
 volatile int i;
 while (delay_count-- > 0)
 {
 for (i = 0;i < 1000;i++);
 }
}

void SetPID(void)
{



 Kp = Q15_Ftoi(0.1);
 Ki = Q15_Ftoi(0.01);
 Kd = Q15_Ftoi(0.001);
}

void debug(void)
{
 Delay_ms(20);
 MotorSpeed = (signed int)(float_measuredspeed * 6000);
 if(Current_Direction ==  1 ){MotorSpeed = -MotorSpeed ;}
}
