//********************************************************************
// Config .c
// This module handles all Configurations of Micro Controller
//--------------------------------------------------------------------
#include "Config.h"
//--------------------------------------------------------------------
#define FCY  20000000    // xtal = 8Mhz; with PLL -> 20 MIPS
#define FPWM 20000       // 20 kHz,so that no audible noise is present.
//----------------------------------------------------------------------


//--------------------------------------------------------------------
// IO Mapping
//--------------------------------------------------------------------
void IOMap()
{
  NSTDIS_bit = 1;        //Block Nestet Interrupts follow Priority order
  AD1PCFGL = 0xFFFF;    // PORTB pins as Digital
//---------------------------------------------------------
// Configure IC Pins for Hall Sensors and CAN pins
//---------------------------------------------------------
  Unlock_IOLOCK();
  PPS_Mapping_NoLock(8,_INPUT ,_FLTA1);
  PPS_Mapping_NoLock(1,_INPUT ,_IC1); // IC1 RP1 RB1- RB3
  PPS_Mapping_NoLock(2,_INPUT ,_IC2); // IC2 RP2
  PPS_Mapping_NoLock(3,_INPUT ,_IC7); // IC7 RP3
  PPS_Mapping_NoLock(9,_OUTPUT,_C1TX);
  PPS_Mapping_NoLock(7,_INPUT,_CIRX);
  Lock_IOLOCK();
 //-------------------------------------------------------
 // Set Pin Directions
 //-----------------------------------------------------
  TRISBbits.TRISB15 = 0;        //PWM1L1
  TRISBbits.TRISB14 = 0;        //PWM1H1
  TRISBbits.TRISB13 = 0;        //PWM1L2
  TRISBbits.TRISB12 = 0;        //PWM1H2
  TRISBbits.TRISB11 = 0;        //PWM1L3
  TRISBbits.TRISB10 = 0;        //PWM1H3
  TRISBbits.TRISB1  = 1;
  TRISBbits.TRISB2  = 1;       // RB1-3 as inputs
  TRISBbits.TRISB3  = 1;
  TRISBbits.TRISB9  = 0;       //output for scope
  TRISAbits.TRISA8  = 1;       // RA 8 S2 switch input pin
}
//-------------------------------------------------------------------
// Initialize ADC Module
//-------------------------------------------------------------------
void InitADC()
{
   AD1PCFGL.PCFG8 = 0;   // AN8 analog
  /* Signed Fractional */
    AD1CON1bits.FORM = 3;
  /* Internal Counter Ends Sampling and Starts Conversion */
    AD1CON1bits.SSRC = 3;
  /* Sampling begins immediately after last conversion */
    AD1CON1bits.ASAM = 1;
  /* Select 12-bit, 1 channel ADC operation */
    AD1CON1bits.AD12B = 1;
  /* No channel scan for CH0+, Use MUX A, SMPI = 1 per interrupt,
    Vref = AVdd/AVss */
    AD1CON2 = 0x0000;
  /* Set Samples and bit conversion time */
    AD1CON3 = 0x032F;
  /* No Channels to Scan */
    AD1CSSL = 0x0000;
  /* Channel select AN8 */
    AD1CHS0 = 0x0008;
  /* Reset ADC interrupt flag */
    IFS0bits.AD1IF = 0;
  /* Enable ADC interrupts, disable this interrupt if the DMA is enabled */
    IEC0bits.AD1IE = 1;
  /* Turn on ADC module */
    AD1CON1bits.ADON = 1;
}
//-------------------------------------------------------------------
//  Initialize MCPWM
//-------------------------------------------------------------------
void InitMCPWM(void)
{
   PTPER = ((FCY/FPWM - 1) >> 1); // Compute Period based on CPU speed and
                        // required PWM frequency (see defines)
   OVDCON = 0x0000;     // Disable all PWM outputs.
   DTCON1 = 0x0028;     // 2 us of dead time
   PWMCON1 = 0x0077;    // Enable PWM output pins and configure them as
                        // complementary mode
   PDC1 = PTPER;        // Initialize as 0 voltage
   PDC2 = PTPER;        // Initialize as 0 voltage
   PDC3 = PTPER;        // Initialize as 0 voltage
   SEVTCMP = 1;         // Enable triggering for ADC
   PWMCON2 = 0x0F02;    // 16 postscale values, for achieving 20 kHz
   PTCON = 0x8002;      // start PWM as center aligned mode
   FLTACON = 0x0087;    // All PWM into inactive state, cycle-by-cycle,
}
//-------------------------------------------------------------------------
// Initialize IC Captures
//---------------------------------------------------------------------
void InitIC()
{
//Hall A -> IC1. Hall A is only used for commutation.
//Hall B -> IC2. Hall B is used for Speed measurement and commutation.
//Hall C -> IC7. Hall C is only used for commutation.
 IC1CON = 1;
 IC2CON = 1;
 IC7CON = 1;
 IFS0bits.IC1IF = 0;     // Clear interrupt flag  IC1,IC2;IC7
 IFS0bits.IC2IF = 0;
 IFS1bits.IC7IF = 0;
}
//-------------------------------------------------------------------------
// Initialize Timer 1
//--------------------------------------------------
void InitTMR1(void)
{
   T1CON = 0x0020;  // internal Tcy/64 clock
   TMR1 = 0;
   PR1 = 313;       // 1 ms interrupts for 20 MIPS
   T1CON.TON = 1;   // turn on timer 1
}
//--------------------------------------------------------------------------
// Initialize Timer 3
//---------------------------------------------------
void InitTMR3(void)
{
   T3CON = 0x0020;  // internal Tcy/64 clock
   TMR3 = 0;
   PR3 = 0xFFFF;
   T3CON.TON = 1;  // turn on timer 3
}
//------------------------------------EOF-------------------------------------