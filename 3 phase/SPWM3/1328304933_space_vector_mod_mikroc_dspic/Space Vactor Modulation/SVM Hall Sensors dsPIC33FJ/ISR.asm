
_C1Interrupt:
	PUSH	RCOUNT
	PUSH	W0
	MOV	#2, W0
	REPEAT	#12
	PUSH	[W0++]

;ISR.c,14 :: 		void C1Interrupt(void)iv IVT_ADDR_C1INTERRUPT
;ISR.c,16 :: 		IFS2bits.C1IF = 0;  // clear ECAN interrupt flag
	BCLR	IFS2bits, #3
;ISR.c,17 :: 		if(C1INTFbits.TBIF){// was it tx interrupt?
	BTSS	C1INTFbits, #0
	GOTO	L_C1Interrupt0
;ISR.c,18 :: 		C1INTFbits.TBIF = 0;}// if yes clear tx interrupt flag
	BCLR	C1INTFbits, #0
L_C1Interrupt0:
;ISR.c,20 :: 		if(C1INTFbits.RBIF){ // was it rx interrupt?
	BTSS	C1INTFbits, #1
	GOTO	L_C1Interrupt1
;ISR.c,21 :: 		C1INTFbits.RBIF = 0;} // if yes clear rx interrupt flag
	BCLR	C1INTFbits, #1
L_C1Interrupt1:
;ISR.c,22 :: 		}
L_end_C1Interrupt:
	MOV	#26, W0
	REPEAT	#12
	POP	[W0--]
	POP	W0
	POP	RCOUNT
	RETFIE
; end of _C1Interrupt

_Timer1Int:
	PUSH	RCOUNT
	PUSH	W0
	MOV	#2, W0
	REPEAT	#12
	PUSH	[W0++]

;ISR.c,26 :: 		void Timer1Int(void) iv IVT_ADDR_T1INTERRUPT
;ISR.c,28 :: 		IFS0bits.T1IF = 0;
	PUSH	W10
	PUSH	W11
	BCLR	IFS0bits, #3
;ISR.c,29 :: 		Period = ActualCapture - PastCapture;  // This is an UNsigned substraction
	MOV	_ActualCapture, W1
	MOV	#lo_addr(_PastCapture), W0
	SUB	W1, [W0], W1
	MOV	W1, _Period
;ISR.c,34 :: 		if (Period < (unsigned int)MINPERIOD)  // MINPERIOD or 6000 rpm
	MOV	#313, W0
	CP	W1, W0
	BRA LTU	L__Timer1Int63
	GOTO	L_Timer1Int2
L__Timer1Int63:
;ISR.c,35 :: 		Period = MINPERIOD;
	MOV	#313, W0
	MOV	W0, _Period
	GOTO	L_Timer1Int3
L_Timer1Int2:
;ISR.c,36 :: 		else if (Period > (unsigned int)MAXPERIOD) // MAXPERIOD or 60 rpm
	MOV	_Period, W1
	MOV	#31250, W0
	CP	W1, W0
	BRA GTU	L__Timer1Int64
	GOTO	L_Timer1Int4
L__Timer1Int64:
;ISR.c,37 :: 		Period = MAXPERIOD;
	MOV	#31250, W0
	MOV	W0, _Period
L_Timer1Int4:
L_Timer1Int3:
;ISR.c,47 :: 		PhaseInc = (signed int)(512000UL/Period);
	MOV	_Period, W2
	CLR	W3
	MOV	#53248, W0
	MOV	#7, W1
	CLR	W4
	CALL	__Divide_32x32
	MOV	W0, _PhaseInc
;ISR.c,55 :: 		float_measuredspeed = (float)MINPERIOD/Period ;
	MOV	_Period, W0
	CLR	W1
	CALL	__Long2Float
	MOV.D	W0, W2
	MOV	#32768, W0
	MOV	#17308, W1
	CALL	__Div_FP
	MOV	W0, _float_measuredspeed
	MOV	W1, _float_measuredspeed+2
;ISR.c,56 :: 		MeasuredSpeed = Q15_Ftoi(float_measuredspeed);
	MOV.D	W0, W10
	CALL	_Q15_Ftoi
	MOV	W0, _MeasuredSpeed
;ISR.c,61 :: 		if (Current_Direction == CCW)
	MOV	#lo_addr(_Current_Direction), W0
	MOV.B	[W0], W0
	CP.B	W0, #1
	BRA Z	L__Timer1Int65
	GOTO	L_Timer1Int5
L__Timer1Int65:
;ISR.c,62 :: 		{ MeasuredSpeed = -MeasuredSpeed ;}
	MOV	_MeasuredSpeed, W1
	MOV	#lo_addr(_MeasuredSpeed), W0
	SUBR	W1, #0, [W0]
L_Timer1Int5:
;ISR.c,73 :: 		SpeedControl(); // Speed PID controller is called here. It will use
	CALL	_SpeedControl
;ISR.c,85 :: 		A = DSP_MPY(_MAX_PH_ADV,MeasuredSpeed,0, 0, 0, 0,0, 0);
	MOV	__MAX_PH_ADV, W6
	MOV	_MeasuredSpeed, W7
	MPY	W6*W7, A
;ISR.c,86 :: 		PhaseAdvance  = DSP_SAC(A,0);
	MOV	#lo_addr(_PhaseAdvance), W9
	SAC	A, #0, [W9]
;ISR.c,89 :: 		MotorStalledCounter++;// We increment a timeout variable to see if the
	MOV	#1, W1
	MOV	#lo_addr(_MotorStalledCounter), W0
	ADD	W1, [W0], [W0]
;ISR.c,94 :: 		if ((MotorStalledCounter % _10MILLISEC) == 0)
	MOV	_MotorStalledCounter, W0
	MOV	#10, W2
	REPEAT	#17
	DIV.U	W0, W2
	MOV	W1, W0
	CP	W0, #0
	BRA Z	L__Timer1Int66
	GOTO	L_Timer1Int6
L__Timer1Int66:
;ISR.c,96 :: 		ForceCommutation();// Force Commutation if no hall sensor changes
	CALL	_ForceCommutation
;ISR.c,98 :: 		}
	GOTO	L_Timer1Int7
L_Timer1Int6:
;ISR.c,99 :: 		else if (MotorStalledCounter >= _100MILLISEC)
	MOV	#100, W1
	MOV	#lo_addr(_MotorStalledCounter), W0
	CP	W1, [W0]
	BRA LEU	L__Timer1Int67
	GOTO	L_Timer1Int8
L__Timer1Int67:
;ISR.c,101 :: 		StopMotor(); // Stop motor is no hall changes have occured in
	CALL	_StopMotor
;ISR.c,103 :: 		}
L_Timer1Int8:
L_Timer1Int7:
;ISR.c,104 :: 		}
L_end_Timer1Int:
	POP	W11
	POP	W10
	MOV	#26, W0
	REPEAT	#12
	POP	[W0--]
	POP	W0
	POP	RCOUNT
	RETFIE
; end of _Timer1Int

_IC1Interrupt:
	PUSH	RCOUNT
	PUSH	W0
	MOV	#2, W0
	REPEAT	#12
	PUSH	[W0++]

;ISR.c,108 :: 		void IC1Interrupt(void) iv IVT_ADDR_IC1INTERRUPT
;ISR.c,110 :: 		IFS0bits.IC1IF = 0;        // Clear interrupt flag
	BCLR	IFS0bits, #1
;ISR.c,111 :: 		HallValue = (unsigned int)((PORTB >> 1) & 0x0007); // Read halls
	MOV	PORTB, WREG
	LSR	W0, #1, W0
	AND	W0, #7, W1
	MOV	W1, _HallValue
;ISR.c,112 :: 		Sector = SectorTable[HallValue];        // Get Sector from table
	MOV	#lo_addr(_SectorTable), W0
	ADD	W0, W1, W0
	MOV.B	[W0], W0
	ZE	W0, W0
	MOV	W0, _Sector
;ISR.c,115 :: 		if (Sector != LastSector)
	MOV	_Sector, W1
	MOV	#lo_addr(_LastSector), W0
	CP	W1, [W0]
	BRA NZ	L__IC1Interrupt69
	GOTO	L_IC1Interrupt9
L__IC1Interrupt69:
;ISR.c,119 :: 		MotorStalledCounter = 0;
	CLR	W0
	MOV	W0, _MotorStalledCounter
;ISR.c,121 :: 		if ((Sector == 5) || (Sector == 2))
	MOV	_Sector, W0
	CP	W0, #5
	BRA NZ	L__IC1Interrupt70
	GOTO	L__IC1Interrupt54
L__IC1Interrupt70:
	MOV	_Sector, W0
	CP	W0, #2
	BRA NZ	L__IC1Interrupt71
	GOTO	L__IC1Interrupt53
L__IC1Interrupt71:
	GOTO	L_IC1Interrupt12
L__IC1Interrupt54:
L__IC1Interrupt53:
;ISR.c,122 :: 		Current_Direction = CCW;
	MOV	#lo_addr(_Current_Direction), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	GOTO	L_IC1Interrupt13
L_IC1Interrupt12:
;ISR.c,124 :: 		Current_Direction = CW;
	MOV	#lo_addr(_Current_Direction), W1
	CLR	W0
	MOV.B	W0, [W1]
L_IC1Interrupt13:
;ISR.c,127 :: 		if (Required_Direction == CW)
	MOV	#lo_addr(_Required_Direction), W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA Z	L__IC1Interrupt72
	GOTO	L_IC1Interrupt14
L__IC1Interrupt72:
;ISR.c,129 :: 		Phase = PhaseValues[Sector];
	MOV	_Sector, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_PhaseValues), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
	MOV	W0, _Phase
;ISR.c,130 :: 		}
	GOTO	L_IC1Interrupt15
L_IC1Interrupt14:
;ISR.c,135 :: 		Phase = PhaseValues[(Sector + 3) % 6] + PhaseOffset;
	MOV	_Sector, W0
	ADD	W0, #3, W1
	MOV	#6, W2
	REPEAT	#17
	DIV.U	W1, W2
	MOV	W1, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_PhaseValues), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W2
	MOV	#lo_addr(_PhaseOffset), W1
	MOV	#lo_addr(_Phase), W0
	ADD	W2, [W1], [W0]
;ISR.c,136 :: 		}
L_IC1Interrupt15:
;ISR.c,137 :: 		LastSector = Sector; // Update last sector
	MOV	_Sector, W0
	MOV	W0, _LastSector
;ISR.c,138 :: 		}
L_IC1Interrupt9:
;ISR.c,139 :: 		}
L_end_IC1Interrupt:
	MOV	#26, W0
	REPEAT	#12
	POP	[W0--]
	POP	W0
	POP	RCOUNT
	RETFIE
; end of _IC1Interrupt

_IC2Interrupt:
	PUSH	RCOUNT
	PUSH	W0
	MOV	#2, W0
	REPEAT	#12
	PUSH	[W0++]

;ISR.c,143 :: 		void IC2Interrupt(void) iv IVT_ADDR_IC2INTERRUPT
;ISR.c,145 :: 		IFS0.IC2IF = 0;        // Clear interrupt flag
	BCLR	IFS0, #5
;ISR.c,146 :: 		HallValue = (unsigned int)((PORTB >> 1) & 0x0007);        // Read halls
	MOV	PORTB, WREG
	LSR	W0, #1, W0
	AND	W0, #7, W1
	MOV	W1, _HallValue
;ISR.c,147 :: 		Sector = SectorTable[HallValue];    // Get Sector from table
	MOV	#lo_addr(_SectorTable), W0
	ADD	W0, W1, W0
	MOV.B	[W0], W0
	ZE	W0, W0
	MOV	W0, _Sector
;ISR.c,149 :: 		if (Sector != LastSector)
	MOV	_Sector, W1
	MOV	#lo_addr(_LastSector), W0
	CP	W1, [W0]
	BRA NZ	L__IC2Interrupt74
	GOTO	L_IC2Interrupt16
L__IC2Interrupt74:
;ISR.c,152 :: 		PastCapture = ActualCapture;
	MOV	_ActualCapture, W0
	MOV	W0, _PastCapture
;ISR.c,153 :: 		ActualCapture = IC2BUF;
	MOV	IC2BUF, WREG
	MOV	W0, _ActualCapture
;ISR.c,156 :: 		MotorStalledCounter = 0;
	CLR	W0
	MOV	W0, _MotorStalledCounter
;ISR.c,158 :: 		if ((Sector == 3) || (Sector == 0))
	MOV	_Sector, W0
	CP	W0, #3
	BRA NZ	L__IC2Interrupt75
	GOTO	L__IC2Interrupt57
L__IC2Interrupt75:
	MOV	_Sector, W0
	CP	W0, #0
	BRA NZ	L__IC2Interrupt76
	GOTO	L__IC2Interrupt56
L__IC2Interrupt76:
	GOTO	L_IC2Interrupt19
L__IC2Interrupt57:
L__IC2Interrupt56:
;ISR.c,159 :: 		Current_Direction = CCW;
	MOV	#lo_addr(_Current_Direction), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	GOTO	L_IC2Interrupt20
L_IC2Interrupt19:
;ISR.c,161 :: 		Current_Direction = CW;
	MOV	#lo_addr(_Current_Direction), W1
	CLR	W0
	MOV.B	W0, [W1]
L_IC2Interrupt20:
;ISR.c,164 :: 		if (Required_Direction == CW)
	MOV	#lo_addr(_Required_Direction), W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA Z	L__IC2Interrupt77
	GOTO	L_IC2Interrupt21
L__IC2Interrupt77:
;ISR.c,166 :: 		Phase = PhaseValues[Sector];
	MOV	_Sector, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_PhaseValues), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
	MOV	W0, _Phase
;ISR.c,167 :: 		}
	GOTO	L_IC2Interrupt22
L_IC2Interrupt21:
;ISR.c,172 :: 		Phase = PhaseValues[(Sector + 3) % 6] + PhaseOffset;
	MOV	_Sector, W0
	ADD	W0, #3, W1
	MOV	#6, W2
	REPEAT	#17
	DIV.U	W1, W2
	MOV	W1, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_PhaseValues), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W2
	MOV	#lo_addr(_PhaseOffset), W1
	MOV	#lo_addr(_Phase), W0
	ADD	W2, [W1], [W0]
;ISR.c,173 :: 		}
L_IC2Interrupt22:
;ISR.c,174 :: 		LastSector = Sector; // Update last sector
	MOV	_Sector, W0
	MOV	W0, _LastSector
;ISR.c,175 :: 		}
L_IC2Interrupt16:
;ISR.c,176 :: 		}
L_end_IC2Interrupt:
	MOV	#26, W0
	REPEAT	#12
	POP	[W0--]
	POP	W0
	POP	RCOUNT
	RETFIE
; end of _IC2Interrupt

_IC7Interrupt:
	PUSH	RCOUNT
	PUSH	W0
	MOV	#2, W0
	REPEAT	#12
	PUSH	[W0++]

;ISR.c,180 :: 		void IC7Interrupt(void) iv IVT_ADDR_IC7INTERRUPT
;ISR.c,182 :: 		IFS1.IC7IF = 0;        // Clear interrupt flag
	BCLR	IFS1, #6
;ISR.c,183 :: 		HallValue = (unsigned int)((PORTB >> 1) & 0x0007);// Read halls
	MOV	PORTB, WREG
	LSR	W0, #1, W0
	AND	W0, #7, W1
	MOV	W1, _HallValue
;ISR.c,184 :: 		Sector = SectorTable[HallValue];        // Get Sector from table
	MOV	#lo_addr(_SectorTable), W0
	ADD	W0, W1, W0
	MOV.B	[W0], W0
	ZE	W0, W0
	MOV	W0, _Sector
;ISR.c,186 :: 		if (Sector != LastSector)
	MOV	_Sector, W1
	MOV	#lo_addr(_LastSector), W0
	CP	W1, [W0]
	BRA NZ	L__IC7Interrupt79
	GOTO	L_IC7Interrupt23
L__IC7Interrupt79:
;ISR.c,190 :: 		MotorStalledCounter = 0;
	CLR	W0
	MOV	W0, _MotorStalledCounter
;ISR.c,192 :: 		if ((Sector == 1) || (Sector == 4))
	MOV	_Sector, W0
	CP	W0, #1
	BRA NZ	L__IC7Interrupt80
	GOTO	L__IC7Interrupt60
L__IC7Interrupt80:
	MOV	_Sector, W0
	CP	W0, #4
	BRA NZ	L__IC7Interrupt81
	GOTO	L__IC7Interrupt59
L__IC7Interrupt81:
	GOTO	L_IC7Interrupt26
L__IC7Interrupt60:
L__IC7Interrupt59:
;ISR.c,193 :: 		Current_Direction = CCW;
	MOV	#lo_addr(_Current_Direction), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	GOTO	L_IC7Interrupt27
L_IC7Interrupt26:
;ISR.c,195 :: 		Current_Direction = CW;
	MOV	#lo_addr(_Current_Direction), W1
	CLR	W0
	MOV.B	W0, [W1]
L_IC7Interrupt27:
;ISR.c,198 :: 		if (Required_Direction == CW)
	MOV	#lo_addr(_Required_Direction), W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA Z	L__IC7Interrupt82
	GOTO	L_IC7Interrupt28
L__IC7Interrupt82:
;ISR.c,200 :: 		Phase = PhaseValues[Sector];
	MOV	_Sector, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_PhaseValues), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
	MOV	W0, _Phase
;ISR.c,201 :: 		}
	GOTO	L_IC7Interrupt29
L_IC7Interrupt28:
;ISR.c,206 :: 		Phase = PhaseValues[(Sector + 3) % 6] + PhaseOffset;
	MOV	_Sector, W0
	ADD	W0, #3, W1
	MOV	#6, W2
	REPEAT	#17
	DIV.U	W1, W2
	MOV	W1, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_PhaseValues), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W2
	MOV	#lo_addr(_PhaseOffset), W1
	MOV	#lo_addr(_Phase), W0
	ADD	W2, [W1], [W0]
;ISR.c,207 :: 		}
L_IC7Interrupt29:
;ISR.c,208 :: 		LastSector = Sector; // Update last sector
	MOV	_Sector, W0
	MOV	W0, _LastSector
;ISR.c,209 :: 		}
L_IC7Interrupt23:
;ISR.c,210 :: 		}
L_end_IC7Interrupt:
	MOV	#26, W0
	REPEAT	#12
	POP	[W0--]
	POP	W0
	POP	RCOUNT
	RETFIE
; end of _IC7Interrupt

_PmwInterrupt:
	PUSH	RCOUNT
	PUSH	W0
	MOV	#2, W0
	REPEAT	#12
	PUSH	[W0++]

;ISR.c,214 :: 		void PmwInterrupt(void) iv IVT_ADDR_MPWM1INTERRUPT
;ISR.c,216 :: 		IFS3bits.PWM1IF = 0;// Clear interrupt flag
	PUSH	W10
	PUSH	W11
	BCLR	IFS3bits, #9
;ISR.c,217 :: 		if (Required_Direction == CW)
	MOV	#lo_addr(_Required_Direction), W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA Z	L__PmwInterrupt84
	GOTO	L_PmwInterrupt30
L__PmwInterrupt84:
;ISR.c,219 :: 		if (Current_Direction == CW)
	MOV	#lo_addr(_Current_Direction), W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA Z	L__PmwInterrupt85
	GOTO	L_PmwInterrupt31
L__PmwInterrupt85:
;ISR.c,220 :: 		Phase += PhaseInc;   // Increment Phase if CW to generate the
	MOV	_PhaseInc, W1
	MOV	#lo_addr(_Phase), W0
	ADD	W1, [W0], [W0]
L_PmwInterrupt31:
;ISR.c,224 :: 		asm nop;
	NOP
;ISR.c,225 :: 		SVM(ControlOutput, Phase + PhaseAdvance);// PhaseAdvance addition
	MOV	_Phase, W1
	MOV	#lo_addr(_PhaseAdvance), W0
	ADD	W1, [W0], W0
	MOV	W0, W11
	MOV	_ControlOutput, W10
	CALL	_SVM
;ISR.c,232 :: 		}
	GOTO	L_PmwInterrupt32
L_PmwInterrupt30:
;ISR.c,235 :: 		if (Current_Direction == CCW)
	MOV	#lo_addr(_Current_Direction), W0
	MOV.B	[W0], W0
	CP.B	W0, #1
	BRA Z	L__PmwInterrupt86
	GOTO	L_PmwInterrupt33
L__PmwInterrupt86:
;ISR.c,236 :: 		Phase -= PhaseInc; // Decrement Phase if CCW to generate
	MOV	_PhaseInc, W1
	MOV	#lo_addr(_Phase), W0
	SUBR	W1, [W0], [W0]
L_PmwInterrupt33:
;ISR.c,241 :: 		SVM(-(ControlOutput+1),Phase + PhaseAdvance);//PhaseAdvance addition
	MOV	_Phase, W1
	MOV	#lo_addr(_PhaseAdvance), W0
	ADD	W1, [W0], W1
	MOV	_ControlOutput, W0
	INC	W0
	SUBR	W0, #0, W0
	MOV	W1, W11
	MOV	W0, W10
	CALL	_SVM
;ISR.c,247 :: 		}
L_PmwInterrupt32:
;ISR.c,248 :: 		}
L_end_PmwInterrupt:
	POP	W11
	POP	W10
	MOV	#26, W0
	REPEAT	#12
	POP	[W0--]
	POP	W0
	POP	RCOUNT
	RETFIE
; end of _PmwInterrupt

_AdcInterrupt:
	PUSH	RCOUNT
	PUSH	W0
	MOV	#2, W0
	REPEAT	#12
	PUSH	[W0++]

;ISR.c,252 :: 		void AdcInterrupt(void) iv IVT_ADDR_ADC1INTERRUPT
;ISR.c,254 :: 		IFS0bits.AD1IF = 0;      // Clear interrupt flag
	BCLR	IFS0bits, #13
;ISR.c,255 :: 		RefSpeed = ADC1BUF0; // Read POT value to set Reference Speed
	MOV	ADC1BUF0, WREG
	MOV	W0, _RefSpeed
;ISR.c,257 :: 		}
L_end_AdcInterrupt:
	MOV	#26, W0
	REPEAT	#12
	POP	[W0--]
	POP	W0
	POP	RCOUNT
	RETFIE
; end of _AdcInterrupt

_SpeedControl:

;ISR.c,259 :: 		void SpeedControl(void)
;ISR.c,269 :: 		ControlOutput = RefSpeed;
	MOV	_RefSpeed, W0
	MOV	W0, _ControlOutput
;ISR.c,272 :: 		if (ControlOutput < 0)
	MOV	_RefSpeed, W0
	CP	W0, #0
	BRA LT	L__SpeedControl89
	GOTO	L_SpeedControl34
L__SpeedControl89:
;ISR.c,273 :: 		Required_Direction = CCW;
	MOV	#lo_addr(_Required_Direction), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	GOTO	L_SpeedControl35
L_SpeedControl34:
;ISR.c,275 :: 		Required_Direction = CW;
	MOV	#lo_addr(_Required_Direction), W1
	CLR	W0
	MOV.B	W0, [W1]
L_SpeedControl35:
;ISR.c,277 :: 		}
L_end_SpeedControl:
	RETURN
; end of _SpeedControl

_ForceCommutation:

;ISR.c,279 :: 		void ForceCommutation(void)
;ISR.c,281 :: 		HallValue = (unsigned int)((PORTB >> 1) & 0x0007);  // Read halls
	MOV	PORTB, WREG
	LSR	W0, #1, W0
	AND	W0, #7, W1
	MOV	W1, _HallValue
;ISR.c,282 :: 		Sector = SectorTable[HallValue];        // Read sector based on halls
	MOV	#lo_addr(_SectorTable), W0
	ADD	W0, W1, W0
	MOV.B	[W0], W0
	ZE	W0, W0
	MOV	W0, _Sector
;ISR.c,283 :: 		if (Sector != -1)        // If the sector is invalid don't do anything
	MOV	#65535, W1
	MOV	#lo_addr(_Sector), W0
	CP	W1, [W0]
	BRA NZ	L__ForceCommutation91
	GOTO	L_ForceCommutation36
L__ForceCommutation91:
;ISR.c,286 :: 		if (Required_Direction == CW)
	MOV	#lo_addr(_Required_Direction), W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA Z	L__ForceCommutation92
	GOTO	L_ForceCommutation37
L__ForceCommutation92:
;ISR.c,289 :: 		Phase = PhaseValues[Sector];
	MOV	_Sector, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_PhaseValues), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
	MOV	W0, _Phase
;ISR.c,290 :: 		}
	GOTO	L_ForceCommutation38
L_ForceCommutation37:
;ISR.c,295 :: 		Phase = PhaseValues[(Sector + 3) % 6] + PhaseOffset;
	MOV	_Sector, W0
	ADD	W0, #3, W1
	MOV	#6, W2
	REPEAT	#17
	DIV.U	W1, W2
	MOV	W1, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_PhaseValues), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W2
	MOV	#lo_addr(_PhaseOffset), W1
	MOV	#lo_addr(_Phase), W0
	ADD	W2, [W1], [W0]
;ISR.c,296 :: 		}
L_ForceCommutation38:
;ISR.c,297 :: 		}
L_ForceCommutation36:
;ISR.c,298 :: 		}
L_end_ForceCommutation:
	RETURN
; end of _ForceCommutation

_RunMotor:

;ISR.c,302 :: 		void RunMotor(void)
;ISR.c,304 :: 		ChargeBootstraps();
	CALL	_ChargeBootstraps
;ISR.c,305 :: 		TMR1 = 0;                        // Reset timer 1 for speed control
	CLR	TMR1
;ISR.c,306 :: 		TMR3 = 0;                        // Reset timer 3 for speed measurement
	CLR	TMR3
;ISR.c,307 :: 		ActualCapture = MAXPERIOD;      // Initialize captures for minimum speed
	MOV	#31250, W0
	MOV	W0, _ActualCapture
;ISR.c,309 :: 		PastCapture = 0;
	CLR	W0
	MOV	W0, _PastCapture
;ISR.c,312 :: 		HallValue = (unsigned int)((PORTB >> 1) & 0x0007);// Read halls
	MOV	PORTB, WREG
	LSR	W0, #1, W0
	AND	W0, #7, W1
	MOV	W1, _HallValue
;ISR.c,313 :: 		LastSector = Sector = SectorTable[HallValue];    // Initialize Sector
	MOV	#lo_addr(_SectorTable), W0
	ADD	W0, W1, W0
	MOV.B	[W0], W0
	ZE	W0, W0
	MOV	W0, _Sector
	MOV	_Sector, W0
	MOV	W0, _LastSector
;ISR.c,319 :: 		if (RefSpeed < 0)
	MOV	_RefSpeed, W0
	CP	W0, #0
	BRA LT	L__RunMotor94
	GOTO	L_RunMotor39
L__RunMotor94:
;ISR.c,321 :: 		ControlOutput = 0;        // Initial output voltage
	CLR	W0
	MOV	W0, _ControlOutput
;ISR.c,322 :: 		Current_Direction = Required_Direction = CCW;
	MOV	#lo_addr(_Required_Direction), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Current_Direction), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
;ISR.c,323 :: 		Phase = PhaseValues[(Sector + 3) % 6] + PhaseOffset;
	MOV	_Sector, W0
	ADD	W0, #3, W1
	MOV	#6, W2
	REPEAT	#17
	DIV.U	W1, W2
	MOV	W1, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_PhaseValues), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W2
	MOV	#lo_addr(_PhaseOffset), W1
	MOV	#lo_addr(_Phase), W0
	ADD	W2, [W1], [W0]
;ISR.c,324 :: 		}
	GOTO	L_RunMotor40
L_RunMotor39:
;ISR.c,327 :: 		ControlOutput = 0;        // Initial output voltage
	CLR	W0
	MOV	W0, _ControlOutput
;ISR.c,328 :: 		Current_Direction = Required_Direction = CW;
	MOV	#lo_addr(_Required_Direction), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Current_Direction), W1
	CLR	W0
	MOV.B	W0, [W1]
;ISR.c,329 :: 		Phase = PhaseValues[Sector];
	MOV	_Sector, W0
	SL	W0, #1, W1
	MOV	#lo_addr(_PhaseValues), W0
	ADD	W0, W1, W1
	MOV	#___Lib_System_DefaultPage, W0
	MOV	WREG, 52
	MOV	[W1], W0
	MOV	W0, _Phase
;ISR.c,330 :: 		}
L_RunMotor40:
;ISR.c,332 :: 		MotorStalledCounter = 0;  // Reset motor stalled protection counter
	CLR	W0
	MOV	W0, _MotorStalledCounter
;ISR.c,335 :: 		PhaseInc = (signed int)(512000UL/MAXPERIOD);
	MOV	#16, W0
	MOV	W0, _PhaseInc
;ISR.c,338 :: 		IFS0.T1IF = 0;    // Clear timer 1 flag
	BCLR	IFS0, #3
;ISR.c,339 :: 		IFS0bits.IC1IF = 0;     // Clear interrupt flag  IC1,IC2;IC7
	BCLR	IFS0bits, #1
;ISR.c,340 :: 		IFS0bits.IC2IF = 0;
	BCLR	IFS0bits, #5
;ISR.c,341 :: 		IFS1bits.IC7IF = 0;
	BCLR	IFS1bits, #6
;ISR.c,342 :: 		IFS3bits.PWM1IF = 0;  // Clear interrupt flag
	BCLR	IFS3bits, #9
;ISR.c,344 :: 		IEC0bits.T1IE = 1;        // Enable interrupts for timer 1
	BSET	IEC0bits, #3
;ISR.c,345 :: 		IEC0bits.IC1IE = 1;       // Enable interrupt
	BSET	IEC0bits, #1
;ISR.c,346 :: 		IEC0bits.IC2IE = 1;
	BSET	IEC0bits, #5
;ISR.c,347 :: 		IEC1bits.IC7IE = 1;
	BSET	IEC1bits, #6
;ISR.c,348 :: 		IEC3bits.PWM1IE = 1;      // Enable PWM interrupts
	BSET	IEC3bits, #9
;ISR.c,349 :: 		DISICNT = 0;
	CLR	DISICNT
;ISR.c,350 :: 		Flags.MotorRunning = 1;   // Indicate that the motor is running
	MOV	#lo_addr(_Flags), W0
	BSET	[W0], #0
;ISR.c,351 :: 		}
L_end_RunMotor:
	RETURN
; end of _RunMotor

_StopMotor:

;ISR.c,353 :: 		void StopMotor(void)
;ISR.c,355 :: 		OVDCON = 0x0000;          // turn OFF every transistor
	CLR	OVDCON
;ISR.c,357 :: 		IEC0bits.T1IE = 0;        // Disable interrupts for timer 1
	BCLR	IEC0bits, #3
;ISR.c,358 :: 		IEC0bits.IC1IE = 0;       // Disable interrupts on IC1
	BCLR	IEC0bits, #1
;ISR.c,359 :: 		IEC0bits.IC2IE = 0;       // Disable interrupts on IC2
	BCLR	IEC0bits, #5
;ISR.c,360 :: 		IEC1bits.IC7IE = 0;       // Disable interrupts on IC7
	BCLR	IEC1bits, #6
;ISR.c,361 :: 		IEC3bits.PWM1IE = 0;      // Disable PWM interrupts
	BCLR	IEC3bits, #9
;ISR.c,362 :: 		DISICNT = 0;
	CLR	DISICNT
;ISR.c,363 :: 		Flags.MotorRunning = 0;// Indicate that the motor has been stopped
	MOV	#lo_addr(_Flags), W0
	BCLR	[W0], #0
;ISR.c,364 :: 		}
L_end_StopMotor:
	RETURN
; end of _StopMotor

_ChargeBootstraps:

;ISR.c,368 :: 		void ChargeBootstraps(void)
;ISR.c,371 :: 		OVDCON = 0x0015;       // Turn ON low side transistors to charge
	MOV	#21, W0
	MOV	WREG, OVDCON
;ISR.c,372 :: 		for (i = 0; i < 33330; i++);// 10 ms Delay at 20 MIPs
; i start address is: 2 (W1)
	CLR	W1
; i end address is: 2 (W1)
L_ChargeBootstraps41:
; i start address is: 2 (W1)
	MOV	#33330, W0
	CP	W1, W0
	BRA LTU	L__ChargeBootstraps97
	GOTO	L_ChargeBootstraps42
L__ChargeBootstraps97:
; i start address is: 2 (W1)
	INC	W1
; i end address is: 2 (W1)
; i end address is: 2 (W1)
	GOTO	L_ChargeBootstraps41
L_ChargeBootstraps42:
;ISR.c,373 :: 		PWMCON2bits.UDIS = 1;
	BSET	PWMCON2bits, #0
;ISR.c,374 :: 		PDC1 = PTPER;        // Initialize as 0 voltage
	MOV	PTPER, WREG
	MOV	WREG, PDC1
;ISR.c,375 :: 		PDC2 = PTPER;        // Initialize as 0 voltage
	MOV	PTPER, WREG
	MOV	WREG, PDC2
;ISR.c,376 :: 		PDC3 = PTPER;        // Initialize as 0 voltage
	MOV	PTPER, WREG
	MOV	WREG, PDC3
;ISR.c,377 :: 		OVDCON = 0x3F00;     // Configure PWM0-5 to be governed by PWM module
	MOV	#16128, W0
	MOV	WREG, OVDCON
;ISR.c,378 :: 		PWMCON2bits.UDIS = 0;
	BCLR	PWMCON2bits, #0
;ISR.c,379 :: 		}
L_end_ChargeBootstraps:
	RETURN
; end of _ChargeBootstraps

_Delay:

;ISR.c,381 :: 		void Delay(unsigned int delay_count)
;ISR.c,384 :: 		while (delay_count-- > 0)
L_Delay44:
	MOV	W10, W1
	SUB	W10, #1, W0
	MOV	W0, W10
	CP	W1, #0
	BRA GTU	L__Delay99
	GOTO	L_Delay45
L__Delay99:
;ISR.c,386 :: 		for (i = 0;i < 1000;i++);
; i start address is: 2 (W1)
	CLR	W1
; i end address is: 2 (W1)
L_Delay46:
; i start address is: 2 (W1)
	MOV	#1000, W0
	CP	W1, W0
	BRA LT	L__Delay100
	GOTO	L_Delay47
L__Delay100:
	ADD	W1, #1, W0
	MOV	W0, W1
; i end address is: 2 (W1)
	GOTO	L_Delay46
L_Delay47:
;ISR.c,387 :: 		}
	GOTO	L_Delay44
L_Delay45:
;ISR.c,388 :: 		}
L_end_Delay:
	RETURN
; end of _Delay

_SetPID:

;ISR.c,390 :: 		void SetPID(void)
;ISR.c,395 :: 		Kp = Q15_Ftoi(0.1);   // P Gain
	PUSH	W10
	PUSH	W11
	MOV	#52429, W10
	MOV	#15820, W11
	CALL	_Q15_Ftoi
	MOV	W0, _Kp
;ISR.c,396 :: 		Ki = Q15_Ftoi(0.01);  // I Gain
	MOV	#55050, W10
	MOV	#15395, W11
	CALL	_Q15_Ftoi
	MOV	W0, _Ki
;ISR.c,397 :: 		Kd = Q15_Ftoi(0.001); // D Gain
	MOV	#4719, W10
	MOV	#14979, W11
	CALL	_Q15_Ftoi
	MOV	W0, _Kd
;ISR.c,398 :: 		}
L_end_SetPID:
	POP	W11
	POP	W10
	RETURN
; end of _SetPID

_debug:

;ISR.c,400 :: 		void debug(void)
;ISR.c,402 :: 		Delay_ms(20);
	MOV	#3, W8
	MOV	#2261, W7
L_debug49:
	DEC	W7
	BRA NZ	L_debug49
	DEC	W8
	BRA NZ	L_debug49
;ISR.c,403 :: 		MotorSpeed = (signed int)(float_measuredspeed * 6000);
	MOV	_float_measuredspeed, W0
	MOV	_float_measuredspeed+2, W1
	MOV	#32768, W2
	MOV	#17851, W3
	CALL	__Mul_FP
	CALL	__Float2Longint
	MOV	W0, _MotorSpeed
;ISR.c,404 :: 		if(Current_Direction == CCW){MotorSpeed = -MotorSpeed ;}
	MOV	#lo_addr(_Current_Direction), W0
	MOV.B	[W0], W0
	CP.B	W0, #1
	BRA Z	L__debug103
	GOTO	L_debug51
L__debug103:
	MOV	_MotorSpeed, W1
	MOV	#lo_addr(_MotorSpeed), W0
	SUBR	W1, #0, [W0]
L_debug51:
;ISR.c,405 :: 		}
L_end_debug:
	RETURN
; end of _debug
