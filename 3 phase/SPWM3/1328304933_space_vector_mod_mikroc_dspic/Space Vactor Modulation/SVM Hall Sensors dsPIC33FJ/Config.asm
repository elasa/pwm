
_IOMap:

;Config.c,15 :: 		void IOMap()
;Config.c,17 :: 		NSTDIS_bit = 1;        //Block Nestet Interrupts follow Priority order
	PUSH	W10
	PUSH	W11
	PUSH	W12
	BSET	NSTDIS_bit, #15
;Config.c,18 :: 		AD1PCFGL = 0xFFFF;    // PORTB pins as Digital
	MOV	#65535, W0
	MOV	WREG, AD1PCFGL
;Config.c,22 :: 		Unlock_IOLOCK();
	CALL	_Unlock_IOLOCK
;Config.c,23 :: 		PPS_Mapping_NoLock(8,_INPUT ,_FLTA1);
	MOV.B	#11, W12
	MOV.B	#1, W11
	MOV.B	#8, W10
	CALL	_PPS_Mapping_NoLock
;Config.c,24 :: 		PPS_Mapping_NoLock(1,_INPUT ,_IC1); // IC1 RP1 RB1- RB3
	MOV.B	#6, W12
	MOV.B	#1, W11
	MOV.B	#1, W10
	CALL	_PPS_Mapping_NoLock
;Config.c,25 :: 		PPS_Mapping_NoLock(2,_INPUT ,_IC2); // IC2 RP2
	MOV.B	#7, W12
	MOV.B	#1, W11
	MOV.B	#2, W10
	CALL	_PPS_Mapping_NoLock
;Config.c,26 :: 		PPS_Mapping_NoLock(3,_INPUT ,_IC7); // IC7 RP3
	MOV.B	#8, W12
	MOV.B	#1, W11
	MOV.B	#3, W10
	CALL	_PPS_Mapping_NoLock
;Config.c,27 :: 		PPS_Mapping_NoLock(9,_OUTPUT,_C1TX);
	MOV.B	#16, W12
	CLR	W11
	MOV.B	#9, W10
	CALL	_PPS_Mapping_NoLock
;Config.c,28 :: 		PPS_Mapping_NoLock(7,_INPUT,_CIRX);
	MOV.B	#29, W12
	MOV.B	#1, W11
	MOV.B	#7, W10
	CALL	_PPS_Mapping_NoLock
;Config.c,29 :: 		Lock_IOLOCK();
	CALL	_Lock_IOLOCK
;Config.c,33 :: 		TRISBbits.TRISB15 = 0;        //PWM1L1
	BCLR	TRISBbits, #15
;Config.c,34 :: 		TRISBbits.TRISB14 = 0;        //PWM1H1
	BCLR	TRISBbits, #14
;Config.c,35 :: 		TRISBbits.TRISB13 = 0;        //PWM1L2
	BCLR	TRISBbits, #13
;Config.c,36 :: 		TRISBbits.TRISB12 = 0;        //PWM1H2
	BCLR	TRISBbits, #12
;Config.c,37 :: 		TRISBbits.TRISB11 = 0;        //PWM1L3
	BCLR	TRISBbits, #11
;Config.c,38 :: 		TRISBbits.TRISB10 = 0;        //PWM1H3
	BCLR	TRISBbits, #10
;Config.c,39 :: 		TRISBbits.TRISB1  = 1;
	BSET	TRISBbits, #1
;Config.c,40 :: 		TRISBbits.TRISB2  = 1;       // RB1-3 as inputs
	BSET	TRISBbits, #2
;Config.c,41 :: 		TRISBbits.TRISB3  = 1;
	BSET	TRISBbits, #3
;Config.c,42 :: 		TRISBbits.TRISB9  = 0;       //output for scope
	BCLR	TRISBbits, #9
;Config.c,43 :: 		TRISAbits.TRISA8  = 1;       // RA 8 S2 switch input pin
	BSET	TRISAbits, #8
;Config.c,44 :: 		}
L_end_IOMap:
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _IOMap

_InitADC:

;Config.c,48 :: 		void InitADC()
;Config.c,50 :: 		AD1PCFGL.PCFG8 = 0;   // AN8 analog
	BCLR	AD1PCFGL, #8
;Config.c,52 :: 		AD1CON1bits.FORM = 3;
	MOV	AD1CON1bits, W1
	MOV	#768, W0
	IOR	W1, W0, W0
	MOV	WREG, AD1CON1bits
;Config.c,54 :: 		AD1CON1bits.SSRC = 3;
	MOV.B	#96, W0
	MOV.B	W0, W1
	MOV	#lo_addr(AD1CON1bits), W0
	XOR.B	W1, [W0], W1
	MOV.B	#224, W0
	AND.B	W1, W0, W1
	MOV	#lo_addr(AD1CON1bits), W0
	XOR.B	W1, [W0], W1
	MOV	#lo_addr(AD1CON1bits), W0
	MOV.B	W1, [W0]
;Config.c,56 :: 		AD1CON1bits.ASAM = 1;
	BSET	AD1CON1bits, #2
;Config.c,58 :: 		AD1CON1bits.AD12B = 1;
	BSET	AD1CON1bits, #10
;Config.c,61 :: 		AD1CON2 = 0x0000;
	CLR	AD1CON2
;Config.c,63 :: 		AD1CON3 = 0x032F;
	MOV	#815, W0
	MOV	WREG, AD1CON3
;Config.c,65 :: 		AD1CSSL = 0x0000;
	CLR	AD1CSSL
;Config.c,67 :: 		AD1CHS0 = 0x0008;
	MOV	#8, W0
	MOV	WREG, AD1CHS0
;Config.c,69 :: 		IFS0bits.AD1IF = 0;
	BCLR	IFS0bits, #13
;Config.c,71 :: 		IEC0bits.AD1IE = 1;
	BSET	IEC0bits, #13
;Config.c,73 :: 		AD1CON1bits.ADON = 1;
	BSET	AD1CON1bits, #15
;Config.c,74 :: 		}
L_end_InitADC:
	RETURN
; end of _InitADC

_InitMCPWM:

;Config.c,78 :: 		void InitMCPWM(void)
;Config.c,80 :: 		PTPER = ((FCY/FPWM - 1) >> 1); // Compute Period based on CPU speed and
	MOV	#499, W0
	MOV	WREG, PTPER
;Config.c,82 :: 		OVDCON = 0x0000;     // Disable all PWM outputs.
	CLR	OVDCON
;Config.c,83 :: 		DTCON1 = 0x0028;     // 2 us of dead time
	MOV	#40, W0
	MOV	WREG, DTCON1
;Config.c,84 :: 		PWMCON1 = 0x0077;    // Enable PWM output pins and configure them as
	MOV	#119, W0
	MOV	WREG, PWMCON1
;Config.c,86 :: 		PDC1 = PTPER;        // Initialize as 0 voltage
	MOV	PTPER, WREG
	MOV	WREG, PDC1
;Config.c,87 :: 		PDC2 = PTPER;        // Initialize as 0 voltage
	MOV	PTPER, WREG
	MOV	WREG, PDC2
;Config.c,88 :: 		PDC3 = PTPER;        // Initialize as 0 voltage
	MOV	PTPER, WREG
	MOV	WREG, PDC3
;Config.c,89 :: 		SEVTCMP = 1;         // Enable triggering for ADC
	MOV	#1, W0
	MOV	WREG, SEVTCMP
;Config.c,90 :: 		PWMCON2 = 0x0F02;    // 16 postscale values, for achieving 20 kHz
	MOV	#3842, W0
	MOV	WREG, PWMCON2
;Config.c,91 :: 		PTCON = 0x8002;      // start PWM as center aligned mode
	MOV	#32770, W0
	MOV	WREG, PTCON
;Config.c,92 :: 		FLTACON = 0x0087;    // All PWM into inactive state, cycle-by-cycle,
	MOV	#135, W0
	MOV	WREG, FLTACON
;Config.c,93 :: 		}
L_end_InitMCPWM:
	RETURN
; end of _InitMCPWM

_InitIC:

;Config.c,97 :: 		void InitIC()
;Config.c,102 :: 		IC1CON = 1;
	MOV	#1, W0
	MOV	WREG, IC1CON
;Config.c,103 :: 		IC2CON = 1;
	MOV	#1, W0
	MOV	WREG, IC2CON
;Config.c,104 :: 		IC7CON = 1;
	MOV	#1, W0
	MOV	WREG, IC7CON
;Config.c,105 :: 		IFS0bits.IC1IF = 0;     // Clear interrupt flag  IC1,IC2;IC7
	BCLR	IFS0bits, #1
;Config.c,106 :: 		IFS0bits.IC2IF = 0;
	BCLR	IFS0bits, #5
;Config.c,107 :: 		IFS1bits.IC7IF = 0;
	BCLR	IFS1bits, #6
;Config.c,108 :: 		}
L_end_InitIC:
	RETURN
; end of _InitIC

_InitTMR1:

;Config.c,112 :: 		void InitTMR1(void)
;Config.c,114 :: 		T1CON = 0x0020;  // internal Tcy/64 clock
	MOV	#32, W0
	MOV	WREG, T1CON
;Config.c,115 :: 		TMR1 = 0;
	CLR	TMR1
;Config.c,116 :: 		PR1 = 313;       // 1 ms interrupts for 20 MIPS
	MOV	#313, W0
	MOV	WREG, PR1
;Config.c,117 :: 		T1CON.TON = 1;   // turn on timer 1
	BSET	T1CON, #15
;Config.c,118 :: 		}
L_end_InitTMR1:
	RETURN
; end of _InitTMR1

_InitTMR3:

;Config.c,122 :: 		void InitTMR3(void)
;Config.c,124 :: 		T3CON = 0x0020;  // internal Tcy/64 clock
	MOV	#32, W0
	MOV	WREG, T3CON
;Config.c,125 :: 		TMR3 = 0;
	CLR	TMR3
;Config.c,126 :: 		PR3 = 0xFFFF;
	MOV	#65535, W0
	MOV	WREG, PR3
;Config.c,127 :: 		T3CON.TON = 1;  // turn on timer 3
	BSET	T3CON, #15
;Config.c,128 :: 		}
L_end_InitTMR3:
	RETURN
; end of _InitTMR3
