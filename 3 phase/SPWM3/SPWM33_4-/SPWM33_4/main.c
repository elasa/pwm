//-----------------------------------------------------------------------------
// Copyright:      www.KnowledgePlus.ir
// Author:         OZHAN KD ported by Sh. Nourbakhsh Rad
// Remarks:        
// known Problems: none
// Version:        1.1.0
// Description:    SPWM test...
//-----------------------------------------------------------------------------

//	Include Headers
//*****************************************************************************
#include "app_config.h"
#include "HW_SPWM.h"

//--------------------------------------------------
#include "N11\N1100.h"
#include "sFONT\sFONT.h"

//--------------------------------------------------
#include "ADC\ADC.h"

//--------------------------------------------------
#include "SPWM\SPWM.h"
// Alphanumeric LCD functions
#include <lcd.h>
//	Constants and Varables
//*****************************************************************************

//Temp Const.
char 														Ctemp[24];
int  														Itemp = 0;


//	Functions Prototype
//*****************************************************************************
void Initial(void);
void LCD_0(void);

void Splash(void);

void test01(void);

//	<<< main function >>>
//*****************************************************************************
void main(void)
{
   LCD_0();
	Initial();
	test01();
	
	while(1);
} //main


//---------------------------------------------------------//
//---------------------------------------------------------//
void LCD_0(void)
{
    #asm
    //.equ __lcd_port=0x18; PORTD    
    .equ __lcd_port=0x12 ;PORTD
    #endasm     
    // Port D initialization
    // Func7=Out Func6=Out Func5=Out Func4=Out Func3=Out Func2=Out Func1=Out Func0=Out 
    // State7=0 State6=0 State5=0 State4=0 State3=0 State2=0 State1=0 State0=0 
    PORTD=0x00;
    DDRD=0xFF;
   // Alphanumeric LCD initialization
    // Connections are specified in the
    // Project|Configure|C Compiler|Libraries|Alphanumeric LCD menu:
    // RS - PORTD Bit 0
    // RD - PORTD Bit 1
    // EN - PORTD Bit 2
    // D4 - PORTD Bit 4
    // D5 - PORTD Bit 5
    // D6 - PORTD Bit 6
    // D7 - PORTD Bit 7
    // Characters/line: 12
    lcd_init(12);

    //////////////////////////////////////////////
   
    lcd_clear();
    lcd_putsf("salam");
    lcd_gotoxy(13,0);
    
    
    //_lcd_ready(); //waits until the LCD module is ready for receiving data 
    lcd_init(16);
    lcd_gotoxy(0,0);
    lcd_putsf("LCD Connection"); /* or any that you want*/

    lcd_gotoxy(0,1);
    lcd_putchar('A');
}
//---------------------------------------------------------//
void Initial(void)
{
	cli();												//Interrupts disable

	HW_init();
	a2dInit();										//AVCC & DIV64
	
	_delay_ms(500);

	//----------------------
	N11_Init();
	N11_Contrast(10);
	N11_Backlight(ON);
	
	//----------------------
	BUZZER(BOOT_sign);
	_delay_ms(500);

	//----------------------
	//static FILE mystdout = FDEV_SETUP_STREAM(N11_PrintChar, NULL, _FDEV_SETUP_WRITE);
  //stdout = &mystdout;
	
	N11_CLS();
	
	//----------------------
	Splash();
	BUZZER(OK_sign);

	//------------
	sei();												//Interrupts enabel	
}	//Initial

void Splash(void)
{
	static char *SPchar[5] = {
			"  SPWM test!!!",
			"by :",
			"Sh. Nourbakhsh",
			"Copyright:",
			"    OZHAN KD"
			};

	//-------------
	StringAt(0, 2, SPchar[0]);
	StringAt(2, 2, SPchar[1]);
	StringAt(3, 2, SPchar[2]);
	StringAt(5, 2, SPchar[3]);
	StringAt(6, 2, SPchar[4]);

	//-------------
	_delay_ms(3000);
	N11_CLS();
}	//Splash


//---------------------------------------------------------//
//---------------------------------------------------------//
void test01(void)
{
	unsigned int				FRQtemp;
	unsigned char				ACCtemp;
	unsigned char				DECtemp;
	
	unsigned char				LFlag = 0;

	SPWM_init();
	
	ADC_CH_init(FRQ_ACH);
	ADC_CH_init(ACC_ACH);
	ADC_CH_init(DEC_ACH);
	
	//----- main loop!
	while(1)
	{		
		FRQtemp = a2dConvert10bit(FRQ_ACH);
		ACCtemp = a2dConvert8bit(ACC_ACH);
		DECtemp = a2dConvert8bit(DEC_ACH);
		
		//-------------------------------
		FRQtemp = SetFrequency(FRQtemp);
		ACCtemp = SetAcceleration(ACCtemp);
		DECtemp = SetDeceleration(DECtemp);

		//-------------------------------
		sprintf(Ctemp, "FRQ: %03d.%01d Hz ", (FRQtemp/10), (FRQtemp%10));
		StringAt(1, 2, Ctemp);
		
		sprintf(Ctemp, "ACC: %03d sec ", ACCtemp);
		StringAt(3, 2, Ctemp);

		sprintf(Ctemp, "DEC: %03d sec ", DECtemp);
		StringAt(5, 2, Ctemp);

		//_delay_ms(200);
	}//while
} //test01
