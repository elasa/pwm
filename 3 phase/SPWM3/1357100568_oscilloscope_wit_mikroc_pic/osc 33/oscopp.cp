#line 1 "D:/micro/2012/Oscilloscope/osc 32/oscopp.c"
#line 23 "D:/micro/2012/Oscilloscope/osc 32/oscopp.c"
char GLCD_DataPort at PORTb;

sbit GLCD_CS1 at lata3_bit;
sbit GLCD_CS2 at lata4_bit;
sbit GLCD_RS at latc0_bit;
sbit GLCD_RW at latc1_bit;
sbit GLCD_EN at lata2_bit;
sbit GLCD_RST at lata5_bit;

sbit GLCD_CS1_Direction at TRISa3_bit;
sbit GLCD_CS2_Direction at TRISA4_bit;
sbit GLCD_RS_Direction at TRISc0_bit;
sbit GLCD_RW_Direction at TRISc1_bit;
sbit GLCD_EN_Direction at TRISa2_bit;
sbit GLCD_RST_Direction at TRISa5_bit;




 unsigned short adread[1400];
 unsigned int ff;
 unsigned int ffw;
 unsigned short mmax;
 unsigned short mmin;
 unsigned short ifdmmin;
 int mint1;
 int mint2;
 unsigned int freq1[2];
 unsigned int freq2[2];
 unsigned int freq3[2];
 unsigned int freqoencc;
 unsigned int maxduty[2];
 unsigned short ferbit ;
 unsigned int resadc ;
 unsigned short dutycyclee;


 unsigned int adcreading ;
 char otxt1[8];
 char otxt2[8];
 char otxt3[8];
 char otxt4[8];
 char otxt5[8];
 char otxt6[8];









void main() {

 trisa=255;
 trisa3_bit=0;
 trisa4_bit=0;
 trisa5_bit=0;
 trisb=0;
 trisc=0;
 trisd=0;

 PWM1_Init(5000);
 PWM1_Set_Duty(120);
 PWM1_Start();
 delay_ms(10);

 ADC_Init();
 delay_ms(10);

 ADCON0 = 0b10 ;
 ADCON1 = 0b1101 ;
 ADCON2 = 0b000000 ;
 ADCON0 = 0b11 ;

 delay_ms(100);
 Glcd_Init();
 delay_ms(200);
 Glcd_Fill(0x00);

 Glcd_Set_Font(System3x5 , 3, 5, 32);
 Glcd_Write_Text("3X5 FONT (V2)", 0, 0, 1);
 delay_ms(500);



 sttt:


 for ( ffw = 0 ; ffw < 1400 ; ffw++ ) {
 adread[ffw]=(ADC_Read(0) / 4);
 }

 mmin = adread[5];
 mmax = adread[5];
 for ( ffw = 0 ; ffw < 1400 ; ffw++ ) {
 mmin = min(mmin,adread[ffw]);
 mmax = max(mmax,adread[ffw]);
 }


 ferbit = 0 ;
 freq1[0] = 0 ;
 freq2[0] = 0 ;
 freq3[0] = 0 ;
 freq1[1] = 0 ;
 freq2[1] = 0 ;
 freq3[1] = 0 ;

 for ( ffw = 0 ; ffw < 1400 ; ffw++ ) {

 freq2[0] = ((mmax - mmin)/2) ;
 freq2[1] = ((adread[ffw] - mmin)) ;
 ifdmmin = 5;

 if (freq2[1] <= freq2[0]) {
 ifdmmin = 0 ;
 }

 if (ifdmmin == 0) {
 if (ferbit == 0) {
 ++freq3[0] ;
 freq1[0] = 1 ;
 }
 }

 if (ifdmmin > 0) {
 if (freq1[0] == 1) {
 ferbit = 1 ;
 }
 }

 if (ifdmmin == 0) {
 if (ferbit == 1) {
 ++freq3[1] ;
 freq1[0] = 2 ;
 }
 }
 if (ifdmmin > 0) {
 if (freq1[0] == 2) {
 ferbit = 3 ;
 }
 }
 }

 maxduty[0] = max(freq3[1],freq3[0]);

 ferbit = 0 ;
 freq1[0] = 0 ;
 freq2[0] = 0 ;
 freq3[0] = 0 ;
 freq1[1] = 0 ;
 freq2[1] = 0 ;
 freq3[1] = 0 ;

 for ( ffw = 0 ; ffw < 1400 ; ffw++ ) {

 freq2[0] = ((mmax - mmin)/2) ;
 freq2[1] = ((adread[ffw] - mmin)) ;
 ifdmmin = 5;

 if (freq2[1] >= freq2[0]) {
 ifdmmin = 0 ;
 }

 if (ifdmmin == 0) {
 if (ferbit == 0) {
 ++freq3[0] ;
 freq1[0] = 1 ;
 }
 }

 if (ifdmmin > 0) {
 if (freq1[0] == 1) {
 ferbit = 1 ;
 }
 }

 if (ifdmmin == 0) {
 if (ferbit == 1) {
 ++freq3[1] ;
 freq1[0] = 2 ;
 }
 }
 if (ifdmmin > 0) {
 if (freq1[0] == 2) {
 ferbit = 3 ;
 }
 }
 }

 maxduty[1] = max(freq3[1],freq3[0]);

 freq3[0] = maxduty[1] ;
 freq3[1] = maxduty[0] ;

 if (maxduty[1] > maxduty[0] ) {
 dutycyclee = ((freq3[1]+0.001)/(freq3[0]))*50 ;
 dutycyclee = 100 - dutycyclee ;
 }

 if (maxduty[1] < maxduty[0] ) {
 dutycyclee = ((freq3[0]+0.001)/(freq3[1]))*50 ;
 }

 if (maxduty[1] == maxduty[0] ) {
 dutycyclee = 50 ;
 }

 bytetostr(dutycyclee,otxt5);

 if (maxduty[1]<5) {
 strcpy(otxt2,"?");
 }

 if (maxduty[0]<5) {
 strcpy(otxt2,"?");
 }

 resadc = maxduty[1]+maxduty[0] ;
 freqoencc = ( 14000 ) / resadc ;

 if (resadc>1000) {
 resadc = 1000 ;
 }

 inttostr(freqoencc,otxt6);


 mint1 = ((mmin*10)/510) ;
 mint2 = ((mmin*10)%510)/10 ;
 inttostr(mint1,otxt1);
 inttostr(mint2,otxt2);

 mint1 = ((mmax*10)/510) ;
 mint2 = ((mmax*10)%510)/10 ;
 inttostr(mint1,otxt3);
 inttostr(mint2,otxt4);

 for ( ffw = 0 ; ffw < 129 ; ffw++ ) {
 adread[ffw]=(ADC_Read(0) / 16);
 }

 if (resadc > 40 ) {

 if (0<freqoencc) {
 if (freqoencc<100) {
 resadc = (resadc*2)+(resadc/10) ;
 }
 }
 if (freqoencc>99) {
 resadc = (resadc)+(resadc/10);
 }

 for ( ffw = 0 ; ffw < 129 ; ffw++ ) {
 adread[ffw]=(ADC_Read(0) / 16);
 for ( ff = 0 ; ff < resadc ; ff++ ) {


 }
 }
 }

 for ( ffw = 0 ; ffw < 200 ; ffw++ ) {
 adread[ffw]=63-(adread[ffw]);
 }

 Glcd_Fill(0x00);
 for ( ff = 15 ; ff < 128 ; ff++ ) {

 Glcd_Line(ff, adread[ff], ff+1 ,adread[ff+1], 1);
 }

 strcpy(otxt1,Rtrim(otxt1));
 strcpy(otxt1,ltrim(otxt1));
 strcpy(otxt2,Rtrim(otxt2));
 strcpy(otxt2,ltrim(otxt2));

 strcpy(otxt3,Rtrim(otxt3));
 strcpy(otxt3,ltrim(otxt3));
 strcpy(otxt4,Rtrim(otxt4));
 strcpy(otxt4,ltrim(otxt4));

 strcpy(otxt5,Rtrim(otxt5));
 strcpy(otxt5,ltrim(otxt5));
 strcpy(otxt6,Rtrim(otxt6));
 strcpy(otxt6,ltrim(otxt6));

 Glcd_Write_Text("OFFSET", 0, 0, 1);
 Glcd_Write_Text(otxt1, 0, 1, 1);
 Glcd_Write_Text(".", 7, 1, 1);
 Glcd_Write_Text(otxt2, 10, 1, 1);

 Glcd_Write_Text("PEAK", 0, 2, 1);
 Glcd_Write_Text(otxt3, 0, 3, 1);
 Glcd_Write_Text(".", 7, 3, 1);
 Glcd_Write_Text(otxt4, 10, 3, 1);

 Glcd_Write_Text("DUTY", 0, 4, 1);
 Glcd_Write_Text(otxt5, 0, 5, 1);

 Glcd_Write_Text("FREQ", 0, 6, 1);
 Glcd_Write_Text(otxt6, 0, 7, 1);


 delay_ms(1000);
 goto sttt ;




}
