
_main:

;oscopp.c,76 :: 		void main() {
;oscopp.c,78 :: 		trisa=255;
	MOVLW       255
	MOVWF       TRISA+0 
;oscopp.c,79 :: 		trisa3_bit=0;
	BCF         TRISA3_bit+0, BitPos(TRISA3_bit+0) 
;oscopp.c,80 :: 		trisa4_bit=0;
	BCF         TRISA4_bit+0, BitPos(TRISA4_bit+0) 
;oscopp.c,81 :: 		trisa5_bit=0;
	BCF         TRISA5_bit+0, BitPos(TRISA5_bit+0) 
;oscopp.c,82 :: 		trisb=0;
	CLRF        TRISB+0 
;oscopp.c,83 :: 		trisc=0;
	CLRF        TRISC+0 
;oscopp.c,84 :: 		trisd=0;
	CLRF        TRISD+0 
;oscopp.c,86 :: 		PWM1_Init(5000); // '5000Hz
	BSF         T2CON+0, 0, 0
	BSF         T2CON+0, 1, 0
	MOVLW       149
	MOVWF       PR2+0, 0
	CALL        _PWM1_Init+0, 0
;oscopp.c,87 :: 		PWM1_Set_Duty(120); //' from 0 to 255, where 0 is 0%, 127 is 50%, and 255 is 100% duty ratio
	MOVLW       120
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
;oscopp.c,88 :: 		PWM1_Start();
	CALL        _PWM1_Start+0, 0
;oscopp.c,89 :: 		delay_ms(10);
	MOVLW       156
	MOVWF       R12, 0
	MOVLW       215
	MOVWF       R13, 0
L_main0:
	DECFSZ      R13, 1, 1
	BRA         L_main0
	DECFSZ      R12, 1, 1
	BRA         L_main0
;oscopp.c,91 :: 		ADC_Init();
	CALL        _ADC_Init+0, 0
;oscopp.c,92 :: 		delay_ms(10);
	MOVLW       156
	MOVWF       R12, 0
	MOVLW       215
	MOVWF       R13, 0
L_main1:
	DECFSZ      R13, 1, 1
	BRA         L_main1
	DECFSZ      R12, 1, 1
	BRA         L_main1
;oscopp.c,94 :: 		ADCON0 = 0b10 ;
	MOVLW       2
	MOVWF       ADCON0+0 
;oscopp.c,95 :: 		ADCON1 = 0b1101 ;
	MOVLW       13
	MOVWF       ADCON1+0 
;oscopp.c,96 :: 		ADCON2 = 0b000000 ;  //  TAD - FOSC/2
	CLRF        ADCON2+0 
;oscopp.c,97 :: 		ADCON0 = 0b11 ;
	MOVLW       3
	MOVWF       ADCON0+0 
;oscopp.c,99 :: 		delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_main2:
	DECFSZ      R13, 1, 1
	BRA         L_main2
	DECFSZ      R12, 1, 1
	BRA         L_main2
	DECFSZ      R11, 1, 1
	BRA         L_main2
	NOP
;oscopp.c,100 :: 		Glcd_Init();
	CALL        _Glcd_Init+0, 0
;oscopp.c,101 :: 		delay_ms(200);
	MOVLW       13
	MOVWF       R11, 0
	MOVLW       45
	MOVWF       R12, 0
	MOVLW       215
	MOVWF       R13, 0
L_main3:
	DECFSZ      R13, 1, 1
	BRA         L_main3
	DECFSZ      R12, 1, 1
	BRA         L_main3
	DECFSZ      R11, 1, 1
	BRA         L_main3
	NOP
	NOP
;oscopp.c,102 :: 		Glcd_Fill(0x00);
	CLRF        FARG_Glcd_Fill_pattern+0 
	CALL        _Glcd_Fill+0, 0
;oscopp.c,104 :: 		Glcd_Set_Font(System3x5 , 3, 5, 32);
	MOVLW       _System3x5+0
	MOVWF       FARG_Glcd_Set_Font_activeFont+0 
	MOVLW       hi_addr(_System3x5+0)
	MOVWF       FARG_Glcd_Set_Font_activeFont+1 
	MOVLW       higher_addr(_System3x5+0)
	MOVWF       FARG_Glcd_Set_Font_activeFont+2 
	MOVLW       3
	MOVWF       FARG_Glcd_Set_Font_aFontWidth+0 
	MOVLW       5
	MOVWF       FARG_Glcd_Set_Font_aFontHeight+0 
	MOVLW       32
	MOVWF       FARG_Glcd_Set_Font_aFontOffs+0 
	MOVLW       0
	MOVWF       FARG_Glcd_Set_Font_aFontOffs+1 
	CALL        _Glcd_Set_Font+0, 0
;oscopp.c,105 :: 		Glcd_Write_Text("3X5 FONT (V2)", 0, 0, 1);
	MOVLW       51
	MOVWF       ?lstr1_oscopp+0 
	MOVLW       88
	MOVWF       ?lstr1_oscopp+1 
	MOVLW       53
	MOVWF       ?lstr1_oscopp+2 
	MOVLW       32
	MOVWF       ?lstr1_oscopp+3 
	MOVLW       70
	MOVWF       ?lstr1_oscopp+4 
	MOVLW       79
	MOVWF       ?lstr1_oscopp+5 
	MOVLW       78
	MOVWF       ?lstr1_oscopp+6 
	MOVLW       84
	MOVWF       ?lstr1_oscopp+7 
	MOVLW       32
	MOVWF       ?lstr1_oscopp+8 
	MOVLW       40
	MOVWF       ?lstr1_oscopp+9 
	MOVLW       86
	MOVWF       ?lstr1_oscopp+10 
	MOVLW       50
	MOVWF       ?lstr1_oscopp+11 
	MOVLW       41
	MOVWF       ?lstr1_oscopp+12 
	CLRF        ?lstr1_oscopp+13 
	MOVLW       ?lstr1_oscopp+0
	MOVWF       FARG_Glcd_Write_Text_text+0 
	MOVLW       hi_addr(?lstr1_oscopp+0)
	MOVWF       FARG_Glcd_Write_Text_text+1 
	CLRF        FARG_Glcd_Write_Text_x_pos+0 
	CLRF        FARG_Glcd_Write_Text_page_num+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_color+0 
	CALL        _Glcd_Write_Text+0, 0
;oscopp.c,106 :: 		delay_ms(500);
	MOVLW       31
	MOVWF       R11, 0
	MOVLW       113
	MOVWF       R12, 0
	MOVLW       30
	MOVWF       R13, 0
L_main4:
	DECFSZ      R13, 1, 1
	BRA         L_main4
	DECFSZ      R12, 1, 1
	BRA         L_main4
	DECFSZ      R11, 1, 1
	BRA         L_main4
	NOP
;oscopp.c,110 :: 		sttt:
___main_sttt:
;oscopp.c,113 :: 		for ( ffw = 0 ; ffw < 1400 ; ffw++ ) {
	CLRF        _ffw+0 
	CLRF        _ffw+1 
L_main5:
	MOVLW       5
	SUBWF       _ffw+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main62
	MOVLW       120
	SUBWF       _ffw+0, 0 
L__main62:
	BTFSC       STATUS+0, 0 
	GOTO        L_main6
;oscopp.c,114 :: 		adread[ffw]=(ADC_Read(0) / 4);
	MOVLW       _adread+0
	ADDWF       _ffw+0, 0 
	MOVWF       FLOC__main+0 
	MOVLW       hi_addr(_adread+0)
	ADDWFC      _ffw+1, 0 
	MOVWF       FLOC__main+1 
	CLRF        FARG_ADC_Read_channel+0 
	CALL        _ADC_Read+0, 0
	MOVF        R0, 0 
	MOVWF       R2 
	MOVF        R1, 0 
	MOVWF       R3 
	RRCF        R3, 1 
	RRCF        R2, 1 
	BCF         R3, 7 
	RRCF        R3, 1 
	RRCF        R2, 1 
	BCF         R3, 7 
	MOVFF       FLOC__main+0, FSR1
	MOVFF       FLOC__main+1, FSR1H
	MOVF        R2, 0 
	MOVWF       POSTINC1+0 
;oscopp.c,113 :: 		for ( ffw = 0 ; ffw < 1400 ; ffw++ ) {
	INFSNZ      _ffw+0, 1 
	INCF        _ffw+1, 1 
;oscopp.c,115 :: 		}
	GOTO        L_main5
L_main6:
;oscopp.c,117 :: 		mmin = adread[5];
	MOVF        _adread+5, 0 
	MOVWF       _mmin+0 
;oscopp.c,118 :: 		mmax = adread[5];
	MOVF        _adread+5, 0 
	MOVWF       _mmax+0 
;oscopp.c,119 :: 		for ( ffw = 0 ; ffw < 1400 ; ffw++ ) {
	CLRF        _ffw+0 
	CLRF        _ffw+1 
L_main8:
	MOVLW       5
	SUBWF       _ffw+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main63
	MOVLW       120
	SUBWF       _ffw+0, 0 
L__main63:
	BTFSC       STATUS+0, 0 
	GOTO        L_main9
;oscopp.c,120 :: 		mmin = min(mmin,adread[ffw]);
	MOVF        _mmin+0, 0 
	MOVWF       FARG_min_a+0 
	MOVLW       0
	MOVWF       FARG_min_a+1 
	MOVLW       _adread+0
	ADDWF       _ffw+0, 0 
	MOVWF       FSR0 
	MOVLW       hi_addr(_adread+0)
	ADDWFC      _ffw+1, 0 
	MOVWF       FSR0H 
	MOVF        POSTINC0+0, 0 
	MOVWF       FARG_min_b+0 
	MOVLW       0
	MOVWF       FARG_min_b+1 
	MOVLW       0
	MOVWF       FARG_min_b+1 
	CALL        _min+0, 0
	MOVF        R0, 0 
	MOVWF       _mmin+0 
;oscopp.c,121 :: 		mmax = max(mmax,adread[ffw]);
	MOVF        _mmax+0, 0 
	MOVWF       FARG_max_a+0 
	MOVLW       0
	MOVWF       FARG_max_a+1 
	MOVLW       _adread+0
	ADDWF       _ffw+0, 0 
	MOVWF       FSR0 
	MOVLW       hi_addr(_adread+0)
	ADDWFC      _ffw+1, 0 
	MOVWF       FSR0H 
	MOVF        POSTINC0+0, 0 
	MOVWF       FARG_max_b+0 
	MOVLW       0
	MOVWF       FARG_max_b+1 
	MOVLW       0
	MOVWF       FARG_max_b+1 
	CALL        _max+0, 0
	MOVF        R0, 0 
	MOVWF       _mmax+0 
;oscopp.c,119 :: 		for ( ffw = 0 ; ffw < 1400 ; ffw++ ) {
	INFSNZ      _ffw+0, 1 
	INCF        _ffw+1, 1 
;oscopp.c,122 :: 		}
	GOTO        L_main8
L_main9:
;oscopp.c,125 :: 		ferbit = 0 ;
	CLRF        _ferbit+0 
;oscopp.c,126 :: 		freq1[0]  = 0 ;
	CLRF        _freq1+0 
	CLRF        _freq1+1 
;oscopp.c,127 :: 		freq2[0]  = 0 ;
	CLRF        _freq2+0 
	CLRF        _freq2+1 
;oscopp.c,128 :: 		freq3[0]  = 0 ;
	CLRF        _freq3+0 
	CLRF        _freq3+1 
;oscopp.c,129 :: 		freq1[1]  = 0 ;
	CLRF        _freq1+2 
	CLRF        _freq1+3 
;oscopp.c,130 :: 		freq2[1]  = 0 ;
	CLRF        _freq2+2 
	CLRF        _freq2+3 
;oscopp.c,131 :: 		freq3[1]  = 0 ;
	CLRF        _freq3+2 
	CLRF        _freq3+3 
;oscopp.c,133 :: 		for ( ffw = 0 ; ffw < 1400 ; ffw++ ) {
	CLRF        _ffw+0 
	CLRF        _ffw+1 
L_main11:
	MOVLW       5
	SUBWF       _ffw+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main64
	MOVLW       120
	SUBWF       _ffw+0, 0 
L__main64:
	BTFSC       STATUS+0, 0 
	GOTO        L_main12
;oscopp.c,135 :: 		freq2[0]  = ((mmax - mmin)/2) ;
	MOVF        _mmin+0, 0 
	SUBWF       _mmax+0, 0 
	MOVWF       _freq2+0 
	CLRF        _freq2+1 
	MOVLW       0
	SUBWFB      _freq2+1, 1 
	RRCF        _freq2+1, 1 
	RRCF        _freq2+0, 1 
	BCF         _freq2+1, 7 
	BTFSC       _freq2+1, 6 
	BSF         _freq2+1, 7 
;oscopp.c,136 :: 		freq2[1] = ((adread[ffw] - mmin)) ;
	MOVLW       _adread+0
	ADDWF       _ffw+0, 0 
	MOVWF       FSR0 
	MOVLW       hi_addr(_adread+0)
	ADDWFC      _ffw+1, 0 
	MOVWF       FSR0H 
	MOVF        _mmin+0, 0 
	SUBWF       POSTINC0+0, 0 
	MOVWF       R1 
	CLRF        R2 
	MOVLW       0
	SUBWFB      R2, 1 
	MOVF        R1, 0 
	MOVWF       _freq2+2 
	MOVF        R2, 0 
	MOVWF       _freq2+3 
;oscopp.c,137 :: 		ifdmmin = 5;
	MOVLW       5
	MOVWF       _ifdmmin+0 
;oscopp.c,139 :: 		if (freq2[1] <= freq2[0]) {
	MOVF        R2, 0 
	SUBWF       _freq2+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main65
	MOVF        R1, 0 
	SUBWF       _freq2+0, 0 
L__main65:
	BTFSS       STATUS+0, 0 
	GOTO        L_main14
;oscopp.c,140 :: 		ifdmmin = 0 ;
	CLRF        _ifdmmin+0 
;oscopp.c,141 :: 		}
L_main14:
;oscopp.c,143 :: 		if (ifdmmin == 0) {
	MOVF        _ifdmmin+0, 0 
	XORLW       0
	BTFSS       STATUS+0, 2 
	GOTO        L_main15
;oscopp.c,144 :: 		if (ferbit == 0) {
	MOVF        _ferbit+0, 0 
	XORLW       0
	BTFSS       STATUS+0, 2 
	GOTO        L_main16
;oscopp.c,145 :: 		++freq3[0] ;
	INFSNZ      _freq3+0, 1 
	INCF        _freq3+1, 1 
;oscopp.c,146 :: 		freq1[0] = 1 ;
	MOVLW       1
	MOVWF       _freq1+0 
	MOVLW       0
	MOVWF       _freq1+1 
;oscopp.c,147 :: 		}
L_main16:
;oscopp.c,148 :: 		}
L_main15:
;oscopp.c,150 :: 		if (ifdmmin > 0) {
	MOVF        _ifdmmin+0, 0 
	SUBLW       0
	BTFSC       STATUS+0, 0 
	GOTO        L_main17
;oscopp.c,151 :: 		if (freq1[0] == 1) {
	MOVLW       0
	XORWF       _freq1+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main66
	MOVLW       1
	XORWF       _freq1+0, 0 
L__main66:
	BTFSS       STATUS+0, 2 
	GOTO        L_main18
;oscopp.c,152 :: 		ferbit = 1 ;
	MOVLW       1
	MOVWF       _ferbit+0 
;oscopp.c,153 :: 		}
L_main18:
;oscopp.c,154 :: 		}
L_main17:
;oscopp.c,156 :: 		if (ifdmmin == 0) {
	MOVF        _ifdmmin+0, 0 
	XORLW       0
	BTFSS       STATUS+0, 2 
	GOTO        L_main19
;oscopp.c,157 :: 		if (ferbit == 1) {
	MOVF        _ferbit+0, 0 
	XORLW       1
	BTFSS       STATUS+0, 2 
	GOTO        L_main20
;oscopp.c,158 :: 		++freq3[1] ;
	INFSNZ      _freq3+2, 1 
	INCF        _freq3+3, 1 
;oscopp.c,159 :: 		freq1[0] = 2 ;
	MOVLW       2
	MOVWF       _freq1+0 
	MOVLW       0
	MOVWF       _freq1+1 
;oscopp.c,160 :: 		}
L_main20:
;oscopp.c,161 :: 		}
L_main19:
;oscopp.c,162 :: 		if (ifdmmin > 0) {
	MOVF        _ifdmmin+0, 0 
	SUBLW       0
	BTFSC       STATUS+0, 0 
	GOTO        L_main21
;oscopp.c,163 :: 		if (freq1[0] == 2) {
	MOVLW       0
	XORWF       _freq1+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main67
	MOVLW       2
	XORWF       _freq1+0, 0 
L__main67:
	BTFSS       STATUS+0, 2 
	GOTO        L_main22
;oscopp.c,164 :: 		ferbit = 3 ;
	MOVLW       3
	MOVWF       _ferbit+0 
;oscopp.c,165 :: 		}
L_main22:
;oscopp.c,166 :: 		}
L_main21:
;oscopp.c,133 :: 		for ( ffw = 0 ; ffw < 1400 ; ffw++ ) {
	INFSNZ      _ffw+0, 1 
	INCF        _ffw+1, 1 
;oscopp.c,167 :: 		}
	GOTO        L_main11
L_main12:
;oscopp.c,169 :: 		maxduty[0] = max(freq3[1],freq3[0]);
	MOVF        _freq3+2, 0 
	MOVWF       FARG_max_a+0 
	MOVF        _freq3+3, 0 
	MOVWF       FARG_max_a+1 
	MOVF        _freq3+0, 0 
	MOVWF       FARG_max_b+0 
	MOVF        _freq3+1, 0 
	MOVWF       FARG_max_b+1 
	CALL        _max+0, 0
	MOVF        R0, 0 
	MOVWF       _maxduty+0 
	MOVF        R1, 0 
	MOVWF       _maxduty+1 
;oscopp.c,171 :: 		ferbit = 0 ;
	CLRF        _ferbit+0 
;oscopp.c,172 :: 		freq1[0]  = 0 ;
	CLRF        _freq1+0 
	CLRF        _freq1+1 
;oscopp.c,173 :: 		freq2[0]  = 0 ;
	CLRF        _freq2+0 
	CLRF        _freq2+1 
;oscopp.c,174 :: 		freq3[0]  = 0 ;
	CLRF        _freq3+0 
	CLRF        _freq3+1 
;oscopp.c,175 :: 		freq1[1]  = 0 ;
	CLRF        _freq1+2 
	CLRF        _freq1+3 
;oscopp.c,176 :: 		freq2[1]  = 0 ;
	CLRF        _freq2+2 
	CLRF        _freq2+3 
;oscopp.c,177 :: 		freq3[1]  = 0 ;
	CLRF        _freq3+2 
	CLRF        _freq3+3 
;oscopp.c,179 :: 		for ( ffw = 0 ; ffw < 1400 ; ffw++ ) {
	CLRF        _ffw+0 
	CLRF        _ffw+1 
L_main23:
	MOVLW       5
	SUBWF       _ffw+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main68
	MOVLW       120
	SUBWF       _ffw+0, 0 
L__main68:
	BTFSC       STATUS+0, 0 
	GOTO        L_main24
;oscopp.c,181 :: 		freq2[0]  = ((mmax - mmin)/2) ;
	MOVF        _mmin+0, 0 
	SUBWF       _mmax+0, 0 
	MOVWF       _freq2+0 
	CLRF        _freq2+1 
	MOVLW       0
	SUBWFB      _freq2+1, 1 
	RRCF        _freq2+1, 1 
	RRCF        _freq2+0, 1 
	BCF         _freq2+1, 7 
	BTFSC       _freq2+1, 6 
	BSF         _freq2+1, 7 
;oscopp.c,182 :: 		freq2[1] = ((adread[ffw] - mmin)) ;
	MOVLW       _adread+0
	ADDWF       _ffw+0, 0 
	MOVWF       FSR0 
	MOVLW       hi_addr(_adread+0)
	ADDWFC      _ffw+1, 0 
	MOVWF       FSR0H 
	MOVF        _mmin+0, 0 
	SUBWF       POSTINC0+0, 0 
	MOVWF       R1 
	CLRF        R2 
	MOVLW       0
	SUBWFB      R2, 1 
	MOVF        R1, 0 
	MOVWF       _freq2+2 
	MOVF        R2, 0 
	MOVWF       _freq2+3 
;oscopp.c,183 :: 		ifdmmin = 5;
	MOVLW       5
	MOVWF       _ifdmmin+0 
;oscopp.c,185 :: 		if (freq2[1] >= freq2[0]) {
	MOVF        _freq2+1, 0 
	SUBWF       R2, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main69
	MOVF        _freq2+0, 0 
	SUBWF       R1, 0 
L__main69:
	BTFSS       STATUS+0, 0 
	GOTO        L_main26
;oscopp.c,186 :: 		ifdmmin = 0 ;
	CLRF        _ifdmmin+0 
;oscopp.c,187 :: 		}
L_main26:
;oscopp.c,189 :: 		if (ifdmmin == 0) {
	MOVF        _ifdmmin+0, 0 
	XORLW       0
	BTFSS       STATUS+0, 2 
	GOTO        L_main27
;oscopp.c,190 :: 		if (ferbit == 0) {
	MOVF        _ferbit+0, 0 
	XORLW       0
	BTFSS       STATUS+0, 2 
	GOTO        L_main28
;oscopp.c,191 :: 		++freq3[0] ;
	INFSNZ      _freq3+0, 1 
	INCF        _freq3+1, 1 
;oscopp.c,192 :: 		freq1[0] = 1 ;
	MOVLW       1
	MOVWF       _freq1+0 
	MOVLW       0
	MOVWF       _freq1+1 
;oscopp.c,193 :: 		}
L_main28:
;oscopp.c,194 :: 		}
L_main27:
;oscopp.c,196 :: 		if (ifdmmin > 0) {
	MOVF        _ifdmmin+0, 0 
	SUBLW       0
	BTFSC       STATUS+0, 0 
	GOTO        L_main29
;oscopp.c,197 :: 		if (freq1[0] == 1) {
	MOVLW       0
	XORWF       _freq1+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main70
	MOVLW       1
	XORWF       _freq1+0, 0 
L__main70:
	BTFSS       STATUS+0, 2 
	GOTO        L_main30
;oscopp.c,198 :: 		ferbit = 1 ;
	MOVLW       1
	MOVWF       _ferbit+0 
;oscopp.c,199 :: 		}
L_main30:
;oscopp.c,200 :: 		}
L_main29:
;oscopp.c,202 :: 		if (ifdmmin == 0) {
	MOVF        _ifdmmin+0, 0 
	XORLW       0
	BTFSS       STATUS+0, 2 
	GOTO        L_main31
;oscopp.c,203 :: 		if (ferbit == 1) {
	MOVF        _ferbit+0, 0 
	XORLW       1
	BTFSS       STATUS+0, 2 
	GOTO        L_main32
;oscopp.c,204 :: 		++freq3[1] ;
	INFSNZ      _freq3+2, 1 
	INCF        _freq3+3, 1 
;oscopp.c,205 :: 		freq1[0] = 2 ;
	MOVLW       2
	MOVWF       _freq1+0 
	MOVLW       0
	MOVWF       _freq1+1 
;oscopp.c,206 :: 		}
L_main32:
;oscopp.c,207 :: 		}
L_main31:
;oscopp.c,208 :: 		if (ifdmmin > 0) {
	MOVF        _ifdmmin+0, 0 
	SUBLW       0
	BTFSC       STATUS+0, 0 
	GOTO        L_main33
;oscopp.c,209 :: 		if (freq1[0] == 2) {
	MOVLW       0
	XORWF       _freq1+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main71
	MOVLW       2
	XORWF       _freq1+0, 0 
L__main71:
	BTFSS       STATUS+0, 2 
	GOTO        L_main34
;oscopp.c,210 :: 		ferbit = 3 ;
	MOVLW       3
	MOVWF       _ferbit+0 
;oscopp.c,211 :: 		}
L_main34:
;oscopp.c,212 :: 		}
L_main33:
;oscopp.c,179 :: 		for ( ffw = 0 ; ffw < 1400 ; ffw++ ) {
	INFSNZ      _ffw+0, 1 
	INCF        _ffw+1, 1 
;oscopp.c,213 :: 		}
	GOTO        L_main23
L_main24:
;oscopp.c,215 :: 		maxduty[1] = max(freq3[1],freq3[0]);
	MOVF        _freq3+2, 0 
	MOVWF       FARG_max_a+0 
	MOVF        _freq3+3, 0 
	MOVWF       FARG_max_a+1 
	MOVF        _freq3+0, 0 
	MOVWF       FARG_max_b+0 
	MOVF        _freq3+1, 0 
	MOVWF       FARG_max_b+1 
	CALL        _max+0, 0
	MOVF        R0, 0 
	MOVWF       _maxduty+2 
	MOVF        R1, 0 
	MOVWF       _maxduty+3 
;oscopp.c,217 :: 		freq3[0] = maxduty[1] ;
	MOVF        R0, 0 
	MOVWF       _freq3+0 
	MOVF        R1, 0 
	MOVWF       _freq3+1 
;oscopp.c,218 :: 		freq3[1] = maxduty[0] ;
	MOVF        _maxduty+0, 0 
	MOVWF       _freq3+2 
	MOVF        _maxduty+1, 0 
	MOVWF       _freq3+3 
;oscopp.c,220 :: 		if (maxduty[1] > maxduty[0] ) {
	MOVF        R1, 0 
	SUBWF       _maxduty+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main72
	MOVF        R0, 0 
	SUBWF       _maxduty+0, 0 
L__main72:
	BTFSC       STATUS+0, 0 
	GOTO        L_main35
;oscopp.c,221 :: 		dutycyclee = ((freq3[1]+0.001)/(freq3[0]))*50 ;
	MOVF        _freq3+2, 0 
	MOVWF       R0 
	MOVF        _freq3+3, 0 
	MOVWF       R1 
	CALL        _Word2Double+0, 0
	MOVLW       111
	MOVWF       R4 
	MOVLW       18
	MOVWF       R5 
	MOVLW       3
	MOVWF       R6 
	MOVLW       117
	MOVWF       R7 
	CALL        _Add_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       FLOC__main+0 
	MOVF        R1, 0 
	MOVWF       FLOC__main+1 
	MOVF        R2, 0 
	MOVWF       FLOC__main+2 
	MOVF        R3, 0 
	MOVWF       FLOC__main+3 
	MOVF        _freq3+0, 0 
	MOVWF       R0 
	MOVF        _freq3+1, 0 
	MOVWF       R1 
	CALL        _Word2Double+0, 0
	MOVF        R0, 0 
	MOVWF       R4 
	MOVF        R1, 0 
	MOVWF       R5 
	MOVF        R2, 0 
	MOVWF       R6 
	MOVF        R3, 0 
	MOVWF       R7 
	MOVF        FLOC__main+0, 0 
	MOVWF       R0 
	MOVF        FLOC__main+1, 0 
	MOVWF       R1 
	MOVF        FLOC__main+2, 0 
	MOVWF       R2 
	MOVF        FLOC__main+3, 0 
	MOVWF       R3 
	CALL        _Div_32x32_FP+0, 0
	MOVLW       0
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVLW       72
	MOVWF       R6 
	MOVLW       132
	MOVWF       R7 
	CALL        _Mul_32x32_FP+0, 0
	CALL        _Double2Byte+0, 0
	MOVF        R0, 0 
	MOVWF       _dutycyclee+0 
;oscopp.c,222 :: 		dutycyclee = 100 - dutycyclee ;
	MOVF        R0, 0 
	SUBLW       100
	MOVWF       _dutycyclee+0 
;oscopp.c,223 :: 		}
L_main35:
;oscopp.c,225 :: 		if (maxduty[1] < maxduty[0] ) {
	MOVF        _maxduty+1, 0 
	SUBWF       _maxduty+3, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main73
	MOVF        _maxduty+0, 0 
	SUBWF       _maxduty+2, 0 
L__main73:
	BTFSC       STATUS+0, 0 
	GOTO        L_main36
;oscopp.c,226 :: 		dutycyclee = ((freq3[0]+0.001)/(freq3[1]))*50 ;
	MOVF        _freq3+0, 0 
	MOVWF       R0 
	MOVF        _freq3+1, 0 
	MOVWF       R1 
	CALL        _Word2Double+0, 0
	MOVLW       111
	MOVWF       R4 
	MOVLW       18
	MOVWF       R5 
	MOVLW       3
	MOVWF       R6 
	MOVLW       117
	MOVWF       R7 
	CALL        _Add_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       FLOC__main+0 
	MOVF        R1, 0 
	MOVWF       FLOC__main+1 
	MOVF        R2, 0 
	MOVWF       FLOC__main+2 
	MOVF        R3, 0 
	MOVWF       FLOC__main+3 
	MOVF        _freq3+2, 0 
	MOVWF       R0 
	MOVF        _freq3+3, 0 
	MOVWF       R1 
	CALL        _Word2Double+0, 0
	MOVF        R0, 0 
	MOVWF       R4 
	MOVF        R1, 0 
	MOVWF       R5 
	MOVF        R2, 0 
	MOVWF       R6 
	MOVF        R3, 0 
	MOVWF       R7 
	MOVF        FLOC__main+0, 0 
	MOVWF       R0 
	MOVF        FLOC__main+1, 0 
	MOVWF       R1 
	MOVF        FLOC__main+2, 0 
	MOVWF       R2 
	MOVF        FLOC__main+3, 0 
	MOVWF       R3 
	CALL        _Div_32x32_FP+0, 0
	MOVLW       0
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVLW       72
	MOVWF       R6 
	MOVLW       132
	MOVWF       R7 
	CALL        _Mul_32x32_FP+0, 0
	CALL        _Double2Byte+0, 0
	MOVF        R0, 0 
	MOVWF       _dutycyclee+0 
;oscopp.c,227 :: 		}
L_main36:
;oscopp.c,229 :: 		if (maxduty[1] == maxduty[0] ) {
	MOVF        _maxduty+3, 0 
	XORWF       _maxduty+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main74
	MOVF        _maxduty+0, 0 
	XORWF       _maxduty+2, 0 
L__main74:
	BTFSS       STATUS+0, 2 
	GOTO        L_main37
;oscopp.c,230 :: 		dutycyclee = 50 ;
	MOVLW       50
	MOVWF       _dutycyclee+0 
;oscopp.c,231 :: 		}
L_main37:
;oscopp.c,233 :: 		bytetostr(dutycyclee,otxt5);
	MOVF        _dutycyclee+0, 0 
	MOVWF       FARG_ByteToStr_input+0 
	MOVLW       _otxt5+0
	MOVWF       FARG_ByteToStr_output+0 
	MOVLW       hi_addr(_otxt5+0)
	MOVWF       FARG_ByteToStr_output+1 
	CALL        _ByteToStr+0, 0
;oscopp.c,235 :: 		if (maxduty[1]<5) {
	MOVLW       0
	SUBWF       _maxduty+3, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main75
	MOVLW       5
	SUBWF       _maxduty+2, 0 
L__main75:
	BTFSC       STATUS+0, 0 
	GOTO        L_main38
;oscopp.c,236 :: 		strcpy(otxt2,"?");
	MOVLW       _otxt2+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt2+0)
	MOVWF       FARG_strcpy_to+1 
	MOVLW       63
	MOVWF       ?lstr2_oscopp+0 
	CLRF        ?lstr2_oscopp+1 
	MOVLW       ?lstr2_oscopp+0
	MOVWF       FARG_strcpy_from+0 
	MOVLW       hi_addr(?lstr2_oscopp+0)
	MOVWF       FARG_strcpy_from+1 
	CALL        _strcpy+0, 0
;oscopp.c,237 :: 		}
L_main38:
;oscopp.c,239 :: 		if (maxduty[0]<5) {
	MOVLW       0
	SUBWF       _maxduty+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main76
	MOVLW       5
	SUBWF       _maxduty+0, 0 
L__main76:
	BTFSC       STATUS+0, 0 
	GOTO        L_main39
;oscopp.c,240 :: 		strcpy(otxt2,"?");
	MOVLW       _otxt2+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt2+0)
	MOVWF       FARG_strcpy_to+1 
	MOVLW       63
	MOVWF       ?lstr3_oscopp+0 
	CLRF        ?lstr3_oscopp+1 
	MOVLW       ?lstr3_oscopp+0
	MOVWF       FARG_strcpy_from+0 
	MOVLW       hi_addr(?lstr3_oscopp+0)
	MOVWF       FARG_strcpy_from+1 
	CALL        _strcpy+0, 0
;oscopp.c,241 :: 		}
L_main39:
;oscopp.c,243 :: 		resadc = maxduty[1]+maxduty[0] ;
	MOVF        _maxduty+0, 0 
	ADDWF       _maxduty+2, 0 
	MOVWF       R4 
	MOVF        _maxduty+1, 0 
	ADDWFC      _maxduty+3, 0 
	MOVWF       R5 
	MOVF        R4, 0 
	MOVWF       _resadc+0 
	MOVF        R5, 0 
	MOVWF       _resadc+1 
;oscopp.c,244 :: 		freqoencc = ( 14000 ) / resadc ;
	MOVLW       176
	MOVWF       R0 
	MOVLW       54
	MOVWF       R1 
	CALL        _Div_16x16_U+0, 0
	MOVF        R0, 0 
	MOVWF       _freqoencc+0 
	MOVF        R1, 0 
	MOVWF       _freqoencc+1 
;oscopp.c,246 :: 		if (resadc>1000) {
	MOVF        _resadc+1, 0 
	SUBLW       3
	BTFSS       STATUS+0, 2 
	GOTO        L__main77
	MOVF        _resadc+0, 0 
	SUBLW       232
L__main77:
	BTFSC       STATUS+0, 0 
	GOTO        L_main40
;oscopp.c,247 :: 		resadc = 1000 ;
	MOVLW       232
	MOVWF       _resadc+0 
	MOVLW       3
	MOVWF       _resadc+1 
;oscopp.c,248 :: 		}
L_main40:
;oscopp.c,250 :: 		inttostr(freqoencc,otxt6);
	MOVF        _freqoencc+0, 0 
	MOVWF       FARG_IntToStr_input+0 
	MOVF        _freqoencc+1, 0 
	MOVWF       FARG_IntToStr_input+1 
	MOVLW       _otxt6+0
	MOVWF       FARG_IntToStr_output+0 
	MOVLW       hi_addr(_otxt6+0)
	MOVWF       FARG_IntToStr_output+1 
	CALL        _IntToStr+0, 0
;oscopp.c,253 :: 		mint1 = ((mmin*10)/510) ;
	MOVLW       10
	MULWF       _mmin+0 
	MOVF        PRODL+0, 0 
	MOVWF       FLOC__main+0 
	MOVF        PRODH+0, 0 
	MOVWF       FLOC__main+1 
	MOVLW       254
	MOVWF       R4 
	MOVLW       1
	MOVWF       R5 
	MOVF        FLOC__main+0, 0 
	MOVWF       R0 
	MOVF        FLOC__main+1, 0 
	MOVWF       R1 
	CALL        _Div_16x16_S+0, 0
	MOVF        R0, 0 
	MOVWF       _mint1+0 
	MOVF        R1, 0 
	MOVWF       _mint1+1 
;oscopp.c,254 :: 		mint2 = ((mmin*10)%510)/10 ;  // mint2 = ((mmin*10)%512)/10
	MOVLW       254
	MOVWF       R4 
	MOVLW       1
	MOVWF       R5 
	MOVF        FLOC__main+0, 0 
	MOVWF       R0 
	MOVF        FLOC__main+1, 0 
	MOVWF       R1 
	CALL        _Div_16x16_S+0, 0
	MOVF        R8, 0 
	MOVWF       R0 
	MOVF        R9, 0 
	MOVWF       R1 
	MOVLW       10
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	CALL        _Div_16x16_S+0, 0
	MOVF        R0, 0 
	MOVWF       _mint2+0 
	MOVF        R1, 0 
	MOVWF       _mint2+1 
;oscopp.c,255 :: 		inttostr(mint1,otxt1);
	MOVF        _mint1+0, 0 
	MOVWF       FARG_IntToStr_input+0 
	MOVF        _mint1+1, 0 
	MOVWF       FARG_IntToStr_input+1 
	MOVLW       _otxt1+0
	MOVWF       FARG_IntToStr_output+0 
	MOVLW       hi_addr(_otxt1+0)
	MOVWF       FARG_IntToStr_output+1 
	CALL        _IntToStr+0, 0
;oscopp.c,256 :: 		inttostr(mint2,otxt2);
	MOVF        _mint2+0, 0 
	MOVWF       FARG_IntToStr_input+0 
	MOVF        _mint2+1, 0 
	MOVWF       FARG_IntToStr_input+1 
	MOVLW       _otxt2+0
	MOVWF       FARG_IntToStr_output+0 
	MOVLW       hi_addr(_otxt2+0)
	MOVWF       FARG_IntToStr_output+1 
	CALL        _IntToStr+0, 0
;oscopp.c,258 :: 		mint1 = ((mmax*10)/510) ;
	MOVLW       10
	MULWF       _mmax+0 
	MOVF        PRODL+0, 0 
	MOVWF       FLOC__main+0 
	MOVF        PRODH+0, 0 
	MOVWF       FLOC__main+1 
	MOVLW       254
	MOVWF       R4 
	MOVLW       1
	MOVWF       R5 
	MOVF        FLOC__main+0, 0 
	MOVWF       R0 
	MOVF        FLOC__main+1, 0 
	MOVWF       R1 
	CALL        _Div_16x16_S+0, 0
	MOVF        R0, 0 
	MOVWF       _mint1+0 
	MOVF        R1, 0 
	MOVWF       _mint1+1 
;oscopp.c,259 :: 		mint2 = ((mmax*10)%510)/10 ;  //  mint2 = ((mmax*10)%512)/10
	MOVLW       254
	MOVWF       R4 
	MOVLW       1
	MOVWF       R5 
	MOVF        FLOC__main+0, 0 
	MOVWF       R0 
	MOVF        FLOC__main+1, 0 
	MOVWF       R1 
	CALL        _Div_16x16_S+0, 0
	MOVF        R8, 0 
	MOVWF       R0 
	MOVF        R9, 0 
	MOVWF       R1 
	MOVLW       10
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	CALL        _Div_16x16_S+0, 0
	MOVF        R0, 0 
	MOVWF       _mint2+0 
	MOVF        R1, 0 
	MOVWF       _mint2+1 
;oscopp.c,260 :: 		inttostr(mint1,otxt3);
	MOVF        _mint1+0, 0 
	MOVWF       FARG_IntToStr_input+0 
	MOVF        _mint1+1, 0 
	MOVWF       FARG_IntToStr_input+1 
	MOVLW       _otxt3+0
	MOVWF       FARG_IntToStr_output+0 
	MOVLW       hi_addr(_otxt3+0)
	MOVWF       FARG_IntToStr_output+1 
	CALL        _IntToStr+0, 0
;oscopp.c,261 :: 		inttostr(mint2,otxt4);
	MOVF        _mint2+0, 0 
	MOVWF       FARG_IntToStr_input+0 
	MOVF        _mint2+1, 0 
	MOVWF       FARG_IntToStr_input+1 
	MOVLW       _otxt4+0
	MOVWF       FARG_IntToStr_output+0 
	MOVLW       hi_addr(_otxt4+0)
	MOVWF       FARG_IntToStr_output+1 
	CALL        _IntToStr+0, 0
;oscopp.c,263 :: 		for ( ffw = 0 ; ffw < 129 ; ffw++ ) {
	CLRF        _ffw+0 
	CLRF        _ffw+1 
L_main41:
	MOVLW       0
	SUBWF       _ffw+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main78
	MOVLW       129
	SUBWF       _ffw+0, 0 
L__main78:
	BTFSC       STATUS+0, 0 
	GOTO        L_main42
;oscopp.c,264 :: 		adread[ffw]=(ADC_Read(0) / 16);
	MOVLW       _adread+0
	ADDWF       _ffw+0, 0 
	MOVWF       FLOC__main+0 
	MOVLW       hi_addr(_adread+0)
	ADDWFC      _ffw+1, 0 
	MOVWF       FLOC__main+1 
	CLRF        FARG_ADC_Read_channel+0 
	CALL        _ADC_Read+0, 0
	MOVF        R0, 0 
	MOVWF       R2 
	MOVF        R1, 0 
	MOVWF       R3 
	RRCF        R3, 1 
	RRCF        R2, 1 
	BCF         R3, 7 
	RRCF        R3, 1 
	RRCF        R2, 1 
	BCF         R3, 7 
	RRCF        R3, 1 
	RRCF        R2, 1 
	BCF         R3, 7 
	RRCF        R3, 1 
	RRCF        R2, 1 
	BCF         R3, 7 
	MOVFF       FLOC__main+0, FSR1
	MOVFF       FLOC__main+1, FSR1H
	MOVF        R2, 0 
	MOVWF       POSTINC1+0 
;oscopp.c,263 :: 		for ( ffw = 0 ; ffw < 129 ; ffw++ ) {
	INFSNZ      _ffw+0, 1 
	INCF        _ffw+1, 1 
;oscopp.c,265 :: 		}
	GOTO        L_main41
L_main42:
;oscopp.c,267 :: 		if (resadc > 40 ) {
	MOVLW       0
	MOVWF       R0 
	MOVF        _resadc+1, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main79
	MOVF        _resadc+0, 0 
	SUBLW       40
L__main79:
	BTFSC       STATUS+0, 0 
	GOTO        L_main44
;oscopp.c,269 :: 		if (0<freqoencc) {
	MOVLW       0
	MOVWF       R0 
	MOVF        _freqoencc+1, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main80
	MOVF        _freqoencc+0, 0 
	SUBLW       0
L__main80:
	BTFSC       STATUS+0, 0 
	GOTO        L_main45
;oscopp.c,270 :: 		if (freqoencc<100) {
	MOVLW       0
	SUBWF       _freqoencc+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main81
	MOVLW       100
	SUBWF       _freqoencc+0, 0 
L__main81:
	BTFSC       STATUS+0, 0 
	GOTO        L_main46
;oscopp.c,271 :: 		resadc = (resadc*2)+(resadc/10) ;
	MOVF        _resadc+0, 0 
	MOVWF       FLOC__main+0 
	MOVF        _resadc+1, 0 
	MOVWF       FLOC__main+1 
	RLCF        FLOC__main+0, 1 
	BCF         FLOC__main+0, 0 
	RLCF        FLOC__main+1, 1 
	MOVLW       10
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVF        _resadc+0, 0 
	MOVWF       R0 
	MOVF        _resadc+1, 0 
	MOVWF       R1 
	CALL        _Div_16x16_U+0, 0
	MOVF        R0, 0 
	ADDWF       FLOC__main+0, 0 
	MOVWF       _resadc+0 
	MOVF        R1, 0 
	ADDWFC      FLOC__main+1, 0 
	MOVWF       _resadc+1 
;oscopp.c,272 :: 		}
L_main46:
;oscopp.c,273 :: 		}
L_main45:
;oscopp.c,274 :: 		if (freqoencc>99) {
	MOVLW       0
	MOVWF       R0 
	MOVF        _freqoencc+1, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main82
	MOVF        _freqoencc+0, 0 
	SUBLW       99
L__main82:
	BTFSC       STATUS+0, 0 
	GOTO        L_main47
;oscopp.c,275 :: 		resadc = (resadc)+(resadc/10);
	MOVLW       10
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVF        _resadc+0, 0 
	MOVWF       R0 
	MOVF        _resadc+1, 0 
	MOVWF       R1 
	CALL        _Div_16x16_U+0, 0
	MOVF        R0, 0 
	ADDWF       _resadc+0, 1 
	MOVF        R1, 0 
	ADDWFC      _resadc+1, 1 
;oscopp.c,276 :: 		}
L_main47:
;oscopp.c,278 :: 		for ( ffw = 0 ; ffw < 129 ; ffw++ ) {
	CLRF        _ffw+0 
	CLRF        _ffw+1 
L_main48:
	MOVLW       0
	SUBWF       _ffw+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main83
	MOVLW       129
	SUBWF       _ffw+0, 0 
L__main83:
	BTFSC       STATUS+0, 0 
	GOTO        L_main49
;oscopp.c,279 :: 		adread[ffw]=(ADC_Read(0) / 16);
	MOVLW       _adread+0
	ADDWF       _ffw+0, 0 
	MOVWF       FLOC__main+0 
	MOVLW       hi_addr(_adread+0)
	ADDWFC      _ffw+1, 0 
	MOVWF       FLOC__main+1 
	CLRF        FARG_ADC_Read_channel+0 
	CALL        _ADC_Read+0, 0
	MOVF        R0, 0 
	MOVWF       R2 
	MOVF        R1, 0 
	MOVWF       R3 
	RRCF        R3, 1 
	RRCF        R2, 1 
	BCF         R3, 7 
	RRCF        R3, 1 
	RRCF        R2, 1 
	BCF         R3, 7 
	RRCF        R3, 1 
	RRCF        R2, 1 
	BCF         R3, 7 
	RRCF        R3, 1 
	RRCF        R2, 1 
	BCF         R3, 7 
	MOVFF       FLOC__main+0, FSR1
	MOVFF       FLOC__main+1, FSR1H
	MOVF        R2, 0 
	MOVWF       POSTINC1+0 
;oscopp.c,280 :: 		for ( ff = 0 ; ff < resadc ; ff++ ) {
	CLRF        _ff+0 
	CLRF        _ff+1 
L_main51:
	MOVF        _resadc+1, 0 
	SUBWF       _ff+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main84
	MOVF        _resadc+0, 0 
	SUBWF       _ff+0, 0 
L__main84:
	BTFSC       STATUS+0, 0 
	GOTO        L_main52
	INFSNZ      _ff+0, 1 
	INCF        _ff+1, 1 
;oscopp.c,283 :: 		}
	GOTO        L_main51
L_main52:
;oscopp.c,278 :: 		for ( ffw = 0 ; ffw < 129 ; ffw++ ) {
	INFSNZ      _ffw+0, 1 
	INCF        _ffw+1, 1 
;oscopp.c,284 :: 		}
	GOTO        L_main48
L_main49:
;oscopp.c,285 :: 		}
L_main44:
;oscopp.c,287 :: 		for ( ffw = 0 ; ffw < 200 ; ffw++ ) {
	CLRF        _ffw+0 
	CLRF        _ffw+1 
L_main54:
	MOVLW       0
	SUBWF       _ffw+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main85
	MOVLW       200
	SUBWF       _ffw+0, 0 
L__main85:
	BTFSC       STATUS+0, 0 
	GOTO        L_main55
;oscopp.c,288 :: 		adread[ffw]=63-(adread[ffw]);
	MOVLW       _adread+0
	ADDWF       _ffw+0, 0 
	MOVWF       R1 
	MOVLW       hi_addr(_adread+0)
	ADDWFC      _ffw+1, 0 
	MOVWF       R2 
	MOVFF       R1, FSR2
	MOVFF       R2, FSR2H
	MOVF        POSTINC2+0, 0 
	SUBLW       63
	MOVWF       R0 
	MOVFF       R1, FSR1
	MOVFF       R2, FSR1H
	MOVF        R0, 0 
	MOVWF       POSTINC1+0 
;oscopp.c,287 :: 		for ( ffw = 0 ; ffw < 200 ; ffw++ ) {
	INFSNZ      _ffw+0, 1 
	INCF        _ffw+1, 1 
;oscopp.c,289 :: 		}
	GOTO        L_main54
L_main55:
;oscopp.c,291 :: 		Glcd_Fill(0x00);
	CLRF        FARG_Glcd_Fill_pattern+0 
	CALL        _Glcd_Fill+0, 0
;oscopp.c,292 :: 		for ( ff = 15 ; ff < 128 ; ff++ ) {
	MOVLW       15
	MOVWF       _ff+0 
	MOVLW       0
	MOVWF       _ff+1 
L_main57:
	MOVLW       0
	SUBWF       _ff+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main86
	MOVLW       128
	SUBWF       _ff+0, 0 
L__main86:
	BTFSC       STATUS+0, 0 
	GOTO        L_main58
;oscopp.c,294 :: 		Glcd_Line(ff, adread[ff], ff+1 ,adread[ff+1], 1);
	MOVF        _ff+0, 0 
	MOVWF       FARG_Glcd_Line_x_start+0 
	MOVF        _ff+1, 0 
	MOVWF       FARG_Glcd_Line_x_start+1 
	MOVLW       _adread+0
	ADDWF       _ff+0, 0 
	MOVWF       FSR0 
	MOVLW       hi_addr(_adread+0)
	ADDWFC      _ff+1, 0 
	MOVWF       FSR0H 
	MOVF        POSTINC0+0, 0 
	MOVWF       FARG_Glcd_Line_y_start+0 
	MOVLW       0
	MOVWF       FARG_Glcd_Line_y_start+1 
	MOVLW       0
	MOVWF       FARG_Glcd_Line_y_start+1 
	MOVLW       1
	ADDWF       _ff+0, 0 
	MOVWF       R0 
	MOVLW       0
	ADDWFC      _ff+1, 0 
	MOVWF       R1 
	MOVF        R0, 0 
	MOVWF       FARG_Glcd_Line_x_end+0 
	MOVF        R1, 0 
	MOVWF       FARG_Glcd_Line_x_end+1 
	MOVLW       _adread+0
	ADDWF       R0, 0 
	MOVWF       FSR0 
	MOVLW       hi_addr(_adread+0)
	ADDWFC      R1, 0 
	MOVWF       FSR0H 
	MOVF        POSTINC0+0, 0 
	MOVWF       FARG_Glcd_Line_y_end+0 
	MOVLW       0
	MOVWF       FARG_Glcd_Line_y_end+1 
	MOVLW       0
	MOVWF       FARG_Glcd_Line_y_end+1 
	MOVLW       1
	MOVWF       FARG_Glcd_Line_color+0 
	CALL        _Glcd_Line+0, 0
;oscopp.c,292 :: 		for ( ff = 15 ; ff < 128 ; ff++ ) {
	INFSNZ      _ff+0, 1 
	INCF        _ff+1, 1 
;oscopp.c,295 :: 		}
	GOTO        L_main57
L_main58:
;oscopp.c,297 :: 		strcpy(otxt1,Rtrim(otxt1));
	MOVLW       _otxt1+0
	MOVWF       FARG_Rtrim_string+0 
	MOVLW       hi_addr(_otxt1+0)
	MOVWF       FARG_Rtrim_string+1 
	CALL        _Rtrim+0, 0
	MOVF        R0, 0 
	MOVWF       FARG_strcpy_from+0 
	MOVF        R1, 0 
	MOVWF       FARG_strcpy_from+1 
	MOVLW       _otxt1+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt1+0)
	MOVWF       FARG_strcpy_to+1 
	CALL        _strcpy+0, 0
;oscopp.c,298 :: 		strcpy(otxt1,ltrim(otxt1));
	MOVLW       _otxt1+0
	MOVWF       FARG_Ltrim_string+0 
	MOVLW       hi_addr(_otxt1+0)
	MOVWF       FARG_Ltrim_string+1 
	CALL        _Ltrim+0, 0
	MOVF        R0, 0 
	MOVWF       FARG_strcpy_from+0 
	MOVF        R1, 0 
	MOVWF       FARG_strcpy_from+1 
	MOVLW       _otxt1+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt1+0)
	MOVWF       FARG_strcpy_to+1 
	CALL        _strcpy+0, 0
;oscopp.c,299 :: 		strcpy(otxt2,Rtrim(otxt2));
	MOVLW       _otxt2+0
	MOVWF       FARG_Rtrim_string+0 
	MOVLW       hi_addr(_otxt2+0)
	MOVWF       FARG_Rtrim_string+1 
	CALL        _Rtrim+0, 0
	MOVF        R0, 0 
	MOVWF       FARG_strcpy_from+0 
	MOVF        R1, 0 
	MOVWF       FARG_strcpy_from+1 
	MOVLW       _otxt2+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt2+0)
	MOVWF       FARG_strcpy_to+1 
	CALL        _strcpy+0, 0
;oscopp.c,300 :: 		strcpy(otxt2,ltrim(otxt2));
	MOVLW       _otxt2+0
	MOVWF       FARG_Ltrim_string+0 
	MOVLW       hi_addr(_otxt2+0)
	MOVWF       FARG_Ltrim_string+1 
	CALL        _Ltrim+0, 0
	MOVF        R0, 0 
	MOVWF       FARG_strcpy_from+0 
	MOVF        R1, 0 
	MOVWF       FARG_strcpy_from+1 
	MOVLW       _otxt2+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt2+0)
	MOVWF       FARG_strcpy_to+1 
	CALL        _strcpy+0, 0
;oscopp.c,302 :: 		strcpy(otxt3,Rtrim(otxt3));
	MOVLW       _otxt3+0
	MOVWF       FARG_Rtrim_string+0 
	MOVLW       hi_addr(_otxt3+0)
	MOVWF       FARG_Rtrim_string+1 
	CALL        _Rtrim+0, 0
	MOVF        R0, 0 
	MOVWF       FARG_strcpy_from+0 
	MOVF        R1, 0 
	MOVWF       FARG_strcpy_from+1 
	MOVLW       _otxt3+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt3+0)
	MOVWF       FARG_strcpy_to+1 
	CALL        _strcpy+0, 0
;oscopp.c,303 :: 		strcpy(otxt3,ltrim(otxt3));
	MOVLW       _otxt3+0
	MOVWF       FARG_Ltrim_string+0 
	MOVLW       hi_addr(_otxt3+0)
	MOVWF       FARG_Ltrim_string+1 
	CALL        _Ltrim+0, 0
	MOVF        R0, 0 
	MOVWF       FARG_strcpy_from+0 
	MOVF        R1, 0 
	MOVWF       FARG_strcpy_from+1 
	MOVLW       _otxt3+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt3+0)
	MOVWF       FARG_strcpy_to+1 
	CALL        _strcpy+0, 0
;oscopp.c,304 :: 		strcpy(otxt4,Rtrim(otxt4));
	MOVLW       _otxt4+0
	MOVWF       FARG_Rtrim_string+0 
	MOVLW       hi_addr(_otxt4+0)
	MOVWF       FARG_Rtrim_string+1 
	CALL        _Rtrim+0, 0
	MOVF        R0, 0 
	MOVWF       FARG_strcpy_from+0 
	MOVF        R1, 0 
	MOVWF       FARG_strcpy_from+1 
	MOVLW       _otxt4+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt4+0)
	MOVWF       FARG_strcpy_to+1 
	CALL        _strcpy+0, 0
;oscopp.c,305 :: 		strcpy(otxt4,ltrim(otxt4));
	MOVLW       _otxt4+0
	MOVWF       FARG_Ltrim_string+0 
	MOVLW       hi_addr(_otxt4+0)
	MOVWF       FARG_Ltrim_string+1 
	CALL        _Ltrim+0, 0
	MOVF        R0, 0 
	MOVWF       FARG_strcpy_from+0 
	MOVF        R1, 0 
	MOVWF       FARG_strcpy_from+1 
	MOVLW       _otxt4+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt4+0)
	MOVWF       FARG_strcpy_to+1 
	CALL        _strcpy+0, 0
;oscopp.c,307 :: 		strcpy(otxt5,Rtrim(otxt5));
	MOVLW       _otxt5+0
	MOVWF       FARG_Rtrim_string+0 
	MOVLW       hi_addr(_otxt5+0)
	MOVWF       FARG_Rtrim_string+1 
	CALL        _Rtrim+0, 0
	MOVF        R0, 0 
	MOVWF       FARG_strcpy_from+0 
	MOVF        R1, 0 
	MOVWF       FARG_strcpy_from+1 
	MOVLW       _otxt5+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt5+0)
	MOVWF       FARG_strcpy_to+1 
	CALL        _strcpy+0, 0
;oscopp.c,308 :: 		strcpy(otxt5,ltrim(otxt5));
	MOVLW       _otxt5+0
	MOVWF       FARG_Ltrim_string+0 
	MOVLW       hi_addr(_otxt5+0)
	MOVWF       FARG_Ltrim_string+1 
	CALL        _Ltrim+0, 0
	MOVF        R0, 0 
	MOVWF       FARG_strcpy_from+0 
	MOVF        R1, 0 
	MOVWF       FARG_strcpy_from+1 
	MOVLW       _otxt5+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt5+0)
	MOVWF       FARG_strcpy_to+1 
	CALL        _strcpy+0, 0
;oscopp.c,309 :: 		strcpy(otxt6,Rtrim(otxt6));
	MOVLW       _otxt6+0
	MOVWF       FARG_Rtrim_string+0 
	MOVLW       hi_addr(_otxt6+0)
	MOVWF       FARG_Rtrim_string+1 
	CALL        _Rtrim+0, 0
	MOVF        R0, 0 
	MOVWF       FARG_strcpy_from+0 
	MOVF        R1, 0 
	MOVWF       FARG_strcpy_from+1 
	MOVLW       _otxt6+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt6+0)
	MOVWF       FARG_strcpy_to+1 
	CALL        _strcpy+0, 0
;oscopp.c,310 :: 		strcpy(otxt6,ltrim(otxt6));
	MOVLW       _otxt6+0
	MOVWF       FARG_Ltrim_string+0 
	MOVLW       hi_addr(_otxt6+0)
	MOVWF       FARG_Ltrim_string+1 
	CALL        _Ltrim+0, 0
	MOVF        R0, 0 
	MOVWF       FARG_strcpy_from+0 
	MOVF        R1, 0 
	MOVWF       FARG_strcpy_from+1 
	MOVLW       _otxt6+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(_otxt6+0)
	MOVWF       FARG_strcpy_to+1 
	CALL        _strcpy+0, 0
;oscopp.c,312 :: 		Glcd_Write_Text("OFFSET", 0, 0, 1);
	MOVLW       79
	MOVWF       ?lstr4_oscopp+0 
	MOVLW       70
	MOVWF       ?lstr4_oscopp+1 
	MOVLW       70
	MOVWF       ?lstr4_oscopp+2 
	MOVLW       83
	MOVWF       ?lstr4_oscopp+3 
	MOVLW       69
	MOVWF       ?lstr4_oscopp+4 
	MOVLW       84
	MOVWF       ?lstr4_oscopp+5 
	CLRF        ?lstr4_oscopp+6 
	MOVLW       ?lstr4_oscopp+0
	MOVWF       FARG_Glcd_Write_Text_text+0 
	MOVLW       hi_addr(?lstr4_oscopp+0)
	MOVWF       FARG_Glcd_Write_Text_text+1 
	CLRF        FARG_Glcd_Write_Text_x_pos+0 
	CLRF        FARG_Glcd_Write_Text_page_num+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_color+0 
	CALL        _Glcd_Write_Text+0, 0
;oscopp.c,313 :: 		Glcd_Write_Text(otxt1, 0, 1, 1);
	MOVLW       _otxt1+0
	MOVWF       FARG_Glcd_Write_Text_text+0 
	MOVLW       hi_addr(_otxt1+0)
	MOVWF       FARG_Glcd_Write_Text_text+1 
	CLRF        FARG_Glcd_Write_Text_x_pos+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_page_num+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_color+0 
	CALL        _Glcd_Write_Text+0, 0
;oscopp.c,314 :: 		Glcd_Write_Text(".", 7, 1, 1);
	MOVLW       46
	MOVWF       ?lstr5_oscopp+0 
	CLRF        ?lstr5_oscopp+1 
	MOVLW       ?lstr5_oscopp+0
	MOVWF       FARG_Glcd_Write_Text_text+0 
	MOVLW       hi_addr(?lstr5_oscopp+0)
	MOVWF       FARG_Glcd_Write_Text_text+1 
	MOVLW       7
	MOVWF       FARG_Glcd_Write_Text_x_pos+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_page_num+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_color+0 
	CALL        _Glcd_Write_Text+0, 0
;oscopp.c,315 :: 		Glcd_Write_Text(otxt2, 10, 1, 1);
	MOVLW       _otxt2+0
	MOVWF       FARG_Glcd_Write_Text_text+0 
	MOVLW       hi_addr(_otxt2+0)
	MOVWF       FARG_Glcd_Write_Text_text+1 
	MOVLW       10
	MOVWF       FARG_Glcd_Write_Text_x_pos+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_page_num+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_color+0 
	CALL        _Glcd_Write_Text+0, 0
;oscopp.c,317 :: 		Glcd_Write_Text("PEAK", 0, 2, 1);
	MOVLW       80
	MOVWF       ?lstr6_oscopp+0 
	MOVLW       69
	MOVWF       ?lstr6_oscopp+1 
	MOVLW       65
	MOVWF       ?lstr6_oscopp+2 
	MOVLW       75
	MOVWF       ?lstr6_oscopp+3 
	CLRF        ?lstr6_oscopp+4 
	MOVLW       ?lstr6_oscopp+0
	MOVWF       FARG_Glcd_Write_Text_text+0 
	MOVLW       hi_addr(?lstr6_oscopp+0)
	MOVWF       FARG_Glcd_Write_Text_text+1 
	CLRF        FARG_Glcd_Write_Text_x_pos+0 
	MOVLW       2
	MOVWF       FARG_Glcd_Write_Text_page_num+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_color+0 
	CALL        _Glcd_Write_Text+0, 0
;oscopp.c,318 :: 		Glcd_Write_Text(otxt3, 0, 3, 1);
	MOVLW       _otxt3+0
	MOVWF       FARG_Glcd_Write_Text_text+0 
	MOVLW       hi_addr(_otxt3+0)
	MOVWF       FARG_Glcd_Write_Text_text+1 
	CLRF        FARG_Glcd_Write_Text_x_pos+0 
	MOVLW       3
	MOVWF       FARG_Glcd_Write_Text_page_num+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_color+0 
	CALL        _Glcd_Write_Text+0, 0
;oscopp.c,319 :: 		Glcd_Write_Text(".", 7, 3, 1);
	MOVLW       46
	MOVWF       ?lstr7_oscopp+0 
	CLRF        ?lstr7_oscopp+1 
	MOVLW       ?lstr7_oscopp+0
	MOVWF       FARG_Glcd_Write_Text_text+0 
	MOVLW       hi_addr(?lstr7_oscopp+0)
	MOVWF       FARG_Glcd_Write_Text_text+1 
	MOVLW       7
	MOVWF       FARG_Glcd_Write_Text_x_pos+0 
	MOVLW       3
	MOVWF       FARG_Glcd_Write_Text_page_num+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_color+0 
	CALL        _Glcd_Write_Text+0, 0
;oscopp.c,320 :: 		Glcd_Write_Text(otxt4, 10, 3, 1);
	MOVLW       _otxt4+0
	MOVWF       FARG_Glcd_Write_Text_text+0 
	MOVLW       hi_addr(_otxt4+0)
	MOVWF       FARG_Glcd_Write_Text_text+1 
	MOVLW       10
	MOVWF       FARG_Glcd_Write_Text_x_pos+0 
	MOVLW       3
	MOVWF       FARG_Glcd_Write_Text_page_num+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_color+0 
	CALL        _Glcd_Write_Text+0, 0
;oscopp.c,322 :: 		Glcd_Write_Text("DUTY", 0, 4, 1);
	MOVLW       68
	MOVWF       ?lstr8_oscopp+0 
	MOVLW       85
	MOVWF       ?lstr8_oscopp+1 
	MOVLW       84
	MOVWF       ?lstr8_oscopp+2 
	MOVLW       89
	MOVWF       ?lstr8_oscopp+3 
	CLRF        ?lstr8_oscopp+4 
	MOVLW       ?lstr8_oscopp+0
	MOVWF       FARG_Glcd_Write_Text_text+0 
	MOVLW       hi_addr(?lstr8_oscopp+0)
	MOVWF       FARG_Glcd_Write_Text_text+1 
	CLRF        FARG_Glcd_Write_Text_x_pos+0 
	MOVLW       4
	MOVWF       FARG_Glcd_Write_Text_page_num+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_color+0 
	CALL        _Glcd_Write_Text+0, 0
;oscopp.c,323 :: 		Glcd_Write_Text(otxt5, 0, 5, 1);
	MOVLW       _otxt5+0
	MOVWF       FARG_Glcd_Write_Text_text+0 
	MOVLW       hi_addr(_otxt5+0)
	MOVWF       FARG_Glcd_Write_Text_text+1 
	CLRF        FARG_Glcd_Write_Text_x_pos+0 
	MOVLW       5
	MOVWF       FARG_Glcd_Write_Text_page_num+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_color+0 
	CALL        _Glcd_Write_Text+0, 0
;oscopp.c,325 :: 		Glcd_Write_Text("FREQ", 0, 6, 1);
	MOVLW       70
	MOVWF       ?lstr9_oscopp+0 
	MOVLW       82
	MOVWF       ?lstr9_oscopp+1 
	MOVLW       69
	MOVWF       ?lstr9_oscopp+2 
	MOVLW       81
	MOVWF       ?lstr9_oscopp+3 
	CLRF        ?lstr9_oscopp+4 
	MOVLW       ?lstr9_oscopp+0
	MOVWF       FARG_Glcd_Write_Text_text+0 
	MOVLW       hi_addr(?lstr9_oscopp+0)
	MOVWF       FARG_Glcd_Write_Text_text+1 
	CLRF        FARG_Glcd_Write_Text_x_pos+0 
	MOVLW       6
	MOVWF       FARG_Glcd_Write_Text_page_num+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_color+0 
	CALL        _Glcd_Write_Text+0, 0
;oscopp.c,326 :: 		Glcd_Write_Text(otxt6, 0, 7, 1);
	MOVLW       _otxt6+0
	MOVWF       FARG_Glcd_Write_Text_text+0 
	MOVLW       hi_addr(_otxt6+0)
	MOVWF       FARG_Glcd_Write_Text_text+1 
	CLRF        FARG_Glcd_Write_Text_x_pos+0 
	MOVLW       7
	MOVWF       FARG_Glcd_Write_Text_page_num+0 
	MOVLW       1
	MOVWF       FARG_Glcd_Write_Text_color+0 
	CALL        _Glcd_Write_Text+0, 0
;oscopp.c,329 :: 		delay_ms(1000);
	MOVLW       61
	MOVWF       R11, 0
	MOVLW       225
	MOVWF       R12, 0
	MOVLW       63
	MOVWF       R13, 0
L_main60:
	DECFSZ      R13, 1, 1
	BRA         L_main60
	DECFSZ      R12, 1, 1
	BRA         L_main60
	DECFSZ      R11, 1, 1
	BRA         L_main60
	NOP
	NOP
;oscopp.c,330 :: 		goto sttt ;
	GOTO        ___main_sttt
;oscopp.c,335 :: 		}
L_end_main:
	GOTO        $+0
; end of _main
