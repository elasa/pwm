
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;basico628.mpas,14 :: 		begin
;basico628.mpas,15 :: 		inc(toca);
	INCF       _toca+0, 1
	BTFSC      STATUS+0, 2
	INCF       _toca+1, 1
;basico628.mpas,16 :: 		if toca>=muestras then toca:=0;
	MOVLW      0
	SUBWF      _toca+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt16
	MOVLW      120
	SUBWF      _toca+0, 0
L__interrupt16:
	BTFSS      STATUS+0, 0
	GOTO       L__interrupt2
	CLRF       _toca+0
	CLRF       _toca+1
L__interrupt2:
;basico628.mpas,18 :: 		es:=onda[toca];
	MOVF       _toca+0, 0
	MOVWF      R0+0
	MOVF       _toca+1, 0
	MOVWF      R0+1
	RLF        R0+0, 1
	RLF        R0+1, 1
	BCF        R0+0, 0
	MOVLW      _ONDA+0
	ADDWF      R0+0, 1
	MOVLW      hi_addr(_ONDA+0)
	BTFSC      STATUS+0, 0
	ADDLW      1
	ADDWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      ___DoICPAddr+0
	MOVF       R0+1, 0
	MOVWF      ___DoICPAddr+1
	CALL       _____DoICP+0
	MOVWF      R3+0
	INCF       ___DoICPAddr+0, 1
	BTFSC      STATUS+0, 2
	INCF       ___DoICPAddr+1, 1
	CALL       _____DoICP+0
	MOVWF      R3+1
	MOVF       R3+0, 0
	MOVWF      _es+0
	MOVF       R3+1, 0
	MOVWF      _es+1
;basico628.mpas,23 :: 		ccpr1l:=es shr 2;
	MOVF       R3+0, 0
	MOVWF      R0+0
	MOVF       R3+1, 0
	MOVWF      R0+1
	RRF        R0+1, 1
	RRF        R0+0, 1
	BCF        R0+1, 7
	RRF        R0+1, 1
	RRF        R0+0, 1
	BCF        R0+1, 7
	MOVF       R0+0, 0
	MOVWF      CCPR1L+0
;basico628.mpas,25 :: 		ccp1con.ccp1x:=es.1;//8;
	BTFSC      _es+0, 1
	GOTO       L__interrupt17
	BCF        CCP1CON+0, 5
	GOTO       L__interrupt18
L__interrupt17:
	BSF        CCP1CON+0, 5
L__interrupt18:
;basico628.mpas,26 :: 		ccp1con.ccp1y:=es.0;//es.9;
	BTFSC      _es+0, 0
	GOTO       L__interrupt19
	BCF        CCP1CON+0, 4
	GOTO       L__interrupt20
L__interrupt19:
	BSF        CCP1CON+0, 4
L__interrupt20:
;basico628.mpas,29 :: 		TMR2IF_bit:=0;
	BCF        TMR2IF_bit+0, 1
;basico628.mpas,30 :: 		end;
L__interrupt15:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_apaga:

;basico628.mpas,35 :: 		begin
;basico628.mpas,36 :: 		ccp1con:=%00100000;//
	MOVLW      32
	MOVWF      CCP1CON+0
;basico628.mpas,37 :: 		t2con.TMR2ON:=0;//apaga pwm
	BCF        T2CON+0, 2
;basico628.mpas,38 :: 		portb.3:=0;
	BCF        PORTB+0, 3
;basico628.mpas,39 :: 		end;
	RETURN
; end of _apaga

_prende:

;basico628.mpas,42 :: 		begin
;basico628.mpas,43 :: 		ccp1con:=%00101100;// configura como pwm;
	MOVLW      44
	MOVWF      CCP1CON+0
;basico628.mpas,44 :: 		t2con.TMR2ON:=1;//prende pwm
	BSF        T2CON+0, 2
;basico628.mpas,45 :: 		end;
	RETURN
; end of _prende

_main:

;basico628.mpas,48 :: 		begin
;basico628.mpas,50 :: 		pcon:=0;
	CLRF       PCON+0
;basico628.mpas,51 :: 		pcon.oscf:=1;
	BSF        PCON+0, 3
;basico628.mpas,52 :: 		vrcon:=0;
	CLRF       VRCON+0
;basico628.mpas,54 :: 		intcon:=%01000000;//no interrupciones
	MOVLW      64
	MOVWF      INTCON+0
;basico628.mpas,55 :: 		option_reg:=0; //pull up
	CLRF       OPTION_REG+0
;basico628.mpas,57 :: 		cmcon:=7;//comaprador apagado;
	MOVLW      7
	MOVWF      CMCON+0
;basico628.mpas,60 :: 		PIE1:=0;
	CLRF       PIE1+0
;basico628.mpas,61 :: 		PIR1:=0;
	CLRF       PIR1+0
;basico628.mpas,63 :: 		trisa:=0;
	CLRF       TRISA+0
;basico628.mpas,64 :: 		trisb:=0;
	CLRF       TRISB+0
;basico628.mpas,66 :: 		TMR2IE_bit:=1;
	BSF        TMR2IE_bit+0, 1
;basico628.mpas,70 :: 		ccp1con:=%00101100;// configura como pwm;
	MOVLW      44
	MOVWF      CCP1CON+0
;basico628.mpas,73 :: 		t2con:=0;
	CLRF       T2CON+0
;basico628.mpas,74 :: 		t2con.TMR2ON:=1;//apaga tmr2.
	BSF        T2CON+0, 2
;basico628.mpas,77 :: 		t2con.T2CKPS1:=0;  //prescalador time 2
	BCF        T2CON+0, 1
;basico628.mpas,78 :: 		t2con.T2CKPS0:=0;  //prescalador time 2
	BCF        T2CON+0, 0
;basico628.mpas,79 :: 		pr2:=138;            //registro pr2
	MOVLW      138
	MOVWF      PR2+0
;basico628.mpas,80 :: 		toca:=0;
	CLRF       _toca+0
	CLRF       _toca+1
;basico628.mpas,81 :: 		flag:=0;
	BCF        _flag+0, BitPos(_flag+0)
;basico628.mpas,83 :: 		trisb.4:=1;
	BSF        TRISB+0, 4
;basico628.mpas,85 :: 		es:=onda[toca];
	CLRF       R0+0
	CLRF       R0+1
	MOVLW      _ONDA+0
	ADDWF      R0+0, 1
	MOVLW      hi_addr(_ONDA+0)
	BTFSC      STATUS+0, 0
	ADDLW      1
	ADDWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      ___DoICPAddr+0
	MOVF       R0+1, 0
	MOVWF      ___DoICPAddr+1
	CALL       _____DoICP+0
	MOVWF      R3+0
	INCF       ___DoICPAddr+0, 1
	BTFSC      STATUS+0, 2
	INCF       ___DoICPAddr+1, 1
	CALL       _____DoICP+0
	MOVWF      R3+1
	MOVF       R3+0, 0
	MOVWF      _es+0
	MOVF       R3+1, 0
	MOVWF      _es+1
;basico628.mpas,86 :: 		es1:=onda[toca];
	CLRF       R0+0
	CLRF       R0+1
	MOVLW      _ONDA+0
	ADDWF      R0+0, 1
	MOVLW      hi_addr(_ONDA+0)
	BTFSC      STATUS+0, 0
	ADDLW      1
	ADDWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      ___DoICPAddr+0
	MOVF       R0+1, 0
	MOVWF      ___DoICPAddr+1
	CALL       _____DoICP+0
	MOVWF      _es1+0
	INCF       ___DoICPAddr+0, 1
	BTFSC      STATUS+0, 2
	INCF       ___DoICPAddr+1, 1
	CALL       _____DoICP+0
	MOVWF      _es1+1
;basico628.mpas,87 :: 		ccpr1l:=es shr 2;
	MOVF       R3+0, 0
	MOVWF      R0+0
	MOVF       R3+1, 0
	MOVWF      R0+1
	RRF        R0+1, 1
	RRF        R0+0, 1
	BCF        R0+1, 7
	RRF        R0+1, 1
	RRF        R0+0, 1
	BCF        R0+1, 7
	MOVF       R0+0, 0
	MOVWF      CCPR1L+0
;basico628.mpas,88 :: 		ccp1con.ccp1x:=es.1;//8;
	BTFSC      _es+0, 1
	GOTO       L__main21
	BCF        CCP1CON+0, 5
	GOTO       L__main22
L__main21:
	BSF        CCP1CON+0, 5
L__main22:
;basico628.mpas,89 :: 		ccp1con.ccp1y:=es.0;//es.9;
	BTFSC      _es+0, 0
	GOTO       L__main23
	BCF        CCP1CON+0, 4
	GOTO       L__main24
L__main23:
	BSF        CCP1CON+0, 4
L__main24:
;basico628.mpas,90 :: 		intcon.7:=1; //habilita las interrupciones.
	BSF        INTCON+0, 7
;basico628.mpas,91 :: 		prende;
	CALL       _prende+0
;basico628.mpas,94 :: 		while true do
L__main8:
;basico628.mpas,98 :: 		if portb.4=1 then portb.2:=0 else portb.2:=1;
	BTFSS      PORTB+0, 4
	GOTO       L__main13
	BCF        PORTB+0, 2
	GOTO       L__main14
L__main13:
	BSF        PORTB+0, 2
L__main14:
;basico628.mpas,101 :: 		end; //while true
	GOTO       L__main8
;basico628.mpas,104 :: 		end.
	GOTO       $+0
; end of _main
