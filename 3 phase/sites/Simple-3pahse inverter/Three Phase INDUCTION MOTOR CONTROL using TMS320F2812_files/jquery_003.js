(function($){$.fn.slider=function(options)
{var id=$(this).attr('id');settings=jQuery.extend({activeClass:'slider-active',passiveClass:'slider',interval:4,speed:15000},options);var obj=new Object();obj.id=id;obj.interval=0;obj.settings=settings;obj.slides=$('#'+id+' div[title^=slide-]');obj.navigation=$('#'+id+'-nav a[rel^=slide-]');obj.active_id=0;obj.interval=setTimeout(function(){$(this).slide_reload(obj);},obj.settings.interval);obj.navigation.each(function()
{$(this).click(function(){$(this).slide_click($(this).attr('rel'),obj);});});}
$.fn.slide_click=function(id,obj)
{clearTimeout(obj.interval)
var c=obj.slides[obj.active_id];id=id.replace(obj.id+'-','');var num=parseInt(id.split('-')[0]);if((num-1)!=obj.active_id)
{var n=obj.slides[0];if(num>0)
{if(obj.slides[num-1])
{n=obj.slides[num-1];obj.active_id=0;}}
for(var i=0;i<obj.slides.length;i++)
{if($(obj.slides[i]).attr('title')==obj.id+'-'+num)
{n=obj.slides[i];obj.active_id=i;break;}}
$(this).slide_exec(c,n,obj);}
else
{obj.interval=setTimeout(function(){$(this).slide_reload(obj);},obj.settings.interval);}}
$.fn.slide_reload=function(obj)
{var c=obj.slides[obj.active_id];var n=$(this).slide_get_next_slide(obj);$(this).slide_exec(c,n,obj);}
$.fn.slide_exec=function(c,n,obj)
{$(c).fadeOut(obj.speed);$(n).fadeIn(obj.speed);$(this).slide_get_nav(obj,$(c).attr('title')+'-nav').attr('class',obj.settings.passiveClass);$(this).slide_get_nav(obj,$(n).attr('title')+'-nav').attr('class',obj.settings.activeClass);obj.interval=setTimeout(function(){$(this).slide_reload(obj);},obj.settings.interval);}
$.fn.slide_get_nav=function(obj,id)
{for(var i=0;i<obj.navigation.length;i++)
{if($(obj.navigation[i]).attr('rel')==id)
{return $(obj.navigation[i]);}}
return null;}
$.fn.slide_get_next_slide=function(obj)
{var n=obj.slides[0];if(obj.slides.length>(obj.active_id+1))
{n=obj.slides[obj.active_id+1];obj.active_id=obj.active_id+1;}
else
obj.active_id=0;return n;}})(jQuery);
