#include<avr/io.h>
#include<delay.h>
#include<adc.h>
//#include<uart.h>

void main()
{	
	 unsigned char pot=0;
	 adc_init();
	 SREG=SREG|0x80;		//global interrupt enable
	 DDRB=0XFF;
	 DDRD=0XFF;
	 //TIMSK=TIMSK|0x03;	//enable interrupt
	 TCCR0=TCCR0|0x72;		// timer 0 in phase correcting pwm,inverting mode,8 prescaler
	 TCCR2=TCCR2|0x62;		// timer 2 in phase correcting pwm,non-inverting mode,8 prescaler
	 while(1)
	 {
		delayms(10);
		pot=getdata(0);		//read adc 1
		OCR0= 130+(pot/2);	//adjustment 
		OCR2= 124-(pot/2);	//adjustment
	 }
}